<?php
/**
 * Single Product Image
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/single-product/product-image.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see         https://docs.woocommerce.com/document/template-structure/
 * @author      WooThemes
 * @package     WooCommerce/Templates
 * @version     2.6.3
 */
if ( ! defined( 'ABSPATH' ) ) {
    exit;
}
global $post, $product;
?>
<div class="images">
    <?php
        $content = '';
        if ( has_post_thumbnail() ) {
            $attachment_count = count( $product->get_gallery_attachment_ids() );
            $gallery          = $attachment_count > 0 ? '[product-gallery]' : '';
            $props            = wc_get_product_attachment_props( get_post_thumbnail_id(), $post );
            $image            = get_the_post_thumbnail( $post->ID, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), array(
                'title'  => $props['title'],
                'alt'    => $props['alt'],
            ) );

            $attachment_count = count( $product->get_gallery_attachment_ids() );
            $attachment_ids = $product->get_gallery_attachment_ids();

            if ( $attachment_count > 0 ) {
                $content .= '[luxe_slider post_type="images" arrows="false"]';
                $content .= '[luxe_slide]';
                $content .= apply_filters( 'woocommerce_single_product_image_html', $image );
                $content .= '[/luxe_slide]';
                foreach ( $attachment_ids as $attachment_id ) {
                    $image_title    = esc_attr( get_the_title( $attachment_id ) );
                    $image_caption  = esc_attr( get_post_field( 'post_excerpt', $attachment_id ) );

                    $image       = wp_get_attachment_image( $attachment_id, apply_filters( 'single_product_large_thumbnail_size', 'shop_single' ), 0, $attr = array(
                        'title' => $image_title,
                        'alt'   => $image_title
                        ) );
                    $image_link     = wp_get_attachment_url( $attachment_id );

                    $image_content = '<img src="' . $image_link . '" alt="' . $image_caption . '">';
                    $content .= '[luxe_slide]';
                    $content .= apply_filters( 'woocommerce_single_product_image_html', $image_content );
                    $content .= '[/luxe_slide]';
                }
                $content .= '[/luxe_slider]';
            } else {
                $content = $image;
            }
            echo do_shortcode($content); 
        } else {
            echo apply_filters( 'woocommerce_single_product_image_html', sprintf( '<img src="%s" alt="%s" />', wc_placeholder_img_src(), esc_html__( 'Placeholder', 'etch' ) ), $post->ID );
        }
        //do_action( 'woocommerce_product_thumbnails' );
    ?>
</div>
