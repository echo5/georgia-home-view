<?php 
use Luxe\Extras;
use Luxe\Wrapper;
?>
<?php if (Extras\display_sidebar()) : ?>
  <aside class="sidebar" id="sidebar-primary">
    <?php include Wrapper\sidebar_path(); ?>
  </aside><!-- /.sidebar -->
<?php endif; ?>
