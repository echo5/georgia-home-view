<div class="entry-meta h5">
    <time class="updated" datetime="<?php echo get_post_time('c', true); ?>"><?php echo get_the_date('d F Y'); ?></time>
    <span class="byline author vcard"><?php esc_html_e('By', 'etch'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" rel="author" class="fn"><?php echo get_the_author(); ?></a></span>
    <!-- <div class="categories"><?php //echo get_the_category_list( ', ', '', get_the_ID() ); ?></div> -->
</div>
