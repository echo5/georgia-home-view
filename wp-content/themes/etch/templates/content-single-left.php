<?php 
    use Luxe\Extras;
    use Luxe\Elements; 
?>
<?php while (have_posts()) : the_post(); ?>
<article <?php post_class('content-single-left'); ?>>
    <div class="row">
        <div class="featured-content col-md-8 pull-right">
            <div class="featured-content-inner">
                <?php
                    if (!empty(get_post_meta( get_the_ID(), '_portfolio_content_custom', true ))) {
                        echo get_post_meta( get_the_ID(), '_portfolio_content_custom', true );
                    } else {
                        echo get_template_part('templates/post-format',  get_post_format());
                    }
                ?>
            </div>
        </div>
        <div class="entry-content col-md-4">
            <div class="entry-content-inner">
                <h1 class="page-title"><?php echo Elements\title(); ?></h1>
                <?php the_content(); ?>                
                <div class="social-share-wrap">
                    <?php echo Extras\social_share(get_the_ID(), get_the_title(), true); ?>  
                </div>
            </div>
        </div>
    </div>
    <footer>
        <?php get_template_part('templates/single-nav'); ?>
        <?php //get_template_part('templates/author-meta'); ?>
        <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . esc_html__('Pages:', 'etch'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php //comments_template('/templates/comments.php'); ?>
</article>
<script>
jQuery(document).ready(function($) {
    $('body').imagesLoaded( function() {
      var featuredContent = $('.featured-content-inner').first();
      var entryContent = $('.entry-content-inner').first();
      var fixedContent,
          staticContent;
      if ((entryContent.outerHeight() > featuredContent.outerHeight()) && featuredContent.outerHeight() < window.innerHeight) {
          fixedContent = featuredContent;
          staticContent = entryContent;
      } else if ((featuredContent.outerHeight() > entryContent.outerHeight()) && (entryContent.outerHeight() < window.innerHeight)) {
          fixedContent = entryContent;
          staticContent = featuredContent;
      } else {
          fixedContent = null;
      }
      if (fixedContent !== null) {
          fixedContent.scrollToFixed({
              marginTop: function() {
                  if( '<?php echo get_theme_mod( 'header_position', 80 ); ?>' === 'fixed-bottom' ) {
                      return 40;
                  } else {
                      return $('header.banner').outerHeight() + 40;           
                  }
              },
              limit: function() {
                  return staticContent.offset().top + staticContent.height() - $(this).height();
              },  
              minWidth: 991,
              removeOffsets: true
          });
      }
    });
});
</script>
<?php endwhile; ?>
