<?php use Luxe\Extras; ?>
<?php 
    global $image_size;
    $image_size = isset($image_size) ? $image_size : 'blog'; 
?>
<article <?php post_class(); ?>>
    <header>
        <div class="featured-content">
            <?php 
            $images = get_post_meta( get_the_ID(), '_post_content_gallery_images', true );
            if ($images) {
                $image_string = implode(',', array_map(function ($entry) {
                  return $entry['image_id'];
                }, $images));
                $gallery = '[luxe_slider arrows="" bullets="" arrows="true" post_type="images" images="' . $image_string . '" img_size="' . $image_size .'" bullets_position="overlay"][/luxe_slider]';
                echo do_shortcode( $gallery );
            }
            ?>
        </div>
        <div class="categories h5"><?php the_category(', '); ?></div>
        <h2 class="entry-title h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
</article>
