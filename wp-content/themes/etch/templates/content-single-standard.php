<?php use Luxe\Extras; //@TODO remove this - http://z.ultranoir.com/en/articles/1270-a-journey-with-cabbibo-creator-of-enough.html http://www.justbean.co.uk/curators-coffee-gallery.php ?>
<?php while (have_posts()) : the_post(); ?>
<article <?php post_class('content-single-standard'); ?>>
    <div class="container-fluid">
        <header class="page-header full-browser-height">
            <div class="container">
                <h1 class="entry-title"><?php the_title(); ?></h1>
                <span class="post-date h4"><?php the_date(); ?></span>
                <div class="post-date h5"><?php the_category(' / '); ?></div>
            </div>
            <nav class="post-nav">
                <?php previous_post_link('<span class="previous-link h5">%link</span>', esc_html__('<i class="icon ion-ios-arrow-left"></i> Previous', 'etch')); ?> 
                <?php echo Elements\back_link(); ?>
                <?php next_post_link('<span class="next-link h5">%link</a>', esc_html__('Next <i class="icon ion-ios-arrow-right"></i>', 'etch')); ?> 
            </nav>
        </header>
        <div class="post-wrap">
            <?php echo Extras\social_share(get_the_ID(), get_the_title()); ?>
            <div class="entry-content">
                <?php the_content(); ?>
            </div>
            <footer>
                <?php get_template_part('templates/author-meta'); ?>
                <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . esc_html__('Pages:', 'etch'), 'after' => '</p></nav>']); ?>
            </footer>
            <?php comments_template('/templates/comments.php'); ?>
        </div>
    </div>
</article>
<?php endwhile; ?>
