<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
    <footer>
      <?php get_template_part('templates/author-meta'); ?>
      <?php wp_link_pages(['before' => '<nav class="page-nav"><p>' . esc_html__('Pages:', 'etch'), 'after' => '</p></nav>']); ?>
    </footer>
    <?php comments_template('/templates/comments.php'); ?>
  </article>
<?php get_template_part('templates/single-nav'); ?>
<?php endwhile; ?>
