<?php 
    use Luxe\Extras;
    use Luxe\Helper; 
?>
<?php 
    global $image_size, $use_bg;
    $image_size = isset($image_size) ? $image_size : 'full'; 
    $link = Helper\get_post_link();
?>
<article <?php post_class(); ?>>
    <div class="featured-content">
        <?php the_post_thumbnail( $image_size ); ?>
        <div class="post-overlay">
            <a href="<?php echo esc_url($link['url']) ?>" target="<?php echo $link['target']; ?>" class="overlay-link portfolio-link"></a>
            <div class="post-overlay-content">
            	<h2 class="entry-title h3"><a href="<?php echo esc_url( $link['url'] ); ?>" target="<?php echo esc_attr($link['target']); ?>" class="portfolio-link"><?php the_title(); ?></a></h2>
            	<div class="terms h5"><?php the_terms(get_the_ID(), 'portfolio_category', ' ', '  &middot;  '); ?></div>
            </div>
        </div>
    </div>
</article>
