<?php use Luxe\Extras; ?>
<?php 
    global $image_size;
    $image_size = isset($image_size) ? $image_size : 'blog'; 
?>
<article <?php post_class(); ?>>
    <header>
        <div class="featured-content">
            <a href="<?php the_permalink(); ?>">
		        <?php the_post_thumbnail( $image_size ); ?>
            </a>
            <div class="post-overlay">
                <a href="<?php the_permalink(); ?>" class="full-link"></a>
                <?php echo Extras\social_share(get_the_ID(), get_the_title()); ?>
                <?php get_template_part('templates/entry-meta'); ?>
                <h2 class="entry-title h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
            </div>
        </div>
    </header>
</article>
