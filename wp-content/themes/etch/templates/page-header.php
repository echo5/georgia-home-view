<?php 
use Luxe\Elements;
use Luxe\Helper;
?>
<?php if (Elements\display_page_header()) : ?>
    <div class="<?php echo implode(' ', apply_filters('luxe_page_header_class', array('page-header', 'outer-container'))); ?>">
        <?php do_action('luxe_before_page_header_container'); ?>
        <div class="page-header-overlay"></div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <?php do_action('luxe_before_page_title'); ?>
                    <?php do_action('luxe_page_title'); ?>
                    <?php do_action('luxe_after_page_title'); ?>
                </div>
            </div>
        </div>
        <?php do_action('luxe_after_page_header_container'); ?>
    </div>
<?php endif; ?>
