<?php 
$images = get_post_meta( get_the_ID(), '_post_content_gallery_images', true );
if ($images) {
    $gallery = '[luxe_slider arrows="" fill="true" margin="0" autoplay="true" bullets_position="overlay"]';
    foreach ($images as $image) {
        $gallery .= '[luxe_slide bg="' . $image['image'] . '"]';
        // $gallery .= '[luxe_slide]';
        // $gallery .= '<img src="' . $image['image'] . '">';
        $gallery .= '[/luxe_slide]';
    }
    $gallery .= '[/luxe_slider]';
    echo do_shortcode( $gallery );
}
?>
