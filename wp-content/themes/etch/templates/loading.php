<div id="preloader" class="preloader loading-fade">
	<div class="preloader-content">
        <?php echo html_entity_decode( get_theme_mod( 'preloader_content', '' ) ); ?>
        <!-- <div class="uil-ring-css"><div></div></div> -->
    </div>
    <div class="preloader-icon"></div>
</div>
