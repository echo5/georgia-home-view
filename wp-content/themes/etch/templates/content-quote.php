<?php use Luxe\Extras; ?>
<article <?php post_class(''); ?>>
	<span class="quote h1">"</span>
    <h2 class="entry-summary h4">
        <?php echo strip_tags(get_the_content()); ?>
    </h2>
    <div class="entry-title h5">
    	<?php the_title(); ?>
    </div>
</article>
