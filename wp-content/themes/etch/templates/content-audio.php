<?php use Luxe\Extras; ?>
<article <?php post_class(); ?>>
    <header>
        <h2 class="entry-title h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <div class="post-audio-clip">
        <?php echo do_shortcode('[audio mp3="' . get_post_meta( get_the_ID(), '_post_content_audio', true ) . '"]' ); ?>
    </div>
</article>
