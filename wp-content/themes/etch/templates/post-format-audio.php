<div class="post-audio-clip">
    <?php echo do_shortcode('[audio mp3="' . get_post_meta( get_the_ID(), '_post_content_audio', true ) . '"]' ); ?>
</div>
