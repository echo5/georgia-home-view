<?php use Luxe\Elements; ?>
<?php echo Elements\modal_widget(); ?>
<footer class="content-info <?php echo implode(' ', apply_filters('footer_class', array(''))); ?>" id="footer">
    <?php echo Elements\footer_columns(); ?>
    <?php echo Elements\footer_copy(); ?>
</footer>
