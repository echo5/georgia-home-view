<?php use Luxe\Elements; ?>
<nav class="post-nav">
    <?php previous_post_link('<span class="previous-link h5"><i class="icon ion-ios-arrow-left"></i> %link</span>', esc_html__('Previous', 'etch')); ?> 
    <?php echo Elements\back_link(); ?>
    <?php next_post_link('<span class="next-link h5">%link <i class="icon ion-ios-arrow-right"></i></a>', esc_html__('Next', 'etch')); ?> 
</nav>
