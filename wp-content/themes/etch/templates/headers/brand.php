<?php use Luxe\Elements; ?>
<?php if(!get_theme_mod( 'brand_hide', false )) { ?>
    <div class="brand-wrap">
        <a class="brand navbar-brand brand-dark" href="<?php echo esc_url(home_url('/')); ?>"><?php echo Elements\logo('dark', 'logo-dark', 'logo-dark'); ?></a>
        <a class="brand navbar-brand brand-light" href="<?php echo esc_url(home_url('/')); ?>"><?php echo Elements\logo('light', 'logo-light', 'logo-light'); ?></a>
        <a class="brand navbar-brand brand-scrolled" href="<?php echo esc_url(home_url('/')); ?>"><?php echo Elements\logo('scrolled', 'logo-scrolled', 'logo-scrolled'); ?></a>
    </div>
<?php } ?>
