<?php use Luxe\Elements; ?>
<div class="hidden-md hidden-lg">
  <?php global $offcanvas_position; $offcanvas_position = 'right'; ?>
  <?php get_template_part( 'templates/headers/offcanvas-nav' ); ?>
</div>
<header class="banner header-centered-logo-and-nav" id="header">
    <div class="container">
    	<div class="row">
    		<div class="col-md-12">
		        <?php get_template_part( 'templates/headers/brand' ); ?>
    		</div>
    		<div class="col-md-12">
		        <nav class="nav-primary hidden-xs hidden-sm">
		            <?php
		                if (has_nav_menu('primary_navigation')) :
		                    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav'));
		                endif;
		            ?>
		        </nav>
    		</div>
	        <?php dynamic_sidebar('header-buttons'); ?>
	        <?php echo Elements\header_buttons(); ?>
	        <div class="col-md-12">
		        <div class="hidden-md hidden-lg pull-right">
		          <?php echo Elements\nav_button(); ?>
		        </div>
	        </div>
    	</div>
    </div>
</header>
