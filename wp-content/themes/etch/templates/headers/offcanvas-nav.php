<?php 
    global $offcanvas_position;
    if ($offcanvas_position == 'right') {
        $move_x = '100%';
        $content_push = '-200px';
    } else {
        $move_x = '-100%';
        $content_push = '200px';
    }
?>
<nav id="nav-primary" class="nav-primary off-canvas off-canvas-<?php echo $offcanvas_position; ?> nav-side" data-offcanvas-position="<?php echo $offcanvas_position; ?>" data-move-x="<?php echo $move_x; ?>" data-move-y="" data-active-class="nav-active" data-content-push="<?php echo $content_push; ?>">
    <div class="close-btn toggle-off-canvas" data-offcanvas-target="#nav-primary"><i class="icon ion-close-round"></i></div>
    <div class="container">
        <div class="row">
            <div class="col-sm-12"><h4 class="nav-side-title"><?php echo get_theme_mod( 'side_nav_content', '' ); ?></h4></div>
            <div class="col-sm-12">
                <?php
                    if (has_nav_menu('primary_navigation')) :
                        wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav'));
                    endif;
                ?>
            </div>
            <div class="col-sm-12"><h4 class="nav-side-title"><?php echo get_theme_mod( 'side_nav_content_below', '' ); ?></h4></div>
        </div>
    </div>
</nav>
