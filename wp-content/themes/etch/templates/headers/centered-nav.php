<?php use Luxe\Elements; ?>
<div class="hidden-md hidden-lg">
  <?php global $offcanvas_position; $offcanvas_position = 'right'; ?>
  <?php get_template_part( 'templates/headers/offcanvas-nav' ); ?>
</div>
<header class="banner header-centered-nav" id="header">
    <div class="container">
        <div class="pull-left">
            <?php get_template_part( 'templates/headers/brand' ); ?>
        </div>
        <nav class="nav-primary hidden-sm hidden-xs">
            <?php
                if (has_nav_menu('primary_navigation')) :
                    wp_nav_menu(array('theme_location' => 'primary_navigation', 'menu_class' => 'navbar-nav nav'));
                endif;
            ?>
        </nav>
        <?php dynamic_sidebar('header-buttons'); ?>
        <?php echo Elements\header_buttons(); ?>
        <div class="hidden-md hidden-lg pull-right">
          <?php echo Elements\nav_button(); ?>
        </div>
    </div>
</header>
