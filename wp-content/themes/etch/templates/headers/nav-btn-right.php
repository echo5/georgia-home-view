<?php use Luxe\Elements; ?>
<?php global $offcanvas_position; $offcanvas_position = 'right'; ?>
<?php get_template_part( 'templates/headers/offcanvas-nav' ); ?>
<header class="banner header-nav-btn-right" id="header">
    <div class="container">
        <div class="pull-left brand-pull">
            <?php get_template_part( 'templates/headers/brand' ); ?>
        </div>
        <div class="pull-right">
            <?php echo Elements\nav_button(); ?>
        </div>
        <?php dynamic_sidebar('header-buttons'); ?>
        <?php echo Elements\header_buttons(); ?>
    </div>
</header>
