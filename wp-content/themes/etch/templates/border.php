<?php if (get_theme_mod('border', false)) { ?>
    <div class="top border"><div class="inner-border"></div></div>
    <div class="right border"><div class="inner-border"></div></div>
    <div class="bottom border"><div class="inner-border"></div></div>
    <div class="left border"><div class="inner-border"></div></div>
<?php } ?>
