<article <?php post_class(''); ?>>
    <header>
        <?php if (has_post_thumbnail()) { ?>
            <div class="featured-content">
                <a href="<?php the_permalink(); ?>">
                    <?php the_post_thumbnail( 'blog' ); ?>
                </a>
                <div class="post-overlay">
                    <a href="<?php the_permalink(); ?>" class="full-link"></a>
                </div>
            </div>
        <?php } ?>
        <div class="categories h5"><?php the_category(', '); ?></div>
        <h2 class="entry-title h3"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
    </header>
    <div class="entry-summary">
        <?php the_excerpt(); ?>
    </div>
    <footer>
        <?php //get_template_part('templates/entry-meta'); ?>
    </footer>

</article>
