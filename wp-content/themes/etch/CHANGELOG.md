# Change Log
Theme updates, changes, and fixes.

## [1.1.5] - 2017-1-4
### Added
- Add in side navigation content below option

## [1.1.4] - 2017-1-3
### Added
- Update Visual Composer to version 5.0.1

### Fixed
- Responsive design options

## [1.1.3] - 2016-10-28
### Added
- Preloader state styles for Safari

## [1.1.2] - 2016-10-28
### Fixed
- Preloader state for safari back button
- Kirki 2.3.7 compatibility due to removal of Kirki_Sanitize

### Removed
- Temporary fix for "Remove" button bug in Kirki
- Secondary unused parallax background for header

## [1.1.1] - 2016-09-28
### Changed
- Full browser height sections no longer applied to rows on tablet devices and below
- Dependency on parent stylesheet removed in child theme

### Fixed
- Header logo maximum height

## [1.1.0] - 2016-09-23
### Added
- PHP 5.3 compatiblity

### Changed
- Remove Kirki options from theme framework and add to required plugins for faster updates

### Fixed
- Header logo maximum height
