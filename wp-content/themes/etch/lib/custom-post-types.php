<?php

namespace Luxe\CustomPostTypes;

if (class_exists('Luxe_Custom_Post_Types')) {
	/**
	 * Add custom post types
	 */
	$custom_post_types = array(
	    'portfolio',
	    'testimonials'
	);
	new \Luxe_Custom_Post_Types($custom_post_types);
}

