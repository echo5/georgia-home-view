<?php

namespace Luxe\Extras;

use Luxe\Setup;
use Luxe\Header;
use Luxe\Helper;

/**
 * Add <body> classes
 */
function body_class($classes) {
  // Add page slug if it doesn't exist
  if (is_single() || is_page() && !is_front_page()) {
    if (!in_array(basename(get_permalink()), $classes)) {
      $classes[] = basename(get_permalink());
    }
  }

  // Add class if sidebar is active
  if (display_sidebar()) {
    $classes[] = 'sidebar-primary';
  }
  else {
    $classes[] = 'no-sidebar-primary';
  }

  // Preloader
  global $wp_customize;
  if (get_theme_mod( 'preloader', false ) && !isset($wp_customize)) {
    $classes[] = 'loading';
  }

  // Post style
  if (is_singular('portfolio')) {
    $classes[] = 'content-single-left';
  }
  elseif (is_singular('post')) {
    $classes[] = 'content-single-' . get_theme_mod('single_post_style', 'side');
  }

  return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');

/**
 * Determine which pages should NOT display the sidebar
 */
function display_sidebar() {
  static $display;

  $woocommerce = false;
  if ( Helper\is_woocommerce_active() ) {
      $woocommerce = ( is_woocommerce() ? true : false ); 
  }

  // Default hidden array
  $display = !in_array(true, array(
      // The sidebar will NOT be displayed if ANY of the following return true.
      // @link https://codex.wordpress.org/Conditional_Tags
      is_404(),
      is_front_page(),
      is_page_template('template-custom.php'),
      $woocommerce,
    ));

  $page_id = Helper\get_post_id();
  if (is_page()) {
      $display = get_theme_mod( 'sidebar_page', false );
  }
  if (is_single()) {
    $display = get_theme_mod('sidebar_single', false);
  }

  $page_sidebar_display = get_post_meta( $page_id, '_page_layout_sidebar', true );
  if (!empty($page_sidebar_display) && $page_sidebar_display != 'theme-default') {
    if ($page_sidebar_display == 'hide') {
        $display = false;
    }
    else {
        $display = $page_sidebar_display;
    }
  }

  return apply_filters('luxe/display_sidebar', $display);
}

/**
 * Clean up the_excerpt()
 */
function excerpt_more() {
  return ' &hellip; <a class="read-more" href="' . get_permalink() . '">' . esc_attr__('Continue Reading', 'etch') . '</a>';
}
add_filter('excerpt_more', __NAMESPACE__ . '\\excerpt_more');

/**
 * Shorten excerpt
 */
function shorten_excerpt_length($length) {
    return 15; 
}
add_filter('excerpt_length', __NAMESPACE__ . '\shorten_excerpt_length');

/**
 * Get user social fields
 */
function user_social_fields() {
    return array(
        'facebook',
        'linkedin',
        'twitter',
        'googleplus'
    );
}

/**
 * Get social share links
 * @param  int $page_id
 * @param  str $title   
 * @return str
 */
function social_share($page_id, $title, $hover = false) {
    $url = get_permalink($page_id);
    $classes = array();
    $classes[] = $hover ? 'social-share-hover' : '';
    $content = '<div class="social-icons social-share ' . implode(' ', $classes) . '">';
    $content .= '<ul>';
    if ($hover) {
        $content .= '<li><i class="icon ion-android-share-alt share-icon"></i><ul>';
    }
    $content .= '<li class=""><a target="_blank" href="http://www.facebook.com/sharer.php?u='.$url.'"><i class="icon ion-social-facebook"></i></a></li>';
    $content .= '<li class=""><a target="_blank" href="https://twitter.com/share?url='.$url.'&text='.$title.'"><i class="icon ion-social-twitter"></i></a></li>';
    // $content .= '<li class=""><a target="_blank" href="https://plus.google.com/share?url='.$url.'"><i class="icon ion-social-google"></i></a></li>';
    $content .= '<li class=""><a target="_blank" href="http://www.tumblr.com/share/link?url='.$url.'&name='.$title.'&description="><i class="icon ion-social-tumblr"></i></a></li>';
    // $content .= '<li class=""><a target="_blank" href="http://reddit.com/submit?url='.$url.'&title='.$title.'"><i class="icon ion-social-reddit"></i></a></li>';
    // $content .= '<li class=""><a target="_blank" href="http://www.stumbleupon.com/submit?url='.$url.'&title='.$title.'"><i class="icon ion-social-stumbleupon"></i></a></li>';
    if ($hover) {
      $content .= '</ul>';
    }
    $content .= '</li></ul>';
    $content .= '</div>';
    return $content;
}

/**
 * Add in page header classes
 */
function page_header_add_classes($classes) {
    $page_id = Helper\get_post_id();
    $full_browser_height = get_post_meta( $page_id, '_page_header_full_browser_height', true );
    if ($full_browser_height) {
        $classes[] = 'full-browser-height';
    }
    return $classes;
}
add_action( 'luxe_page_header_class', __NAMESPACE__ . '\\page_header_add_classes' );


/**
 * Add custom user font options
 */
function add_custom_fonts( $kirki_fonts ) {
    $user_fonts = array();
    $custom_fonts = get_theme_mod('custom_typography', false);
    if (is_array($custom_fonts)) {
      foreach ($custom_fonts as $font) {
        $user_fonts['"' . $font['font_name'] . '"'] = array(
            'label' => $font['font_name'],
            'stack'  => $font['font_name'] . ',Helvetica,Arial,sans-serif',
            'variants'  => array(
              array(
                'id'    => '100',
                'label' => '100',
              ),
              array(
                'id'    => '200',
                'label' => '200',
              ),
              array(
                'id'    => '300',
                'label' => '300',
              ),
              array(
                'id'    => '400',
                'label' => '400',
              ),
              array(
                'id'    => '500',
                'label' => '500',
              ),
              array(
                'id'    => '600',
                'label' => '600',
              ),
              array(
                'id'    => '700',
                'label' => '700',
              ),
              array(
                'id'    => '800',
                'label' => '800',
              ),
              array(
                'id'    => '900',
                'label' => '900',
              ),
            ),
        );
      }
    }
    $fonts = array_merge($user_fonts, $kirki_fonts);
    return $fonts;
}
add_filter( 'kirki/fonts/standard_fonts', __NAMESPACE__ . '\\add_custom_fonts' );


/**
 * Add footer classes
 */
function add_footer_classes($classes) {
    if (get_theme_mod( 'footer_fixed', true )) {
      $classes[] = 'footer-fixed';
    }
    return $classes;
}
add_filter('footer_class', __NAMESPACE__ . '\\add_footer_classes');
