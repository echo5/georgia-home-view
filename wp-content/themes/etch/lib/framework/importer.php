<?php

use Luxe\Assets;


include_once( LUXE_FRAMEWORK_VENDOR_PATH . 'proteusthemes/one-click-demo-import/one-click-demo-import.php' );

/**
 * Add importer styles
 */
function enqueue_importer_styles($hook) {
    if ( 'appearance_page_radium_demo_installer' != $hook ) {
        return;
    }
    wp_enqueue_style( 'luxe-importer', Assets\luxe_asset_path('styles/importer.css' ));
}
add_action( 'admin_enqueue_scripts', 'enqueue_importer_styles' );
