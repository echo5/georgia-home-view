<?php

namespace Luxe\Elements;

use Luxe\Helper;

/**
 * Page titles
 */
if (!function_exists(__NAMESPACE__ . '\\title')) {
    function title()
    {
        $page_id = Helper\get_post_id();
        $override_title = get_post_meta($page_id, '_page_header_page_title', true);
        if (!empty($override_title)) {
            return $override_title;
        } elseif (is_home()) {
            if (get_option('page_for_posts', true)) {
                return get_the_title(get_option('page_for_posts', true));
            } else {
                return esc_attr__('Latest Posts', 'etch');
            }
        } elseif (Helper\is_shop()) {
            $shop_page_id = get_option('woocommerce_shop_page_id');
            return get_the_title($shop_page_id);
        } elseif (is_archive()) {
            return get_the_archive_title();
        } elseif (is_search()) {
            return sprintf(esc_attr__('Search Results for %s', 'etch'), get_search_query());
        } elseif (is_404()) {
            return esc_attr__('Not Found', 'etch');
        } else {
            return get_the_title();
        }
    }
}

/**
 * Determine if title is displayed
 * @return bool
 */
if (!function_exists(__NAMESPACE__ . '\\display_page_header')) {
    function display_page_header()
    {
        static $display;

        $display = get_theme_mod('page_headers', true);

        if (is_404()) {
            $display = false;
        }
        if (is_single()) {
            $display = true;
        }
        if (Helper\is_product()) {
            $display = false;
        }
        if (is_singular('portfolio')) {
            $display = false;
        }

        $page_id = Helper\get_post_id();
        $page_header_display = get_post_meta($page_id, '_page_header_display', true);
        if (!empty($page_header_display) && $page_header_display != 'theme-default') {
            if ($page_header_display == 'hide') {
                $display = false;
            } else {
                $display = $page_header_display;
            }
        }

        return apply_filters('luxe/display_page_header', $display);
    }
}

/**
 * Logo
 * @param  str $type  Type from theme options
 * @param  str $class Classes
 * @param  str $id    ID
 * @return str
 */
if (!function_exists(__NAMESPACE__ . '\\logo')) {
    function logo($type = 'dark', $class = null, $id = null)
    {
        $logo = get_theme_mod('logo_' . $type, null);
        $id = ($id ? 'id="' . $id . '"' : '');
        if (!empty($logo)) {
            return '<img class="' . $class . '" ' . $id . ' src="' . $logo . '">';
        }
        return '<span class="brand-text ' . $class . '" ' . $id . '>' . get_bloginfo('name') . '</span>';
    }
}

/**
 * Header Buttons
 */
if (!function_exists(__NAMESPACE__ . '\\header_buttons')) {

    function header_buttons()
    {
        $buttons = get_theme_mod('header_buttons', false);
        $output = '<ul class="nav navbar-nav pull-right header-buttons">';

        if ($buttons) {
            foreach ($buttons as $button) {
                if (!empty($button['text'])) {
                    $classes = array();
                    $data_atts = '';
                    $classes[] = ($button['action'] == 'offcanvas_widget' ? 'sidebar-off-canvas-btn toggle-off-canvas no-ajax' : '');
                    $button['url'] = ($button['action'] != 'link' && $button['action'] != '' ? '#' : $button['url']);
                    $classes[] = $button['style'];
                    $data_atts .= ($button['action'] == 'offcanvas_widget' ? 'data-offcanvas-target="#sidebar-off-canvas"' : '');
                    $data_atts .= ($button['action'] == 'modal' ? 'data-toggle="modal" data-target="#modal-widget"' : '');
                    $output .= '<li>';
                    $output .= '<a class="' . implode(' ', $classes) . '" href="' . $button['url'] . '" ' . $data_atts . '>';
                    $output .= $button['text'];
                    $output .= '</a>';
                    $output .= '</li>';
                }
            }
        }

        if (get_theme_mod('header_search', false)) {
            $output .= '<li>';
            $output .= '<a class="search-btn toggle-off-canvas" data-offcanvas-target="#sidebar-search" href="#">';
            $output .= '<i class="icon ion-ios-search-strong"></i>';
            $output .= '</a>';
            ob_start();
            ?>
            <div id="sidebar-search" class="sidebar off-canvas off-canvas-overlay" data-move-x="0" data-move-y="" data-active-class="search-active">
                <?php the_widget('WP_Widget_Search');?>
            </div>
            <?php
            $output .= ob_get_clean();
            $output .= '</li>';
        }

        if (class_exists('WooCommerce')) {
            if (get_theme_mod('header_cart', false) && Helper\is_woocommerce_active()) {
                $output .= '<li>';
                $output .= '<a class="sidebar-cart-btn toggle-off-canvas" data-offcanvas-target="#sidebar-cart" href="#">';
                $output .= '<i class="icon ion-bag"></i>';
                $output .= '<div class="cart-contents">';
                $output .= '<sup class="cart-count">' . WC()->cart->get_cart_contents_count() . '</sup>';
                $output .= '<span class="cart-total">' . WC()->cart->get_cart_total() . '</span>';
                $output .= '</div>';
                $output .= '</a>';
                $output .= '</li>';
            }
        }
        $output .= '</ul>';
        return $output;
    }
}

/**
 * Add cart sidebar if active
 */
if (!function_exists(__NAMESPACE__ . '\\search_sidebar')) {
    function search_sidebar()
    {
        if (Helper\is_woocommerce_active() && get_theme_mod('header_search', false)) {
            ob_start();
            ?>
        <div id="sidebar-search" class="sidebar">
            <?php the_widget('WP_Widget_Search');?>
        </div>
        <?php
            $output = ob_get_clean();
            echo $output;
        }
    }
}
// add_action( 'luxe_after_header', __NAMESPACE__ . '\\search_sidebar', 10 );

/**
 * Footer columns
 */
if (!function_exists(__NAMESPACE__ . '\\footer_columns')) {
    function footer_columns()
    {
        $footer_columns = get_theme_mod('footer_columns', 3);
        if ($footer_columns > 0) {
            $output = '<div class="footer-widgets-wrap">';
            $output .= '<div class="container">';
            $output .= '<div class="row">';
            for ($num = 1; $num <= $footer_columns; $num++) {
                ob_start();
                ?>
            <div class="<?php echo Helper\get_column_class($footer_columns, 'sm'); ?>">
                <?php dynamic_sidebar('sidebar-footer-' . $num);?>
            </div>
            <?php
$output .= ob_get_clean();
            }
            $output .= '</div>';
            $output .= '</div>';
            $output .= '</div>';
            return do_shortcode($output);
        }
    }
}

/**
 * Footer copy
 */
if (!function_exists(__NAMESPACE__ . '\\footer_copy')) {
    function footer_copy()
    {
        $output = '';
        if (get_theme_mod('footer_copy', true)) {
            ob_start();
            ?>
        <div class="footer-copy-wrap">
            <div class="container footer-copy">
                <div class="row">
                    <div class="col-xs-12">
                        <?php dynamic_sidebar('sidebar-footer');?>
                    </div>
                </div>
            </div>
        </div>
        <?php
$output = ob_get_clean();
        }
        return do_shortcode($output);
    }
}

/**
 * Display nav button
 */
if (!function_exists(__NAMESPACE__ . '\\nav_button')) {
    function nav_button()
    {
        $nav_button = get_theme_mod('nav_button', 'theme-default');
        $button = esc_attr__('Menu', 'etch');
        if ($nav_button == 'text') {
            $button = '<span class="nav-text">' . esc_attr(get_theme_mod('nav_button_text', 'Menu')) . '</span>';
        }
        if ($nav_button == 'icon') {
            $button = '<div class="nav-icon"><span class="nav-btn-line"></span><span class="nav-btn-line"></span><span class="nav-btn-line"></span></div>';
        }
        if ($nav_button == 'icon_and_text') {
            // $button = '<i class="icon ion-drag"></i> ';
            $button = '<div class="nav-icon"><span class="nav-btn-line"></span><span class="nav-btn-line"></span><span class="nav-btn-line"></span></div>';
            $button .= '<span class="nav-text">' . esc_attr(get_theme_mod('nav_button_text', 'Menu')) . '</span>';
        }

        $output = '<div class="nav-btn-wrap">';
        $output .= '<div class="toggle-off-canvas nav-btn nav-btn-' . $nav_button . '" data-offcanvas-target="#nav-primary">';
        $output .= $button;
        $output .= '</div>';
        $output .= '</div>';

        return $output;
    }
}

/**
 * Add page title in
 */
if (!function_exists(__NAMESPACE__ . '\\add_page_title')) {
    function add_page_title()
    {
        $subtitle = get_post_meta(Helper\get_post_id(), '_page_header_page_subtitle', true);
        ?>
        <h1><?php echo title(); ?></h1>
        <?php if (!empty($subtitle)) {?>
            <div class="subtitle h3"><?php echo esc_html($subtitle); ?></div>
        <?php } ?>
    <?php
}
}
add_action('luxe_page_title', __NAMESPACE__ . '\\add_page_title');

/**
 * Add post format content to header area
 */
if (!function_exists(__NAMESPACE__ . '\\add_featured_content_to_header')) {
    function add_featured_content_to_header()
    {
        $style = get_post_style();
        if ((is_singular('post') || is_singular('portfolio')) && get_post_format() != '') {
            ?>
        <div class="featured-content">
            <?php echo get_template_part('templates/post-format', get_post_format()); ?>
        </div>
        <?php
}
    }
}
add_action('luxe_before_page_header_container', __NAMESPACE__ . '\\add_featured_content_to_header');

/**
 * Get name of post style
 */
if (!function_exists(__NAMESPACE__ . '\\get_post_style')) {
    function get_post_style()
    {
        if (is_singular('post')) {
            return get_theme_mod('single_post_style', 'default');
        } elseif (is_singular('portfolio')) {
            return get_theme_mod('portfolio_post_style', 'left');
        } else {
            return 'default';
        }

    }
}

/**
 * Back to index link
 */
if (!function_exists(__NAMESPACE__ . '\\back_link')) {
    function back_link()
    {
        $link = '';
        if (is_singular('portfolio')) {
            $link = get_theme_mod('portfolio_link', '');
        } elseif (is_singular('post')) {
            $link = get_theme_mod('blog_link', '');
        }

        if ($link != '') {
            echo '<a class="back-link" href="' . esc_url($link) . '"><i class="icon ion-grid"></i></a>';
        }
    }
}

/**
 * Modal window
 */
if (!function_exists(__NAMESPACE__ . '\\modal_widget')) {
    function modal_widget()
    {
        if (is_active_sidebar('sidebar-modal')) {
            $output = '[luxe_modal id="modal-widget"]';
            ob_start();
            dynamic_sidebar('sidebar-modal');
            $output .= ob_get_clean();
            $output .= '[/luxe_modal]';
            return do_shortcode($output);
        }
    }
}
