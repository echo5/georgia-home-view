<?php

namespace Luxe\Header;

use Luxe\Helper;

/**
 * Add <body> classes
 */
function body_class($classes)
{
    // Header Scheme
    $page_id = Helper\get_post_id();
    $header_scheme = get_post_meta($page_id, '_page_layout_header_scheme', true);
    if ($header_scheme && $header_scheme != 'theme-default') {
        $classes[] = $header_scheme;
    } elseif (Helper\is_product()) {
        $classes[] = get_theme_mod('shop_header_scheme', 'header-dark-active');
    } elseif (is_archive()) {
        $classes[] = get_theme_mod('blog_header_scheme', 'header-dark-active');
    } else {
        $classes[] = get_theme_mod('header_scheme', 'header-dark-active');
    }

    // Header transitions
    $header_transition = get_theme_mod('header_transition', 'none');
    if ($header_transition) {
        $classes[] = 'header-transition-' . $header_transition;
    }
    $header_position = get_theme_mod('header_position', 'absolute');
    if ($header_position) {
        $classes[] = 'header-position-' . $header_position;
    }

    $classes[] = 'header-' . get_theme_mod('header_style', 'default');
    $classes[] = get_theme_mod('side_nav_effect', 'nav-effect-slide');
    $classes[] = get_theme_mod('nav_hover_effect', 'nav-hover-effect-none');

    return $classes;
}
add_filter('body_class', __NAMESPACE__ . '\\body_class');
