<?php

LuxeOption::add_panel( 'body', array(
    'title'          => esc_attr__( 'Body', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
LuxeOption::add_section( 'body_base', array(
    'title'          => esc_attr__( 'Base', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'body',
) );



/**
 * Base
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'body_bg_color',
    'label'       => esc_attr__( 'Body Background Color', 'etch' ),
    'section'     => 'body_base',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'body, .content-wrapper',
            'property' => 'background-color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'body, .content-wrapper',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'container_width',
    'label'       => esc_attr__( 'Maximum Container Width', 'etch' ),
    'description' => esc_attr__( 'Controls how wide your content is on larger screens.', 'etch' ),
    'help'        => esc_attr__( 'This does not apply to full browser width sections.', 'etch' ),
    'section'     => 'body_base',
    'default'     => '1170px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.container',
            'property' => 'max-width',
            'media_query' => '@media (min-width: 1200px)'
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.container',
            'property' => 'max-width',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'border',
    'label'       => esc_attr__( 'Enable Window Border', 'etch' ),
    'description' => esc_attr__( 'Adds a border around the viewing window of your page.', 'etch' ),
    'section'     => 'body_base',
    'default'     => false,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'border_color',
    'label'       => esc_attr__( 'Border Color', 'etch' ),
    'section'     => 'body_base',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.border .inner-border',
            'property' => 'background-color',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.border .inner-border',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'border_width',
    'label'       => esc_attr__( 'Border Width', 'etch' ),
    'description' => esc_attr__( 'The width of the border around your page.', 'etch' ),
    'section'     => 'body_base',
    'default'     => '0',
    'priority'    => 10,
    'choices'      => array(
        'min'  => 0,
        'max'  => 100,
        'step' => 1,
    ),
    'output'      => array(
        array(
            'element'  => '.border.left .inner-border, .border.right .inner-border',
            'property' => 'width',
            'units'    => 'px',
        ),
        array(
            'element'  => '.border.top .inner-border, .border.bottom .inner-border',
            'property' => 'height',
            'units'    => 'px',
        ),
        // array(
        //     'element'  => 'body',
        //     'property' => 'padding',
        //     'units'    => 'px',
        // ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.border.left .inner-border, .border.right .inner-border',
            'property' => 'width',
            'function' => 'css'
        ),
        array(
            'element'  => '.border.top .inner-border, .border.bottom .inner-border',
            'property' => 'height',
            'function' => 'css'
        ),
        array(
            'element'  => 'body',
            'property' => 'padding',
            'function' => 'css'
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'border_padding',
    'label'       => esc_attr__( 'Border Padding', 'etch' ),
    'description' => esc_attr__( 'The spacing between your border and the window.', 'etch' ),
    'section'     => 'body_base',
    'default'     => '0',
    'priority'    => 10,
    'choices'      => array(
        'min'  => 0,
        'max'  => 100,
        'step' => 1,
    ),
    'output'      => array(
        array(
            'element'  => '.border',
            'property' => 'padding',
            'units'    => 'px',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.border',
            'property' => 'padding',
            'function' => 'css'
        ),
    ),
) );
