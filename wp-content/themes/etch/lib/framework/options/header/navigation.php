<?php

LuxeOption::add_section( 'navigation', array(
    'title'          => esc_attr__( 'Off Canvas Navigation', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header'
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'offcanvas_nav_width',
    'label'       => esc_attr__( 'Width', 'etch' ),
    'description' => esc_attr__( 'The maximum width of your off canvas navigation on larger screens.  This will automatically be limited to the screen width.', 'etch' ),
    'help'        => esc_attr__( 'This does not apply to overlay navigations.', 'etch' ),
    'section'     => 'navigation',
    'default'     => '300px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.nav-primary.off-canvas',
            'property' => 'width',
            'media_query' => '@media (min-width: 1200px)'
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.nav-primary.off-canvas',
            'property' => 'width',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'side_nav_effect',
    'label'       => esc_attr__( 'Side Navigation Open Effect', 'etch' ),
    'description' => esc_attr__( 'The effect of your side navigation bar when opened.', 'etch' ),
    'section'     => 'navigation',
    'default'     => 'nav-effect-slide',
    'priority'    => 10,
    'choices'     => array(
        'nav-effect-slide' => esc_attr__( 'Slide Over Content', 'etch' ),
        'nav-effect-push'   => esc_attr__( 'Push Content', 'etch' ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'offcanvas_nav_padding',
    'label'       => esc_attr__( 'Navigation Item Padding', 'etch' ),
    'description' => esc_attr__( 'Control the padding in pixels above and below your navigation items in the sidebar.', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'navigation',
    'default'     => '2',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.nav-primary.off-canvas .navbar-nav>li',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
        array(
            'element'  => '.nav-primary.off-canvas .navbar-nav>li',
            'property' => 'padding-bottom',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.nav-primary.off-canvas .navbar-nav>li',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
        array(
            'element'  => '.nav-primary.off-canvas .navbar-nav>li',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'side_nav_bg_color',
    'label'       => esc_attr__( 'Navigation Background Color', 'etch' ),
    'description' => esc_attr__( 'Set the color of your side navigation\'s background.', 'etch' ),
    'section'     => 'navigation',
    'default'     => '#f7f7f7',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.nav-primary.off-canvas',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.nav-primary.off-canvas',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'editor',
    'settings'    => 'side_nav_content',
    'label'       => esc_attr__( 'Side Navigation Content', 'etch' ),
    'description' => esc_attr__( 'The content that appears above your menu.', 'etch' ),
    'help'        => esc_attr__( 'Leave blank for none.', 'etch' ),
    'section'     => 'navigation',
    'default'     => esc_attr__( '', 'etch' ),
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'editor',
    'settings'    => 'side_nav_content_below',
    'label'       => esc_attr__( 'Side Navigation Content Below', 'etch' ),
    'description' => esc_attr__( 'The content that appears below your menu item.', 'etch' ),
    'help'        => esc_attr__( 'Leave blank for none.', 'etch' ),
    'section'     => 'navigation',
    'default'     => esc_attr__( '', 'etch' ),
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'nav_typography',
    'label'       => esc_attr__( 'Navigation Typography', 'etch' ),
    'description' => esc_attr__( 'Select the font family and style for your navigation menu.', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'navigation',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '14',
        'font-weight'    => '400',
        'line-height'    => '1.5',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#333333',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => true,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.nav-primary.off-canvas li a',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'nav_typography_color_hover',
    'label'       => esc_attr__( 'Navigation Font Hover Color', 'etch' ),
    'description' => esc_attr__( 'Set the color of your navigation font when hovered.', 'etch' ),
    'section'     => 'navigation',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.nav-primary.off-canvas li a:hover, .nav-side a:hover, .nav-primary.off-canvas li.current-menu-item a',
            'property' => 'color',
        ),
        array(
            'element'  => '.nav-primary.off-canvas li a:after',
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'nav_hover_effect',
    'label'       => esc_attr__( 'Navigation Hover Effect', 'etch' ),
    'description' => esc_attr__( 'The effect when one of your navigation items is hovered.', 'etch' ),
    'section'     => 'navigation',
    'default'     => 'none',
    'priority'    => 10,
    'choices'     => array(
        'nav-hover-effect-none' => esc_attr__( 'None', 'etch' ),
        'nav-hover-effect-underline'   => esc_attr__( 'Underline', 'etch' ),
        'nav-hover-effect-move-right'   => esc_attr__( 'Move Right', 'etch' ),
    ),
    'output'      => array(
        array(
            'element'  => '.nav-primary li a:hover',
            'property' => 'animation-name',
        ),
    ),
) );
