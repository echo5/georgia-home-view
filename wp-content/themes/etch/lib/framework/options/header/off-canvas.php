<?php

LuxeOption::add_section( 'offcanvas_widget', array(
    'title'          => esc_attr__( 'Off Canvas Widget', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header',
) );

LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'offcanvas_widget_width',
    'label'       => esc_attr__( 'Width', 'etch' ),
    'description' => esc_attr__( 'The maximum width of your off canvas widget area on larger screens.  This will automatically be limited to the screen width.', 'etch' ),
    'section'     => 'offcanvas_widget',
    'default'     => '300px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '#sidebar-off-canvas',
            'property' => 'width',
            'media_query' => '@media (min-width: 1200px)'
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '#sidebar-off-canvas',
            'property' => 'width',
            'function' => 'css',
        ),
    ),
    'choices' => array(
        'units' => array( 'px', '%' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'offcanvas_bg_color',
    'label'       => esc_attr__( 'Background Color', 'etch' ),
    'description' => esc_attr__( 'Color behind your off-canvas widget.', 'etch' ),
    'section'     => 'offcanvas_widget',
    'default'     => '#f7f7f7',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '#sidebar-off-canvas',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '#sidebar-off-canvas',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
