<?php

LuxeOption::add_section( 'header_scrolled', array(
    'title'          => esc_attr__( 'Scrolled Header', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'header',
) );

/**
 * Header Scrolled
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'header_scrolled',
    'label'       => esc_attr__( 'Change Header on Scroll', 'etch' ),
    'description' => esc_attr__( 'Change header and style after scrolling.', 'etch' ),
    // 'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => false,
    'priority'    => 10,
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_scrolled_distance',
    'label'       => esc_attr__( 'Header Scrolled Distance', 'etch' ),
    'description' => esc_attr__( 'The distance in pixels until the style of the header changes.', 'etch' ),
    // 'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => 300,
    'priority'    => 10,
    'choices'      => array(
        'min'  => 0,
        'max'  => 1000,
        'step' => 10,
    ),
    'required'    => array(
        array(
            'setting'  => 'header_scrolled',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'header_transition',
    'label'       => esc_attr__( 'Header Transition', 'etch' ),
    'description' => esc_attr__( 'The effect of your header after the page is scrolled.  These use the header scrolled distance and header scrolled styling.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => 'slideDown',
    'priority'    => 10,
    'choices'     => array(
        'none' => esc_attr__( 'None', 'etch' ),
        'slideDown' => esc_attr__( 'Slide Down', 'etch' ),
        'hideBeforeScroll'   => esc_attr__( 'Hide Until Scrolled Down', 'etch' ),
    ),
    'required'    => array(
        array(
            'setting'  => 'header_position',
            'operator' => '==',
            'value'    => 'absolute',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'switch',
    'settings'    => 'header_hide_after_scroll',
    'label'       => esc_attr__( 'Hide Header After Scrolling Down', 'etch' ),
    'description' => esc_attr__( 'With this enabled, you\'re header will hide itself when scrolling down and be revealed after scrolling up 50 pixels.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => false,
    'priority'    => 10,
    'required'    => array(
        array(
            'setting'  => 'header_position',
            'operator' => '==',
            'value'    => 'fixed',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_hide_distance',
    'label'       => esc_attr__( 'Header Hidden Distance', 'etch' ),
    'description' => esc_attr__( 'The distance in pixels until your header uses the transition.', 'etch' ),
    'help'        => esc_attr__( 'If you change your header style on scroll, this is better set before or after that distance to avoid animation clashing.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => 500,
    'priority'    => 10,
    'choices'      => array(
        'min'  => 0,
        'max'  => 1000,
        'step' => 10,
    ),
    'required'    => array(
        array(
            'setting'  => 'header_hide_after_scroll',
            'operator' => '==',
            'value'    => true,
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'settings' => 'logo_scrolled',
    'label'    => esc_attr__( 'Scrolled Logo', 'etch' ),
    'section'  => 'header_scrolled',
    'type'     => 'image',
    'priority' => 10,
    'default'  => '',
    // 'transport'   => 'postMessage',
    // 'js_vars'     => array(
    //     array(
    //         'element'  => 'header .navbar-brand',
    //         'function' => 'customize_preview_js',
    //     ),
    // ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'header_scrolled_bg_color',
    'label'       => esc_attr__( 'Header Scrolled Background Color', 'etch' ),
    'description' => esc_attr__( 'Set the color of your header\'s background after scrolling.', 'etch' ),
    // 'help'        => esc_attr__( 'This is a tooltip', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '#fff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner.header-scrolled',
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'header.banner.header-scrolled',
            'function' => 'css',
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_scrolled_typography_color',
    'label'       => esc_attr__( 'Header Scrolled Font Color', 'etch' ),
    'description' => esc_attr__( 'Set the color of your navigation font after scrolling.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.header-scrolled.banner a, .header-scrolled .nav-btn, header.banner.header-scrolled i.icon',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-scrolled .nav-btn .nav-icon span, .header-scrolled a.btn',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'header.header-scrolled.banner a, .header-scrolled .nav-btn, header.banner.header-scrolled i.icon',
            'function' => 'css',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-scrolled .nav-btn .nav-icon span, .header-scrolled a.btn',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_scrolled_typography_color_hover',
    'label'       => esc_attr__( 'Scrolled Header Font Hover Color', 'etch' ),
    'description' => esc_attr__( 'Set the color of your light header font when hovered.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner.header-scrolled a:hover, header.banner.header-scrolled .nav-btn:hover, .header-scrolled .nav-primary li.current-menu-item a, .header-scrolled a:hover i.icon',
            'property' => 'color',
        ),
        array(
            'element'  => '.header-scrolled .nav-btn:hover .nav-icon span',
            'property' => 'border-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_scrolled_height',
    'label'       => esc_attr__( 'Header Height', 'etch' ),
    'description' => esc_attr__( 'Control the height of your header after scrolling.', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '60',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner.header-scrolled, header.banner.header-scrolled .navbar-nav>li, header.banner.header-scrolled .brand-wrap, .header-scrolled .nav-btn-wrap',
            'property' => 'height',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => 'header.banner.header-scrolled, header.banner.header-scrolled:not(.header-centered-logo-and-nav) .navbar-nav>li, header.banner.header-scrolled .brand-wrap, .header-scrolled .nav-btn-wrap',
            'property' => 'height',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 50,
        'max'  => 300,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_scrolled_logo_padding',
    'label'       => esc_attr__( 'Scrolled Logo Padding', 'etch' ),
    'description' => esc_attr__( 'Padding above your logo after scrolling.', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '15',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-scrolled .navbar-brand>span, .header-scrolled .navbar-brand>img',
            'property' => 'padding-top',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => '.header-scrolled .navbar-brand>span, .header-scrolled .navbar-brand>img',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'header_scrolled_logo_max_height',
    'label'       => esc_attr__( 'Max Logo Height', 'etch' ),
    'description' => esc_attr__( 'Reduce your logo\'s maximum size in pixels after scrolling', 'etch' ),
    'help'        => esc_attr__( 'This setting will take place after saving.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '100',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => '.header-scrolled .navbar-brand>img',
            'property' => 'max-height',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    // 'js_vars'      => array(
    //     array(
    //         'element'  => '.header-scrolled .navbar-brand>img',
    //         'property' => 'max-height',
    //         'units'    => 'px',
    //         'function' => 'css',
    //     ),
    // ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 300,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'header_scrolled_border_width',
    'label'       => esc_attr__( 'Scrolled Header Border Width', 'etch' ),
    'description' => esc_attr__( 'The border between your scrolled header and content.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner.header-scrolled',
            'property' => 'border-width',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'header.banner.header-scrolled',
            'function' => 'css',
            'property' => 'border-width',
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'header_scrolled_border_color',
    'label'       => esc_attr__( 'Scrolled Header Border Color', 'etch' ),
    'description' => esc_attr__( 'Set the color of your scrolled header font when hovered.', 'etch' ),
    'section'     => 'header_scrolled',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => 'header.banner.header-scrolled',
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => 'header.banner.header-scrolled',
            'function' => 'css',
            'property' => 'border-color',
        ),
    ),
) );
