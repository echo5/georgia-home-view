<?php

LuxeOption::add_section( 'forms', array(
    'title'          => esc_attr__( 'Forms', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'body',
) );

$elements = array();
$elements[] = 'input';
$elements[] = 'textarea';
$elements[] = 'select';
$elements[] = '.dummy-keep-at-end-input';

/**
 * Form inputs
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_bg_color',
    'label'       => esc_attr__( 'Input Background Color', 'etch' ),
    'section'     => 'forms',
    'default'     => 'rgba(180,180,180,0.1)',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_bg_color_focus',
    'label'       => esc_attr__( 'Input Focus Background Color', 'etch' ),
    'section'     => 'forms',
    'default'     => 'rgba(180,180,180,0.3)',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':focus, ', $elements),
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'input_border_width',
    'label'       => esc_attr__( 'Input Border Width', 'etch' ),
    'description' => esc_attr__( 'Controls how wide your content is on larger screens.', 'etch' ),
    'help'        => esc_attr__( 'This does not apply to full browser width sections.', 'etch' ),
    'section'     => 'forms',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $elements) . ', ' . implode(':hover, ', $elements),
            'property' => 'border-width',
            'suffix' => '; border-style: solid'
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_border_color',
    'label'       => esc_attr__( 'Input Border Color', 'etch' ),
    'section'     => 'forms',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $elements),
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'input_border_color_focus',
    'label'       => esc_attr__( 'Input Focus Border Color', 'etch' ),
    'section'     => 'forms',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':focus, ', $elements),
            'property' => 'border-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'input_typography',
    'label'       => esc_attr__( 'Input Typography', 'etch' ),
    'description' => esc_attr__( '', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'forms',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '16px',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#333333',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => false,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => implode(', ', $elements),
        ),
    ),
) );
