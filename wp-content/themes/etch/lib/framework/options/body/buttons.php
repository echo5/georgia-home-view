<?php

LuxeOption::add_section( 'buttons', array(
    'title'          => esc_attr__( 'Default Buttons', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
    'panel'          => 'body',
) );

$buttons = array();
$buttons[] = '.button';
$buttons[] = '.btn.btn-default';
$buttons[] = '.btn.btn-primary';
$buttons[] = 'input[type=submit]';
$buttons[] = '.vc_btn3-container .vc_btn3';
$buttons[] = '.vc_btn3.vc_btn3-style-theme-default';
$buttons[] = '.vc_btn3.vc_btn3-style-theme-primary';
$buttons[] = '.woocommerce input.button';
$buttons[] = '.woocommerce #respond input#submit.alt';
$buttons[] = '.woocommerce a.button.alt';
$buttons[] = '.woocommerce button.button.alt';
$buttons[] = '.woocommerce input.button.alt';
$buttons[] = '.woocommerce a.button';
$buttons[] = '.woocommerce button.button.alt:disabled[disabled]';
$buttons[] = '.dummy-keep-at-end-btn';

/**
 * Buttons
 */
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'typography',
    'settings'    => 'button_typography',
    'label'       => esc_attr__( 'Button Typography', 'etch' ),
    'description' => esc_attr__( '', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'buttons',
    'default'     => array(
        'font-style'     => array( 'bold', 'italic' ),
        'font-family'    => 'Roboto',
        'font-size'      => '16px',
        'font-weight'    => '400',
        'line-height'    => '1',
        'letter-spacing' => '0',
        'text-transform' => 'none',
        'color'          => '#ffffff',
    ),
    'priority'    => 10,
    'choices'     => array(
        'font-style'     => true,
        'font-family'    => true,
        'font-size'      => true,
        'font-weight'    => true,
        'line-height'    => false,
        'letter-spacing' => true,
        'units'          => array( 'px', 'rem' ),
    ),
    // 'choices'     => Kirki_Fonts::get_font_choices(),
    'transport' => 'auto',
    'output' => array(
        array(
            'element' => '.vc_btn3.vc_btn3-size-md, ' . implode(', ', $buttons),
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color',
    'settings'    => 'button_text_color_hover',
    'label'       => esc_attr__( 'Button Hover Text Color', 'etch' ),
    'section'     => 'buttons',
    'default'     => '#ffffff',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'color',
            'suffix' => ' !important'
        ),
    ),
    'transport'   => 'postMessage',
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_bg_color',
    'label'       => esc_attr__( 'Button Background Color', 'etch' ),
    'section'     => 'buttons',
    'default'     => '#191919',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'background-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => '.btn.btn-default',
            'property' => 'background-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_bg_color_hover',
    'label'       => esc_attr__( 'Button Hover Background Color', 'etch' ),
    'section'     => 'buttons',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'background-color',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'button_padding_vertical',
    'label'       => esc_attr__( 'Button Height', 'etch' ),
    'description' => esc_attr__( 'Control the padding in pixels above and below your button text.', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'buttons',
    'default'     => '15',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons) . ', input',
            'property' => 'padding-top',
            'units'    => 'px',
            'suffix'    => ' !important',
        ),
        array(
            'element'  => implode(', ', $buttons) . ', input',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'suffix'    => ' !important',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => implode(', ', $buttons) . ', input',
            'property' => 'padding-top',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
        array(
            'element'  => implode(', ', $buttons) . ', input',
            'property' => 'padding-bottom',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 60,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'slider',
    'settings'    => 'button_padding_horizontal',
    'label'       => esc_attr__( 'Button Width', 'etch' ),
    'description' => esc_attr__( 'Control the padding in pixels to the left and right of your button text.', 'etch' ),
    'help'        => esc_attr__( '', 'etch' ),
    'section'     => 'buttons',
    'default'     => '15',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-left',
            'units'    => 'px',
        ),
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-right',
            'units'    => 'px',
        ),
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-left',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'padding-right',
            'units'    => 'px',
            'function' => 'css',
            'suffix'    => ' !important',
        ),
    ),
    'choices'      => array(
        'min'  => 0,
        'max'  => 80,
        'step' => 1,
    )
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'select',
    'settings'    => 'button_radius',
    'label'       => esc_attr__( 'Button Shape', 'etch' ),
    'description' => esc_attr__( 'A simple select (dropdown) control, allowing you to make a single option from a relatively large pool of options.', 'etch' ),
    'help'        => esc_attr__( 'This is a tooltip', 'etch' ),
    'section'     => 'buttons',
    'default'     => '0px',
    'priority'    => 10,
    'choices'     => array(
        ''      => esc_attr__( 'Select Button Shape', 'etch' ),
        '0px'   => esc_attr__( 'Square', 'etch' ),
        '5px' => esc_attr__( 'Rounded', 'etch' ),
        '2em'  => esc_attr__( 'Round', 'etch' ),
    ),
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-radius',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-radius',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'dimension',
    'settings'    => 'button_border_width',
    'label'       => esc_attr__( 'Button Border Width', 'etch' ),
    'description' => esc_attr__( 'Controls how wide your content is on larger screens.', 'etch' ),
    'help'        => esc_attr__( 'This does not apply to full browser width sections.', 'etch' ),
    'section'     => 'buttons',
    'default'     => '0px',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons) . ', ' . implode(':hover, ', $buttons),
            'property' => 'border-width',
            'suffix' => '; border-style: solid'
        ),
    ),
    'choices' => array(
        'units' => array( 'px' )
    ),
    'transport'    => 'postMessage',
    'js_vars'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-width',
            'suffix' => '; border-style: solid'
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_border_color',
    'label'       => esc_attr__( 'Button Border Color', 'etch' ),
    'section'     => 'buttons',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
        ),
    ),
    'transport'   => 'postMessage',
    'js_vars'     => array(
        array(
            'element'  => implode(', ', $buttons),
            'property' => 'border-color',
            'function' => 'css',
        ),
    ),
) );
LuxeOption::add_field( 'luxe_options', array(
    'type'        => 'color-alpha',
    'settings'    => 'button_border_color_hover',
    'label'       => esc_attr__( 'Button Hover Border Color', 'etch' ),
    'section'     => 'buttons',
    'default'     => '#3d3d3d',
    'priority'    => 10,
    'output'      => array(
        array(
            'element'  => implode(':hover, ', $buttons),
            'property' => 'border-color',
        ),
    ),
) );
