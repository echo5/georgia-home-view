<?php
/**
 * Plugin Name: ThemeLuxe Framework & Shortcodes
 * Plugin URI: http://themeluxe.com
 * Description: The framework and shortcodes for ThemeLuxe themes
 * Version: 1.0.1
 * Author: Joshua Flowers
 * Author URI: http://themeluxe.com
 * License: GPL2
 */

/**
 * Luxe Framework Init
 */

define('LUXE_FRAMEWORK_PATH', plugin_dir_path( __FILE__ ) );
define('LUXE_FRAMEWORK_VENDOR_PATH', plugin_dir_path( __FILE__ ) . 'vendor/' );

require_once( LUXE_FRAMEWORK_PATH . 'helper.php'); // Helper
require_once( LUXE_FRAMEWORK_PATH . 'assets.php'); // Scripts and stylesheets
require_once( LUXE_FRAMEWORK_PATH . 'extras.php'); // Custom functions
require_once( LUXE_FRAMEWORK_PATH . 'custom-post-types.php'); // Custom Post Types
require_once( LUXE_FRAMEWORK_PATH . 'elements.php'); // Page elements
require_once( LUXE_FRAMEWORK_PATH . 'wrapper.php'); // Theme wrapper class
require_once( LUXE_FRAMEWORK_PATH . 'customizer.php'); // Theme customizer
require_once( LUXE_FRAMEWORK_PATH . 'header.php'); // Headers
require_once( LUXE_FRAMEWORK_PATH . 'options.php'); // Options
require_once( LUXE_FRAMEWORK_PATH . 'metaboxes.php'); // Options
require_once( LUXE_FRAMEWORK_PATH . 'shortcodes.php'); // Shortcodes
require_once( LUXE_FRAMEWORK_PATH . 'woocommerce.php'); // Woocommerce
require_once( LUXE_FRAMEWORK_PATH . 'importer.php'); // Importer for demo content
