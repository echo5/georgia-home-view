<?php

namespace Luxe\Helper;

/**
 * Get column class
 */
function get_column_class($columns, $size = 'md') {
    switch ($columns) {
        case 1:
            return 'col-' . $size . '-12';
        case 2:
            return 'col-' . $size . '-6';
        case 3:
            return 'col-' . $size . '-4';
        case 4:
            return 'col-' . $size . '-3';
        case 6:
            return 'col-' . $size . '-2';
        case 0:
            return 'col-' . $size . '-12';
    }
}

/**
 * Get responsive classes in descending order
 */
function get_responsive_column_classes($columns, $size = 'lg') {
    $classes = array();
    $sizes = array(
        0 => 'lg',
        1 => 'md',
        2 => 'sm',
    );
    $array_key = array_search($size, $sizes);

    $classes[] = get_column_class($columns, $size);
    foreach ($sizes as $key => $value) {
        if ($key > $array_key) {
            $columns = $columns > 1 ? $columns - 1 : $columns;
            $classes[] = get_column_class($columns, $value);
        }
    }
    $classes[] = 'col-xs-12';
    return implode(' ', $classes);
}

/**
 * Grid array
 * @return array
 */
function get_grid_list($size = 'md') {
    return array(
        array( 'label' => '6', 'value' => '6' ),
        array( 'label' => '4', 'value' => '4' ),
        array( 'label' => '3', 'value' => '3' ),
        array( 'label' => '2', 'value' => '2' ),
        array( 'label' => '1', 'value' => '1' ),
    );
}

/**
 * Get Woocommerce button classes
 */
function get_woocommerce_button_classes() {
    return '.woocommerce #respond input#submit.alt, .woocommerce a.button.alt, .woocommerce button.button.alt, .woocommerce input.button.alt';
}

/**
 * Return actual page ID
 * @return int
 */
function get_post_id() {
    if (is_home()) {
        return get_option( 'page_for_posts' );
    }
    if ( class_exists( 'WooCommerce' ) ) {
        if (is_shop()) {
            return get_option( 'woocommerce_shop_page_id' );
        }
    }
    return get_the_ID();
}

/**
 * Return custom link if inserted
 * @return str
 */
function get_post_link() {
    $custom_link = get_post_meta( get_post_id(), '_post_content_link', true );
    if (!empty($custom_link)) {
        return array(
            'url' => $custom_link,
            'target' => '_blank'
        );
    } else {
        return array(
            'url' => get_the_permalink(),
            'target' => '_self'
        );
    }
}

/**
 * Get a list of available post types
 * @return array
 */
function get_post_types_list() {
    $post_types = get_post_types( array() );
    $post_types_list = array();
    // if ( is_array( $post_types ) && ! empty( $post_types ) ) {
    //     foreach ( $post_types as $post_type ) {
    //         if ( 'revision' !== $post_type && 'nav_menu_item' !== $post_type ) {
    //             $label = ucfirst( $post_type );
    //             $post_types_list[] = array( $post_type, $label );
    //         }
    //     }
    // }
    $post_types_list[] = array( 'post', esc_attr__( 'Post', 'etch' ) );
    $post_types_list[] = array( 'portfolio', esc_attr__( 'Portfolio', 'etch' ) );
    $post_types_list[] = array( 'testimonial', esc_attr__( 'Testimonial', 'etch' ) );
    $post_types_list[] = array( 'product', esc_attr__( 'Product', 'etch' ) );
    return $post_types_list;
}

/**
 * Check if product
 * @return boolean
 */
function is_product() {
    if ( class_exists( 'WooCommerce' ) ) {
        return \is_product();
    }
    return false;
}

/**
 * Check if shop
 * @return boolean
 */
function is_shop() {
    if ( class_exists( 'WooCommerce' ) ) {
        return \is_shop();
    }
    return false;
}

/**
 * Get the Woocommerce product
 * @return boolean
 */
if ( !function_exists('wc_get_product') ) {
    function wc_get_product( $the_product = false, $args = array() ) {
        if (is_woocommerce_active()) {
            return WC()->product_factory->get_product( $the_product, $args );
        }
        return false;
    }
}

/**
 * Check if woocommerce active
 * @return boolean
 */
function is_woocommerce_active() {
    return class_exists( 'WooCommerce' );
}

/**
 * Check if Visual Composer active
 * @return boolean
 */
function is_vc_active() {
     return class_exists( 'Vc_Manager' );
}

/**
 * Get the custom css vc class
 */
function get_vc_class($css, $atts) {
    if (is_vc_active()) {
        return apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, vc_shortcode_custom_css_class( $css, ' ' ), 'luxe_infobox', $atts );
    }
    else {
        return '';
    }
}

/**
 * Build link atts
 */
function build_link($link) {
    if (is_vc_active()) {
        return \vc_build_link($link);
    } else {
        return parse_multi_attribute($link, array('url' => '', 'title' => '', 'target' => ''));
    }

}

/**
 * Parse string like "title:Hello world|weekday:Monday" to array('title' => 'Hello World', 'weekday' => 'Monday')
 *
 * @param $value
 * @param array $default
 *
 * @since 4.2
 * @return array
 */
function parse_multi_attribute( $value, $default = array() ) {
    $result = $default;
    $params_pairs = explode( '|', $value );
    if ( ! empty( $params_pairs ) ) {
        foreach ( $params_pairs as $pair ) {
            $param = preg_split( '/\:/', $pair );
            if ( ! empty( $param[0] ) && isset( $param[1] ) ) {
                $result[ $param[0] ] = rawurldecode( $param[1] );
            }
        }
    }

    return $result;
}


/**
 * Get animations array
 * @return array
 */
function get_animations() {
    return array (
        array('callout.bounce', 'callout.bounce'),
        array('callout.shake', 'callout.shake'),
        array('callout.flash', 'callout.flash'),
        array('callout.pulse', 'callout.pulse'),
        array('callout.swing', 'callout.swing'),
        array('callout.tada', 'callout.tada'),
        array('transition.fadeIn', 'transition.fadeIn'),
        array('transition.fadeOut', 'transition.fadeOut'),
        array('transition.flipXIn', 'transition.flipXIn'),
        array('transition.flipXOut', 'transition.flipXOut'),
        array('transition.flipYIn', 'transition.flipYIn'),
        array('transition.flipYOut', 'transition.flipYOut'),
        array('transition.flipBounceXIn', 'transition.flipBounceXIn'),
        array('transition.flipBounceXOut', 'transition.flipBounceXOut'),
        array('transition.flipBounceYIn', 'transition.flipBounceYIn'),
        array('transition.flipBounceYOut', 'transition.flipBounceYOut'),
        array('transition.swoopIn', 'transition.swoopIn'),
        array('transition.swoopOut', 'transition.swoopOut'),
        array('transition.whirlIn', 'transition.whirlIn'),
        array('transition.whirlOut', 'transition.whirlOut'),
        array('transition.shrinkIn', 'transition.shrinkIn'),
        array('transition.shrinkOut', 'transition.shrinkOut'),
        array('transition.expandIn', 'transition.expandIn'),
        array('transition.expandOut', 'transition.expandOut'),
        array('transition.bounceIn', 'transition.bounceIn'),
        array('transition.bounceOut', 'transition.bounceOut'),
        array('transition.bounceUpIn', 'transition.bounceUpIn'),
        array('transition.bounceUpOut', 'transition.bounceUpOut'),
        array('transition.bounceDownIn', 'transition.bounceDownIn'),
        array('transition.bounceDownOut', 'transition.bounceDownOut'),
        array('transition.bounceLeftIn', 'transition.bounceLeftIn'),
        array('transition.bounceLeftOut', 'transition.bounceLeftOut'),
        array('transition.bounceRightIn', 'transition.bounceRightIn'),
        array('transition.bounceRightOut', 'transition.bounceRightOut'),
        array('transition.slideUpIn', 'transition.slideUpIn'),
        array('transition.slideUpOut', 'transition.slideUpOut'),
        array('transition.slideDownIn', 'transition.slideDownIn'),
        array('transition.slideDownOut', 'transition.slideDownOut'),
        array('transition.slideLeftIn', 'transition.slideLeftIn'),
        array('transition.slideLeftOut', 'transition.slideLeftOut'),
        array('transition.slideRightIn', 'transition.slideRightIn'),
        array('transition.slideRightOut', 'transition.slideRightOut'),
        array('transition.slideUpBigIn', 'transition.slideUpBigIn'),
        array('transition.slideUpBigOut', 'transition.slideUpBigOut'),
        array('transition.slideDownBigIn', 'transition.slideDownBigIn'),
        array('transition.slideDownBigOut', 'transition.slideDownBigOut'),
        array('transition.slideLeftBigIn', 'transition.slideLeftBigIn'),
        array('transition.slideLeftBigOut', 'transition.slideLeftBigOut'),
        array('transition.slideRightBigIn', 'transition.slideRightBigIn'),
        array('transition.slideRightBigOut', 'transition.slideRightBigOut'),
        array('transition.perspectiveUpIn', 'transition.perspectiveUpIn'),
        array('transition.perspectiveUpOut', 'transition.perspectiveUpOut'),
        array('transition.perspectiveDownIn', 'transition.perspectiveDownIn'),
        array('transition.perspectiveDownOut', 'transition.perspectiveDownOut'),
        array('transition.perspectiveLeftIn', 'transition.perspectiveLeftIn'),
        array('transition.perspectiveLeftOut', 'transition.perspectiveLeftOut'),
        array('transition.perspectiveRightIn', 'transition.perspectiveRightIn'),
        array('transition.perspectiveRightOut', 'transition.perspectiveRightOut'),
    );
}
