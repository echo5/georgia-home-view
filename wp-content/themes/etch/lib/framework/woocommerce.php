<?php

namespace Luxe\Woocommerce;

use Luxe\Assets;
use Luxe\Extras;
use Luxe\Helper;

/**
 * Add Woocommerce support
 */
function setup()
{
    // Add Woocommerce support
    // https://docs.woothemes.com/document/third-party-custom-theme-compatibility/
    add_theme_support('woocommerce');

    // Image sizes
    add_image_size('shop_catalog', 480, 600, true);

}
add_action('after_setup_theme', __NAMESPACE__ . '\\setup');

/**
 * Turn off woocommerce page title in favor of theme page header
 * @return bool
 */
function override_page_title()
{
    return false;
}
add_filter('woocommerce_show_page_title', __NAMESPACE__ . '\\override_page_title');

/**
 * Add cart contents to header elements
 * @param  $fragments
 * @return $fragments
 */
function woocommerce_header_add_to_cart_fragment($fragments)
{
    ob_start();
    ?>
    <div class="cart-contents">
        <sup class="cart-count"><?php echo WC()->cart->get_cart_contents_count(); ?></sup>
        <span class="cart-total"><?php echo WC()->cart->get_cart_total(); ?></span>
    </div>
    <?php
$fragments['div.cart-contents'] = ob_get_clean();
    return $fragments;
}
add_filter('woocommerce_add_to_cart_fragments', __NAMESPACE__ . '\\woocommerce_header_add_to_cart_fragment');

/**
 * Remove unused woocommerce scripts
 */
function remove_unused_woocommerce_scripts()
{
    if (Helper\is_woocommerce_active()) {
        wp_dequeue_style('woocommerce_prettyPhoto_css');
        wp_dequeue_script('prettyPhoto');
        wp_dequeue_script('prettyPhoto-init');
    }
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\remove_unused_woocommerce_scripts', 99);

/**
 * Replace rogue runaway cart fragment script
 * Bug in Woocommerce fragments script that bogs down server
 */
function replace_cart_fragments_script()
{
    if (Helper\is_woocommerce_active()) {
        wp_dequeue_script('wc-cart-fragments');
        wp_enqueue_script('luxe-cart-fragments', Assets\luxe_asset_path('scripts/cart-fragments.js'), array('jquery', 'jquery-cookie'), null, true);

        $handle = 'luxe-cart-fragments';
        $name = 'luxe_cart_fragments_params';
        $data = array(
            'ajax_url' => \WC()->ajax_url(),
            'wc_ajax_url' => \WC_AJAX::get_endpoint("%%endpoint%%"),
            'fragment_name' => apply_filters('woocommerce_cart_fragment_name', 'wc_fragments'),
        );

        wp_localize_script($handle, $name, apply_filters($name, $data));

    }
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\replace_cart_fragments_script', 99);

/**
 * Add cart sidebar if active
 */
function add_woocommerce_sidebar()
{
    if (Helper\is_woocommerce_active() && get_theme_mod('header_cart', false)) {
        ob_start();
        ?>
        <div id="sidebar-cart" class="sidebar off-canvas off-canvas-right sidebar-off-canvas sidebar-cart" data-move-x="100%" data-move-y="" data-active-class="sidebar-cart-active" data-content-push="-200px">
            <div class="close-btn toggle-off-canvas" data-offcanvas-target="#sidebar-cart"><i class="icon ion-close-round"></i></div>
            <?php the_widget('WC_Widget_Cart');?>
        </div>
        <?php
$output = ob_get_clean();
        echo $output;
    }
}
add_action('luxe_after_header', __NAMESPACE__ . '\\add_woocommerce_sidebar', 10);

/**
 * Add class to products with multiple images
 * @param  array $classes
 * @return array $classes
 */
function add_product_gallery_class($classes)
{
    global $product;
    $post_type = get_post_type(get_the_ID());
    if (!is_admin()) {
        if ($post_type == 'product') {

            $attachment_ids = $product->get_gallery_attachment_ids();
            if ($attachment_ids) {
                $classes[] = 'product-has-gallery';
            }
        }
    }
    return $classes;
}
add_filter('post_class', __NAMESPACE__ . '\\add_product_gallery_class');

/**
 * Add second image to products
 */
function add_second_product_image()
{
    global $product, $woocommerce;
    // global $image_size;
    $image_size = isset($image_size) ? $image_size : 'shop_catalog';
    $attachment_ids = $product->get_gallery_attachment_ids();
    if ($attachment_ids) {
        $secondary_image_id = $attachment_ids['0'];
        echo wp_get_attachment_image($secondary_image_id, $image_size, '', $attr = array('class' => 'secondary-image attachment-shop-catalog'));
    }
}
add_action('woocommerce_before_shop_loop_item_title', __NAMESPACE__ . '\\add_second_product_image', 11);

/**
 * Replace hex values with color swatches
 * @param [type] $option [description]
 */
function add_color_swatch_to_variation($option)
{
    $color = '#' . substr($option, strpos($option, "#") + 1);
    // Check for hex color
    if (!is_admin() && class_exists('WC_Radio_Buttons')) {
        if (preg_match('/^#[a-f0-9]{6}$/i', $color)) {
            $option = '<span class="color-swatch"><span class="color-swatch-bg" style="background:' . $color . '"></span></span>';
        } else {
            $option = '<span>' . $option . '</span>';
        }
    }
    return $option;
}
add_filter('woocommerce_variation_option_name', __NAMESPACE__ . '\\add_color_swatch_to_variation');

/**
 * Remove single product summary and clean up product page
 */
if (get_theme_mod('shop_hide_kitchen_sink', false)) {
    remove_all_actions('woocommerce_after_single_product_summary');
}

/**
 * Wrap currency symbol for styling
 */
function wrap_currency_symbol($format, $currency_pos)
{

    $format = '<span class="currency-symbol">%1$s</span>%2$s';
    return $format;
}
add_action('woocommerce_price_format', __NAMESPACE__ . '\\wrap_currency_symbol', 1, 2);

/**
 * Change related product column and limit
 */
function change_related_products_limit($args)
{
    $args['posts_per_page'] = 3;
    $args['columns'] = 3;
    return $args;
}
add_filter('woocommerce_output_related_products_args', __NAMESPACE__ . '\\change_related_products_limit');
