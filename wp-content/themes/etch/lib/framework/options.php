<?php

namespace Luxe\Options;

use Luxe\Assets;

require_once( LUXE_FRAMEWORK_PATH . 'options/options-override.php' );
require_once( LUXE_FRAMEWORK_PATH . 'options/include-kirki.php' );
require_once( LUXE_FRAMEWORK_PATH . 'options/luxe-option.php' );

/**
 * Add the configuration.
 * This way all the fields using the 'luxe_options' ID
 * will inherit these options
 */
\LuxeOption::add_config( 'luxe_options', array(
    'capability'    => 'edit_theme_options',
    'option_type'   => 'theme_mod',
) );

\LuxeOption::add_section( 'checkbox', array(
    'title'          => esc_attr__( 'Checkbox Controls', 'etch' ),
    'priority'       => 1,
    'capability'     => 'edit_theme_options',
) );
/**
 * Add sections
 */
require_once( LUXE_FRAMEWORK_PATH . 'options/header/base.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/header/dark.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/header/light.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/header/scrolled.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/header/navigation.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/header/buttons.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/header/off-canvas.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body/typography.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body/page-headers.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body/buttons.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body/buttons-primary.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body/forms.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/body/sidebars.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/footer.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/post-types.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/preloader.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/woocommerce.php');
require_once( LUXE_FRAMEWORK_PATH . 'options/advanced.php');

/**
 * Insert custom css
 */
function add_user_custom_css() {
    $css = get_theme_mod( 'custom_css', false );
    wp_enqueue_style( 'luxe-user-custom', get_template_directory_uri() . '/custom.css');
    wp_add_inline_style('luxe-user-custom', $css);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\add_user_custom_css', 110);

/**
 * Insert custom js
 */
function add_user_custom_js() {
    $js = get_theme_mod( 'custom_js', false );
    if ($js) {
        echo '<script typed="text/javascript">' . $js . '</script>';
    }
}
add_action('wp_head', __NAMESPACE__ . '\\add_user_custom_js', 110);

/**
 * Insert custom scripts in head
 */
function add_user_custom_head_scripts() {
    $scripts = get_theme_mod( 'custom_head_scripts', false );
    if ($scripts) {
        $allowed_html = array(
            'script' => array(
                'src' => array(),
                'type' => array(),
            ),
            'link' => array(
                'rel' => array(),
                'type' => array(),
                'href' => array(),
            ),
        );
        echo wp_kses($scripts, $allowed_html);
    }
}
add_action('wp_head', __NAMESPACE__ . '\\add_user_custom_head_scripts');

/**
 * Disable Kirki loader
 */
add_filter( 'kirki/config', function( $config ) {
    $config['disable_loader'] = true;
    return $config;
} );

/**
 * Add admin css styles to customizer
 */
function custom_customize_enqueue() {
    wp_enqueue_style('luxe-admin', Assets\luxe_asset_path('styles/customizer.css'));
}
add_action( 'customize_controls_enqueue_scripts', __NAMESPACE__ . '\\custom_customize_enqueue' );

/**
 * Script data
 */
function localize_script_data()
{
    $script_data = array(
        'headerScrolled' => get_theme_mod('header_scrolled', false),
        'headerScrolledDistance' => get_theme_mod('header_scrolled_distance', 300),
        'headerHideAfterScroll' => get_theme_mod('header_hide_after_scroll', false),
        'headerHideDistance' => get_theme_mod('header_hide_distance', 500),
        'headerTransition' => get_theme_mod('header_transition', 'hideAfterScroll'),
        'headerPosition' => get_theme_mod('header_position', 80),
        'headerHeight' => get_theme_mod('header_height', 80),
        'headerScrolledHeight' => get_theme_mod('header_scrolled_height', 60),
        'smoothScroll' => get_theme_mod('smooth_scroll', false),
        'preloader' => get_theme_mod('preloader', true),
        'openCartOnAjaxAdd' => get_theme_mod('open_cart_on_ajax_add', true),
        'fixedFooter' => get_theme_mod('footer_fixed', true),
    );
    wp_localize_script('luxe_main', 'theme', $script_data);
}
add_action('wp_enqueue_scripts', __NAMESPACE__ . '\\localize_script_data', 101);
