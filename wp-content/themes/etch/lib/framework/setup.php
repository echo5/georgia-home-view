<?php

namespace Etch\Setup;

use Luxe\Assets;
use Luxe\Helper;

/**
 * Theme setup
 */
function etch_setup() {
  // Make theme available for translation
  load_theme_textdomain('etch', get_template_directory() . '/lang');

  // Enable plugins to manage the document title
  // http://codex.wordpress.org/Function_Reference/add_theme_support#Title_Tag
  add_theme_support('title-tag');

  // Register wp_nav_menu() menus
  // http://codex.wordpress.org/Function_Reference/register_nav_menus
  register_nav_menus(array(
    'primary_navigation' => esc_html__('Primary Navigation', 'etch')
  ));

  // Enable post thumbnails
  // http://codex.wordpress.org/Post_Thumbnails
  // http://codex.wordpress.org/Function_Reference/set_post_thumbnail_size
  // http://codex.wordpress.org/Function_Reference/add_image_size
  add_theme_support('post-thumbnails');
  add_image_size( 'etch_blog', 800, 300, array( 'center', 'center' ) );
  add_image_size( 'etch_testimonial', 85, 85, true );

  // Enable post formats
  // http://codex.wordpress.org/Post_Formats
  add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio'));

  // Enable HTML5 markup support
  // http://codex.wordpress.org/Function_Reference/add_theme_support#HTML5
  add_theme_support('html5', array('caption', 'comment-form', 'comment-list', 'gallery', 'search-form'));

  // Automatic feed links
  add_theme_support( 'automatic-feed-links' );

  // Set content width
  if ( ! isset( $content_width ) ) $content_width = 1170;

}
add_action('after_setup_theme', __NAMESPACE__ . '\\etch_setup');

/**
 * Register sidebars
 */
function etch_widgets_init() {
    register_sidebar(array(
      'name'          => esc_html__('Primary', 'etch'),
      'id'            => 'sidebar-primary',
      'before_widget' => '<div class="widget %1$s %2$s">',
      'after_widget'  => '</div>',
      'before_title'  => '<h3>',
      'after_title'   => '</h3>',
      'description'   => esc_html__('The primary sidebar used on posts and pages if active in the customizer.', 'etch'),
    ));
    
  register_sidebar(array(
    'name'          => esc_html__('Header Widgets', 'etch'),
    'id'            => 'sidebar-header',
    'before_widget' => '<div class="widget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
    'description'   => esc_html__('The widget area in the header/navigation area if the header used has a widgetized area.', 'etch'),
  ));

  register_sidebar(array(
    'name'          => esc_html__('Off Canvas', 'etch'),
    'id'            => 'sidebar-off-canvas',
    'before_widget' => '<div class="widget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
    'description'   => esc_html__('The widget that sits off-screen and can slide out using one of the header button options available in the customizer.', 'etch'),
  ));

  register_sidebar(array(
    'name'          => esc_html__('Modal', 'etch'),
    'id'            => 'sidebar-modal',
    'before_widget' => '<div class="widget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
    'description'   => esc_html__('A modal window for custom content that can be called by one of the header button options available in the customizer.', 'etch'),
  ));

  register_sidebar(array(
    'name'          => esc_html__('Footer Copy', 'etch'),
    'id'            => 'sidebar-footer',
    'before_widget' => '<div class="widget %1$s %2$s">',
    'after_widget'  => '</div>',
    'before_title'  => '<h3>',
    'after_title'   => '</h3>',
    'description'   => esc_html__('The footer copy area beneath your footer widgets.', 'etch'),
  ));

    $footer_columns = get_theme_mod('footer_columns', 3);
    if ($footer_columns >= 1) {
      register_sidebar(array(
        'name'          => esc_html__('Footer Column 1', 'etch'),
        'id'            => 'sidebar-footer-1',
        'before_widget' => '<div class="widget %1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'description'   => esc_html__('The 1st footer column.', 'etch'),
      ));
    }
    if ($footer_columns >= 2) {
      register_sidebar(array(
        'name'          => esc_html__('Footer Column 2', 'etch'),
        'id'            => 'sidebar-footer-2',
        'before_widget' => '<div class="widget %1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'description'   => esc_html__('The 2nd footer column.', 'etch'),
      ));
    }
    if ($footer_columns >= 3) {
      register_sidebar(array(
        'name'          => esc_html__('Footer Column 3', 'etch'),
        'id'            => 'sidebar-footer-3',
        'before_widget' => '<div class="widget %1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'description'   => esc_html__('The 3rd footer column.', 'etch'),
      ));
    }
    if ($footer_columns >= 4) {
      register_sidebar(array(
        'name'          => esc_html__('Footer Column 4', 'etch'),
        'id'            => 'sidebar-footer-4',
        'before_widget' => '<div class="widget %1$s %2$s">',
        'after_widget'  => '</div>',
        'before_title'  => '<h3>',
        'after_title'   => '</h3>',
        'description'   => esc_html__('The 4th footer column.', 'etch'),
      ));
    }
}
add_action('widgets_init', __NAMESPACE__ . '\\etch_widgets_init');

/**
 * Add in post meta to page header on single posts
 */
function etch_page_header_add_post_meta() {
    if (is_single()) {
        echo '<div class="post-date h5">' .get_the_date('d F Y'). '</div>';
        echo '<div class="post-categories h6">';
        the_tags( '#', ' #' );
        echo '</div>';
    }
}
add_action( 'luxe_after_page_title', __NAMESPACE__ . '\\etch_page_header_add_post_meta' );
