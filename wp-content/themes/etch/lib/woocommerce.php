<?php

namespace Luxe\Woocommerce;

use Luxe\Assets;
use Luxe\Extras;
use Luxe\Helper;


/**
 * Move add to cart button before title and link it
 */
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart');
add_action('woocommerce_before_shop_loop_item_title', 'woocommerce_template_loop_add_to_cart', 5);
remove_action('woocommerce_before_shop_loop_item', 'woocommerce_template_loop_product_link_open');
add_action('woocommerce_shop_loop_item_title', 'woocommerce_template_loop_product_link_open', 5);

/**
 * Move rating after price
 */
remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 15);
remove_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 10);
add_action('woocommerce_single_product_summary', 'woocommerce_template_single_rating', 15);

/**
 * Add overlay link for images
 */
function etch_product_link()
{
    global $product;
    echo '<a class="overlay-link" href="' . get_permalink($product->ID) . '"></a>';
}
add_action('woocommerce_before_shop_loop_item_title', __NAMESPACE__ . '\\etch_product_link', 5);

/**
 * Add in social share icons
 */
function etch_add_social_share()
{
    if (is_single()) {
        echo '<div class="social-icons-wrap">';
        echo Extras\social_share(get_the_ID(), get_the_title(), true);
        echo '</div>';
    }
}
add_action('woocommerce_single_product_summary', __NAMESPACE__ . '\\etch_add_social_share', 50);

/**
 * Remove breadcrumbs
 */
function etch_remove_breadcrumbs()
{
	remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );

}
add_action( 'init', __NAMESPACE__ . '\\etch_remove_breadcrumbs' );

