<?php

use Luxe\Assets;

function etch_import_files() {

	/**
	 * Demo options
	 */
	$demos = array(
		'salon' => array(
			'name' => 'Salon',
		),
		'boutique' => array(
			'name' => 'Boutique',
		),
		'cafe' => array(
			'name' => 'Cafe',
		),
		'personal' => array(
			'name' => 'Personal',
		),
		'restaurant' => array(
			'name' => 'Restaurant',
		),
		'interior' => array(
			'name' => 'Interior',
		),
		'agency' => array(
			'name' => 'Agency',
		),
		'application' => array(
			'name' => 'Application',
		),
		'photography' => array(
			'name' => 'Photography',
		),
		'flat' => array(
			'name' => 'Flat',
		),
		'business' => array(
			'name' => 'Business',
		),
		'store' => array(
			'name' => 'Store',
		),
	);

	$importer_array = array();

	foreach ($demos as $key=>$demo) {
		$importer_array[] = array(
		    'import_file_name'             => $demo['name'],
		    'local_import_file'            => trailingslashit( get_template_directory() ) . 'lib/demo-files/'. $key . '_content.xml',
		    'local_import_widget_file'     => trailingslashit( get_template_directory() ) . 'lib/demo-files/'. $key . '_widgets.wie',
		    'local_import_customizer_file' => trailingslashit( get_template_directory() ) . 'lib/demo-files/'. $key . '_options.dat',
		    'import_preview_image_url'     => Assets\luxe_asset_path('images/demos') . '/' . $key . '.png',
		);
	}

	return $importer_array;

}
add_filter( 'pt-ocdi/import_files', 'etch_import_files' );
