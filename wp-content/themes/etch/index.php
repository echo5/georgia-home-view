<?php use Luxe\Helper; ?>
<?php if (!have_posts()) : ?>
    <div class="alert alert-warning">
        <?php esc_html_e('Sorry, no results were found.', 'etch'); ?>
    </div>
    <?php get_search_form(); ?>
<?php endif; ?>

<div class="row">
	<?php while (have_posts()) : the_post(); ?>
		<div class="<?php echo Helper\get_responsive_column_classes(get_theme_mod('blog_columns', '1')); ?>">
		    <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
	    </div>
	<?php endwhile; ?>
</div>

<?php the_posts_navigation(); ?>
