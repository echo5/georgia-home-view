<?php

/**
 * Add image sizes
 */
add_image_size( 'option', 200, 140 );

/**
 * Enqueue child theme styles
 */
function ghv_enqueue_scripts_and_styles() {
    wp_enqueue_style('ghv',
        get_stylesheet_directory_uri() . '/style.css'
    );
    wp_enqueue_script('ghv',
        get_stylesheet_directory_uri() . '/scripts.js'
    );
}
add_action('wp_enqueue_scripts', 'ghv_enqueue_scripts_and_styles', 1000);

/**
 * Admin scripts
 */
function ghv_admin_scripts_and_styles($hook) {
    wp_enqueue_script( 'ghv-admin', get_stylesheet_directory_uri() . '/admin.js' );
    global $post;
    wp_localize_script('ghv-admin', 'post', array(
            'post_id' => $post->ID
        )
    );
}
add_action( 'admin_enqueue_scripts', 'ghv_admin_scripts_and_styles' );

/**
 * Delete transients for bug in WC Bookings resources
 */
function ghv_delete_query_transient_on_update( $post ) {
    delete_transient( 'book_ress_' . md5( http_build_query( array( $post, WC_Cache_Helper::get_transient_version( 'bookings' ) ) ) ) );
}
add_action( 'save_post', 'ghv_delete_query_transient_on_update' );

function ghv_add_base_duration_to_product_page() {
	global $product;
	$base_duration = $product->get_attribute( 'Base Duration' );
	$base_duration = $base_duration ? $base_duration : '0';
	echo '<input type="hidden" id="duration-base" value="'.$base_duration.'">';
}
add_action('woocommerce_single_product_summary', 'ghv_add_base_duration_to_product_page');

/**
 * WC change items per row
 */
function ghv_loop_columns() {
	return 4;
}
add_filter('loop_shop_columns', 'ghv_loop_columns');

/**
 * WC addons change order
 */
// remove_action( 'woocommerce_before_add_to_cart_button', array( $GLOBALS['Product_Addon_Display'], 'display' ) );
// add_action( 'woocommerce_before_add_to_cart_form', array( $GLOBALS['Product_Addon_Display'], 'display' ), 1 );

/**
 * WC add "From" to price
 */
function ghv_price_html( $price, $product ){
    return 'From: ' . $price ;
}
add_filter( 'woocommerce_get_price_html', 'ghv_price_html', 100, 2 );


/**
 * Add custom addon field
 */
function ghv_add_checkbox_image_field($post, $product_addons, $loop, $option) {
    wp_enqueue_media();
    ob_start();
    ?>
    <td class="checkbox_column">
        <input type="hidden" name="product_addon_option_image[<?php echo $loop; ?>][]" value="<?php echo esc_attr( $option['image']  ); ?>" class="image_attachment_id" />
        <?php if (is_numeric($option['image'])) { 
            $image_src = wp_get_attachment_image_src($option['image']);
            ?>
            <img class="image-preview" src="<?php echo $image_src[0]; ?>" width="60" height="60" style="max-height: 60px; width: 60px;">
        <?php } ?>
        <input type="button" class="button upload_image_button" value="<?php _e( 'Upload image' ); ?>" />
    </td>
    <td class="checkbox_column">
        <input type="text" name="product_addon_option_dependee[<?php echo $loop; ?>][]" value="<?php echo esc_attr( $option['dependee']  ); ?>" />
    </td>
    <?php
    $output = ob_get_clean();
    echo $output;

}
add_action('woocommerce_product_addons_panel_option_row', 'ghv_add_checkbox_image_field', 10, 4);

/**
 * Add checkbox headings to addon fields
 */
function ghv_add_checkbox_heading_fields($post, $addon, $loop) {
    echo '<th class="checkbox_column"><span class="column-title">Image</span></th>';
    echo '<th class="checkbox_column"><span class="column-title">Dependee</span></th>';
}
add_action('woocommerce_product_addons_panel_option_heading', 'ghv_add_checkbox_heading_fields', 10, 3);

/**
 * Save custom addon field
 */
function ghv_save_checkbox_image_field($data, $i) {
    $addon_option_image = $_POST['product_addon_option_image'][$i];
    $addon_option_dependee = $_POST['product_addon_option_dependee'][$i];
    for ( $ii = 0; $ii < sizeof( $data['options'] ); $ii++ ) {
        $image    = sanitize_text_field( stripslashes( $addon_option_image[ $ii ] ) );
        $dependee    = sanitize_text_field( stripslashes( $addon_option_dependee[ $ii ] ) );
        $data['options'][$ii]['image'] = $image;
        $data['options'][$ii]['dependee'] = $dependee;
    }
    return $data;
}
add_filter('woocommerce_product_addons_save_data', 'ghv_save_checkbox_image_field', 10, 2);

