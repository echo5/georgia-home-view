(function ($) {

	/**
	 * Update duration field on select change
	 */
	$( document ).ready(function() {

		var baseDuration = parseInt($('#duration-base').val());

		$('.duration-select').on('change', function() {
			var duration = $(this).find(':selected').data('duration');
			$(this).attr('data-duration-total', duration);
			calculateTotalDuration();
		});

		$('.duration-input').on('change', function() {
			var value = $(this).val();
			if (value >= 0) {
				var durationMultiplier = $(this).data('duration-multiplier');
				var per = $(this).data('duration-per');
				duration = Math.ceil(value/per) * durationMultiplier;
				$(this).attr('data-duration-total', duration);
				calculateTotalDuration();
			}
		});

		$('.duration-checkbox').on('change', function() {
			if ($(this).is(":checked")) {
				var duration = $(this).data('duration');
			} else {
				var duration = 0;
			}
			$(this).attr('data-duration-total', duration);
			calculateTotalDuration();
		});

		function calculateTotalDuration() {
			duration = baseDuration;
			$('.duration-select, .duration-input, .duration-checkbox').each(function() {
				inputDuration = parseInt($(this).attr('data-duration-total'));
				if (inputDuration >= 0) {
					duration += inputDuration;
				}
			});
			if (duration >= 0) {
				$('#wc_bookings_field_duration').val(duration);
			}
		}
	});

	/**
	 * Addon dependencies
	 */
	 $( document ).ready(function() {
	 	$('[data-dependee]').on('change', function() {
	 		var dependee = $($(this).data('dependee'));
	 		if($(this).is(":checked")) {
	 		    dependee.show();
	 		} else {
	 		    dependee.hide();
	 		}
	 	});
	 });

})(jQuery);