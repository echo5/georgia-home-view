<?php foreach ( $addon['options'] as $i => $option ) :

	$price = apply_filters( 'woocommerce_product_addons_option_price',
		$option['price'] > 0 ? '(' . wc_price( get_product_addon_price_for_display( $option['price'] ) ) . ')' : '',
		$option,
		$i,
		'checkbox'
	);

	$selected = isset( $_POST[ 'addon-' . sanitize_title( $addon['field-name'] ) ] ) ? $_POST[ 'addon-' . sanitize_title( $addon['field-name'] ) ] : array();
	if ( ! is_array( $selected ) ) {
		$selected = array( $selected );
	}

	// Duration
	$duration = 0;
	if (strpos($option['label'], '|') !== false) {
		$option_label = explode('|', $option['label']);
		$duration = $option_label[1];
		$duration_class = 'duration-checkbox';
	} else {
		$option_label[0] = $option['label'];
	}

	$current_value = ( in_array( sanitize_title( $option['label'] ), $selected ) ) ? 1 : 0;

	// Image label
	$image = $image_class = null;
	if (is_numeric($option['image'])) {
		$image = wp_get_attachment_image($option['image'], 'option');
		$image_class = 'addon-has-image';
	}

	// Dependee
	$dependee = '';
	if (!empty(trim($option['dependee']))) {
		$dependee = 'data-dependee="' . $option['dependee'] .'"';
	}
	?>


	<p class="form-row form-row-wide addon-wrap-<?php echo sanitize_title( $addon['field-name'] ) . '-' . $i; ?> <?php echo $image_class; ?>">
		<input type="checkbox" id="<?php echo sanitize_title( $option['label'] ); ?>" class="addon addon-checkbox <?php echo isset($duration_class) ? $duration_class: ''; ?>" data-duration="<?php echo $duration; ?>" name="addon-<?php echo sanitize_title( $addon['field-name'] ); ?>[]" data-raw-price="<?php echo esc_attr( $option['price'] ); ?>" data-price="<?php echo get_product_addon_price_for_display( $option['price'] ); ?>" <?php echo $dependee; ?> value="<?php echo sanitize_title( $option['label'] ); ?>" <?php checked( $current_value, 1 ); ?> />
		<label for="<?php echo sanitize_title( $option['label'] ); ?>">
			<?php echo $image; ?>
			<span class="label-desc"><?php echo wptexturize( $option_label[0] . ' ' . $price ); ?></span>
		</label>
	</p>

<?php endforeach; ?>
