<?php
if ( ! defined( 'ABSPATH' ) ) {
	die( '-1' );
}

/**
 * Shortcode attributes
 * @var $atts
 * @var $el_class
 * @var $full_width
 * @var $full_height
 * @var $equal_height
 * @var $columns_placement
 * @var $content_placement
 * @var $parallax
 * @var $parallax_image
 * @var $css
 * @var $el_id
 * @var $video_bg
 * @var $video_bg_url
 * @var $video_bg_parallax
 * @var $content - shortcode content
 * Shortcode class
 * @var $this WPBakeryShortCode_VC_Row
 */
$el_class = $full_height = $full_width = $equal_height = $flex_row = $columns_placement = $content_placement = $parallax = $parallax_image = $css = $el_id = $video_bg = $video_bg_url = $video_bg_parallax = '';
$output = $after_output = '';
$atts = vc_map_get_attributes( $this->getShortcode(), $atts );
extract( $atts );

wp_enqueue_script( 'wpb_composer_front_js' );

$el_class = $this->getExtraClass( $el_class );

$container_classes = array();
$container_attributes = array();
// $container_classes[] = 'outer-container';
$inner_container_classes = array();
$inner_container_classes[] = 'inner-container';

$css_classes = array(
	'vc_row',
	'wpb_row', //deprecated
	'vc_row-fluid',
	$el_class,
	vc_shortcode_custom_css_class( $css ),
);

if (vc_shortcode_custom_css_has_property( $css, array('border', 'background') ) || $video_bg || $parallax) {
	$css_classes[]='vc_row-has-fill';
}

if (!empty($atts['gap'])) {
	$css_classes[] = 'vc_column-gap-'.$atts['gap'];
}

$wrapper_attributes = array();
// build attributes for wrapper
if ( ! empty( $el_id ) ) {
	$wrapper_attributes[] = 'id="' . esc_attr( $el_id ) . '"';
}
if ( ! empty( $full_width ) ) {
	// $wrapper_attributes[] = 'data-vc-full-width="true"';
	// $wrapper_attributes[] = 'data-vc-full-width-init="false"';
    $container_classes[] = 'container-fluid';
    if ( 'stretch_row_content' === $full_width ) {
        $inner_container_classes[] = 'container-fluid'; 
	} elseif ( 'stretch_row_content_no_spaces' === $full_width ) {
		// $container_classes[] = 'no-padding';
	} else {
        $inner_container_classes[] = 'container';
    }
	// $after_output .= '<div class="vc_row-full-width"></div>';
} else {
    $container_classes[] = 'container';
}

if ( ! empty( $animate ) && !empty( $animation ) ) {
	$container_classes[] = 'has-animation';
	$container_attributes[] = 'data-animation="' . $animation . '"';
}

if ( ! empty( $full_height ) ) {
	$css_classes[] = ' full-browser-height';
	if ( ! empty( $columns_placement ) ) {
		$flex_row = true;
		$css_classes[] = ' vc_row-o-columns-' . $columns_placement;
	}
}

if ( ! empty( $equal_height ) ) {
	$flex_row = true;
	$css_classes[] = ' vc_row-o-equal-height';
}

if ( ! empty( $content_placement ) ) {
	$flex_row = true;
	$css_classes[] = ' vc_row-o-content-' . $content_placement;
}

if ( ! empty( $flex_row ) ) {
	$css_classes[] = ' vc_row-flex';
}

// $has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_bg_url ) && vc_extract_youtube_id( $video_bg_url ) );
$has_video_bg = ( ! empty( $video_bg ) && ! empty( $video_iframe ) );

if ( $has_video_bg ) {
	$parallax = $video_bg_parallax;
	$parallax_image = rawurldecode(base64_decode(strip_tags($video_iframe)));
	// $css_classes[] = ' vc_video-bg-container';
	// wp_enqueue_script( 'vc_youtube_iframe_api_js' );
}

if ( ! empty( $parallax ) ) {
    $css_classes[] = 'fixed-bg'; 
	if ( false !== strpos( $parallax, 'fade' ) ) {
        $css_classes[] = 'fade-on-scroll'; 
	} elseif ( false !== strpos( $parallax, 'fixed' ) ) {
	}
}

$parallax_bg = '';
if ( ! empty( $parallax_image ) ) {
	if ( $has_video_bg ) {
		$parallax_image_src = $parallax_image;
        $parallax_bg = '<div class="video-bg"><div class="video-container">';
        $parallax_bg .= $parallax_image_src;
        $parallax_bg .= '</div></div>';
        $css_classes[] = 'row-has-video';
	} else {
		$parallax_image_id = preg_replace( '/[^\d]/', '', $parallax_image );
		$parallax_image_src = wp_get_attachment_image_src( $parallax_image_id, 'full' );
		if ( ! empty( $parallax_image_src[0] ) ) {
			$parallax_image_src = $parallax_image_src[0];
		}
		$css_classes[] = 'parallax-window';
		// $parallax_bg .= '<div class="parallax-bg">';
		// $parallax_bg .= '<div class="parallax-bg-img" style="background-image:url(\'' . $parallax_image_src . '\')"></div>';
		// $parallax_bg .= '</div>';
	}
    // $container_classes[] = 'container-parallax-bg';
    // $parallax_bg .= '<div class="parallax-bg">';
    // $parallax_bg .= '<img src="' . $parallax_image_src . '">';
    // $parallax_bg .= '</div>';
}
if ( ! $parallax && $has_video_bg ) {
	// $wrapper_attributes[] = 'data-vc-video-bg="' . esc_attr( $video_bg_url ) . '"';
}

// Background overlay 
$overlay = '';
if (!empty($bg_overlay)) {
    $overlay = '<div class="row-bg-overlay" style="background-color:' . $bg_overlay . ';"></div>';
} 

// Scroll down
$scroll_down_div = '';
if (!empty($scroll_down)) {
	$scroll_down_position = (isset($scroll_down_position) ? $scroll_down_position : 'center');
	$icon_inline_css = 'style="';
	if (isset($scroll_down_color)) {
		$icon_inline_css .= 'color:' . $scroll_down_color;
	}
	$icon_inline_css .= '"';

    $scroll_down_div .= '<div class="container scroll-down-container">';
    $scroll_down_div .= '<div class="scroll-down has-animation visible '. $scroll_down_position .'" data-animation-loop="true" data-animation="transition.slideDownIn" data-animation-duration="1000" data-animation-delay="600">';
    $scroll_down_div .= '<i class="icon ion-ios-arrow-thin-down" ' . $icon_inline_css . '></i>';
    $scroll_down_div .= '</div>';
    $scroll_down_div .= '</div>';
} 

$css_class = preg_replace( '/\s+/', ' ', apply_filters( VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, implode( ' ', array_filter( $css_classes ) ), $this->settings['base'], $atts ) );
$wrapper_attributes[] = 'class="' . esc_attr( trim( $css_class ) ) . '"';
$wrapper_attributes[] = isset($bg_position) ? 'style="background-position:' . $bg_position . ' !important;"': '';

$output .= '<div class="outer-container">';
$output .= '<div class="' . implode( ' ', $container_classes) . '" ' . implode( ' ', $container_attributes ) . '>';
$output .= '<div ' . implode( ' ', $wrapper_attributes ) . '>';
$output .= $overlay;
$output .= $parallax_bg;
$output .= '<div class="' . implode( ' ', $inner_container_classes) . '">';
$output .= wpb_js_remove_wpautop( $content );
$output .= '</div>';
$output .= '</div>';
$output .= $scroll_down_div;
$output .= '</div>';
$output .= '</div>';
$output .= $after_output;

echo $output;
