<?php

class Luxe_Shortcodes {

  /**
   * Constructor
   * @param array
   */
  function __construct($shortcodes = array()) {
    $this->shortcodes = $shortcodes;
    $this->init();
    $this->add_shortcodes_to_widgets();
    $this->set_vc_templates_dir();
  }

  /**
   * Initialize shortcodes
   */
  function init() {
    foreach($this->shortcodes as $shortcode) {
      $file = 'shortcodes/' . $shortcode . '.php';
      if (!$filepath = locate_template($file)) {
        $filepath = LUXE_SHORTCODES . '/shortcodes/' . $shortcode . '.php';
      }
      require_once $filepath;
    }
  }

  /**
   * Enable shortcodes in widgets
   */
  function add_shortcodes_to_widgets() {
    add_filter('widget_text', 'do_shortcode');
  }

  /**
   * Set visual composer template path
   */
  function set_vc_templates_dir() {
    if (class_exists( 'Vc_Manager' )) {
      vc_set_shortcodes_templates_dir( plugin_dir_path( __FILE__ ) . 'vc_templates/' );
    }
  }

}
