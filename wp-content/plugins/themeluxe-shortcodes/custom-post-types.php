<?php

class Luxe_Custom_Post_Types {

    public function __construct( $custom_post_types = array() ) {
        foreach ($custom_post_types as $custom_post_type) {
            add_action( 'init', array( $this, $custom_post_type . '_init' ), 1 );
        }
    }

    /**
     * Portfolio
     */
    function portfolio_init() {
    	$labels = array(
    		'name'               => esc_attr__( 'Portfolio', 'luxe-text-domain' ),
    		'singular_name'      => esc_attr__( 'Portfolio Item', 'luxe-text-domain' ),
    		'menu_name'          => esc_attr__( 'Portfolio', 'luxe-text-domain' ),
    		'name_admin_bar'     => esc_attr__( 'Portfolio', 'luxe-text-domain' ),
    		'add_new'            => esc_attr__( 'New Portfolio Item', 'luxe-text-domain' ),
    		'add_new_item'       => esc_attr__( 'New Portfolio Item', 'luxe-text-domain' ),
    		'new_item'           => esc_attr__( 'New Portfolio Item', 'luxe-text-domain' ),
    		'edit_item'          => esc_attr__( 'Edit Portfolio Item', 'luxe-text-domain' ),
    		'view_item'          => esc_attr__( 'View Portfolio Item', 'luxe-text-domain' ),
    		'all_items'          => esc_attr__( 'All Portfolio Items', 'luxe-text-domain' ),
    		'search_items'       => esc_attr__( 'Search Portfolio', 'luxe-text-domain' ),
    		'parent_item_colon'  => esc_attr__( 'Parent Portfolio:', 'luxe-text-domain' ),
    		'not_found'          => esc_attr__( 'No portfolio items found.', 'luxe-text-domain' ),
    		'not_found_in_trash' => esc_attr__( 'No portfolio items found in Trash.', 'luxe-text-domain' )
    	);

    	$args = array(
    		'labels'             => $labels,
            'description'        => esc_attr__( 'Organize and show off your work publicly by adding portfolio items.', 'luxe-text-domain' ),
    		'public'             => true,
    		'publicly_queryable' => true,
    		'show_ui'            => true,
    		'show_in_menu'       => true,
    		'query_var'          => true,
    		'rewrite'            => array( 'slug' => $this->get_portfolio_slug() ),
    		'capability_type'    => 'post',
    		'has_archive'        => true,
    		'hierarchical'       => false,
    		'menu_position'      => null,
    		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'post-formats' ),
    	);
    	register_post_type( 'portfolio', $args );

        register_taxonomy( 'portfolio_category', array('portfolio'), array(
            'hierarchical' => true, 
            'label' => 'Categories', 
            'singular_label' => 'Category', 
            'rewrite' => array( 'slug' => 'category', 'with_front'=> false )
            )
        );
        register_taxonomy_for_object_type( 'portfolio_category', 'portfolio' );
    }

    /**
     * Get portfolio slug for URL
     * @return str
     */
    function get_portfolio_slug() {
        return get_theme_mod('portfolio_slug', 'portfolio');
    }

    /**
     * Testimonials
     */
    function testimonials_init() {
        $labels = array(
            'name'               => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'singular_name'      => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'menu_name'          => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'name_admin_bar'     => esc_attr__( 'Testimonial', 'luxe-text-domain' ),
            'add_new'            => esc_attr__( 'New Testimonial', 'luxe-text-domain' ),
            'add_new_item'       => esc_attr__( 'New Testimonial', 'luxe-text-domain' ),
            'new_item'           => esc_attr__( 'New Testimonial', 'luxe-text-domain' ),
            'edit_item'          => esc_attr__( 'Edit Testimonial', 'luxe-text-domain' ),
            'view_item'          => esc_attr__( 'View Testimonial', 'luxe-text-domain' ),
            'all_items'          => esc_attr__( 'All Testimonial Item', 'luxe-text-domain' ),
            'search_items'       => esc_attr__( 'Search Testimonials', 'luxe-text-domain' ),
            'parent_item_colon'  => esc_attr__( 'Parent Testimonial:', 'luxe-text-domain' ),
            'not_found'          => esc_attr__( 'No testimonial items found.', 'luxe-text-domain' ),
            'not_found_in_trash' => esc_attr__( 'No testimonial items found in Trash.', 'luxe-text-domain' )
        );

        $args = array(
            'labels'             => $labels,
            'description'        => esc_attr__( 'Organize and show off your work publicly by adding testimonial items.', 'luxe-text-domain' ),
            'public'             => false,
            'publicly_queryable' => false,
            'show_ui'            => true,
            'show_in_menu'       => true,
            'query_var'          => true,
            'capability_type'    => 'post',
            'has_archive'        => false,
            'hierarchical'       => false,
            'menu_position'      => null,
            'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt' ),
        );
        register_post_type( 'testimonial', $args );

    }

}