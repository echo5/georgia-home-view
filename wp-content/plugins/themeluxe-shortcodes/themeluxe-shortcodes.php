<?php
/**
 * Plugin Name: ThemeLuxe Shortcodes & Custom Post Types
 * Plugin URI: http://themeluxe.com
 * Description: The shortcodes and custom post types for ThemeLuxe themes
 * Version: 1.0.0
 * Author: Joshua Flowers
 * Author URI: http://themeluxe.com
 * License: GPL2
 */

/**
 * Luxe Framework Init
 */

define('LUXE_SHORTCODES', plugin_dir_path( __FILE__ ) );

require_once( LUXE_SHORTCODES . 'shortcodes.php'); // Shortcodes
require_once( LUXE_SHORTCODES . 'custom-post-types.php'); // Custom Post Types