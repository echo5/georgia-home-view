<?php

function luxe_map_vc(){
	vc_map( array(
		"name" => esc_attr__("Custom Google Maps", "luxe-text-domain"),
		"base" => "luxe_map",
		"controls" => "full",
		"show_settings_on_create" => true,
		"icon" => "vc_google_map",
		"description" => esc_attr__("Google maps with custom styling options.", "luxe-text-domain"),
		"category" => "Content",
		"params" => array(
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Width", "luxe-text-domain"),
                "description" => esc_attr__('Can be in pixels or percentage, e.g 300px or 100%)', 'luxe-text-domain'),
				"param_name" => "width",
				"value" => "100%",
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Height ", "luxe-text-domain"),
                "description" => esc_attr__('Must be in pixels, e.g 400px)', 'luxe-text-domain'),
				"param_name" => "height",
				"value" => "300px",
			),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Map Fill Container', 'luxe-text-domain' ),
                'param_name' => 'fill_container',
                'description' => esc_attr__( 'Make map the full height and width of the container it\'s in.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
            ),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_attr__("Map type", "luxe-text-domain"),
				"param_name" => "map_type",
				"admin_label" => true,
				"value" => array(esc_attr__("Roadmap", "luxe-text-domain") => "ROADMAP", esc_attr__("Satellite", "luxe-text-domain") => "SATELLITE", esc_attr__("Hybrid", "luxe-text-domain") => "HYBRID", esc_attr__("Terrain", "luxe-text-domain") => "TERRAIN"),
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Latitude", "luxe-text-domain"),
				"param_name" => "lat",
				"admin_label" => true,
				"value" => "18.591212",
				"description" => '<a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank">'.esc_attr__('Here is a tool','luxe-text-domain').'</a> '.esc_attr__('where you can find Latitude & Longitude of your location', 'luxe-text-domain'),
			),
			array(
				"type" => "textfield",
				"class" => "",
				"heading" => esc_attr__("Longitude", "luxe-text-domain"),
				"param_name" => "lng",
				"admin_label" => true,
				"value" => "73.741261",
				"description" => '<a href="http://universimmedia.pagesperso-orange.fr/geo/loc.htm" target="_blank">'.esc_attr__('Here is a tool','luxe-text-domain').'</a> '.esc_attr__('where you can find Latitude & Longitude of your location', "luxe-text-domain"),
			),
			array(
				"type" => "dropdown",
				"heading" => esc_attr__("Map Zoom", "luxe-text-domain"),
				"param_name" => "zoom",
				"value" => array(
					esc_attr__("18 - Default", "luxe-text-domain") => 12, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 13, 14, 15, 16, 17, 18, 19, 20
				),
			),
			array(
				"type" => "checkbox",
				"heading" => "",
				"param_name" => "scrollwheel",
				"value" => array(
					esc_attr__("Disable map zoom on mouse wheel scroll", "luxe-text-domain") => "disable",
				),
			),
			array(
				"type" => "dropdown",
				"class" => "",
				"heading" => esc_attr__("Marker/Point icon", "luxe-text-domain"),
				"param_name" => "marker_icon",
				"value" => array(
                    esc_attr__("Use Google Default", "luxe-text-domain") => "default",
                    esc_attr__("Use Theme Default", "luxe-text-domain") => "default_self",
                    esc_attr__("Upload Custom", "luxe-text-domain") => "custom"),
				"group" => "Marker",
			),
			array(
				"type" => "attach_image",
				"class" => "",
				"heading" => esc_attr__("Image Icon:", "luxe-text-domain"),
				"param_name" => "icon_img",
				"admin_label" => true,
				"value" => "",
				"description" => esc_attr__("Upload a custom image icon.", "luxe-text-domain"),
				"dependency" => Array("element" => "marker_icon","value" => array("custom")),
				"group" => "Marker"
			),
			array(
				"type" => "textarea_raw_html",
				"class" => "",
				"heading" => esc_attr__("Google Styled Map JSON","luxe-text-domain"),
				"param_name" => "map_style",
				"value" => "",
				"description" => "<a target='_blank' href='http://gmaps-samples-v3.googlecode.com/svn/trunk/styledmaps/wizard/index.html'>".esc_attr__("Click here","luxe-text-domain")."</a> ".esc_attr__("to get the style JSON code for styling your map.","luxe-text-domain"),
				"group" => "Styling",
			),
			array(
				"type" => "textfield",
				"heading" => esc_attr__("Extra class name", "luxe-text-domain"),
				"param_name" => "el_class",
				"description" => esc_attr__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "luxe-text-domain"),
			),
		)
	));
	
}
add_action( 'vc_before_init', 'luxe_map_vc', 100 );

function luxe_map($atts,$content = null) {
	$width = $height = $map_type = $lat = $lng = $zoom = $streetviewcontrol = $maptypecontrol = $pancontrol = $zoomcontrol = $zoomcontrolsize = $dragging = $marker_icon = $icon_img = $output = $map_style = $scrollwheel = $el_class ='';

	extract(shortcode_atts(array(
		//"id" => "map",
		"width" => "100%",
		"height" => "300px",
		"map_type" => "ROADMAP",
		"lat" => "18.591212",
		"lng" => "73.741261",
		"zoom" => "14",
		"scrollwheel" => "false",
		"marker_icon" => "default",
		"icon_img" => "",
        "map_style" => "",
		"fill_container" => "",
		"el_class" => "",
	), $atts));

	// wp_enqueue_script("googleapis","https://maps.googleapis.com/maps/api/js?v=3.21&sensor=false&callback=initialize",null,null,false);

	$border_css = '';
	$marker_lat = $lat;
	$marker_lng = $lng;
	if($marker_icon == 'default_self'){
		$icon_url = get_template_directory_uri() . '/dist/images/map-marker.png';
	} elseif($marker_icon == 'default'){
		$icon_url = '';
	} else {
		$icon_url = wp_get_attachment_url($icon_img);
	}
	$id = uniqid();
	$map_type = strtoupper($map_type);
	$width = (substr($width, -1)!="%" && substr($width, -2)!="px" ? $width . "px" : $width);
	$map_height = (substr($height, -1)!="%" && substr($height, -2)!="px" ? $height . "px" : $height);

    $inline_css = ( $width != '' ? 'width:' . $width . ';' : '');
    $inline_css .= ( $map_height != '' ? 'height:' . $map_height . ';' : '');

	if($scrollwheel == "disable"){
		$scrollwheel = 'false';
	} else {
		$scrollwheel = 'true';
	}

    $map_classes = array();
    if ($fill_container) {
        $map_classes[] = 'fill-container';
    }
    ob_start();
	?>
    <div id="map-<?php echo $id; ?>" data-fxn="mapInit<?php echo $id; ?>" class="google-map <?php echo implode(' ', $map_classes); ?>" style="<?php echo $inline_css; ?>">
    </div>
    <script type='text/javascript'>
        function mapInit<?php echo $id; ?>() {
          var customMapType = new google.maps.StyledMapType(
          <?php if ($map_style) { ?>
            <?php echo rawurldecode(base64_decode(strip_tags($map_style))); ?>, 
          <?php } ?>
          {
              name: 'Custom Style'
          });
          var customMapTypeId = 'custom_style';

          var map = new google.maps.Map(document.getElementById('map-<?php echo $id; ?>'), {
            scrollwheel: <?php echo $scrollwheel; ?>,
            disableDefaultUI: true,
            zoom: <?php echo $zoom; ?>,
            center: {lat: <?php echo $lat; ?>, lng: <?php echo $lng; ?>},
            mapTypeControlOptions: {
                mapTypeIds: [google.maps.MapTypeId.<?php echo $map_type; ?>, customMapTypeId]
            }
          });

          var marker = new google.maps.Marker({
            position: map.getCenter(),
            icon: '<?php echo $icon_url; ?>',
            map: map
          });

          map.mapTypes.set(customMapTypeId, customMapType);
          map.setMapTypeId(customMapTypeId);
        }
	</script>
    <?php
    $output .= ob_get_clean();
	return $output;
}
add_shortcode('luxe_map', 'luxe_map');
