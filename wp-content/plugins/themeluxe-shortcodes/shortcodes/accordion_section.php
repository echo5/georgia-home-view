<?php

/**
 * Luxe accordion VC map
 */

function luxe_accordion_section_vc() {
    vc_map(
    	array(
            'name' => esc_attr__( 'Accordion Section', 'luxe-text-domain' ),
            'base' => 'luxe_accordion_section',
            'icon' => 'icon-wpb-ui-tta-section',
            'allowed_container_element' => 'vc_row',
            'is_container' => true,
            'show_settings_on_create' => false,
            'as_child' => array(
                'only' => 'luxe_tabs,luxe_accordion',
            ),
            'category' => esc_attr__( 'Content', 'luxe-text-domain' ),
            'description' => esc_attr__( 'Section for Accordions.', 'luxe-text-domain' ),
            'params' => array(
                array(
                    'type' => 'textfield',
                    'param_name' => 'heading',
                    'heading' => esc_attr__( 'Heading', 'luxe-text-domain' ),
                    'description' => esc_attr__( 'Enter section heading.', 'luxe-text-domain' ),
                    "admin_label" => true,
                ),
                array(
                   'type' => 'textarea_html',
                   'holder' => 'div',
                   'class' => '',
                   'heading' => esc_attr__( 'Content', 'luxe-text-domain' ),
                   'param_name' => 'content',
                   'value' => esc_attr__( '', 'luxe-text-domain' ),
                   'description' => esc_attr__( 'Enter your content for the section.', 'luxe-text-domain' )
                ),
                array(
                    'type' => 'checkbox',
                    'heading' => esc_attr__( 'Start Section Open', 'luxe-text-domain' ),
                    'param_name' => 'open',
                    'description' => esc_attr__( 'If checked, the section will start opened/active before being toggled.', 'luxe-text-domain' ),
                    'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_attr__( 'Extra class name', 'luxe-text-domain' ),
                    'param_name' => 'el_class',
                    'description' => esc_attr__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'luxe-text-domain' ),
                ),
            ), 
            // 'js_view' => 'VcBackendTtaSectionView',
            // 'custom_markup' => '
            //     <div class="vc_tta-panel-heading">
            //         <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left"><a href="javascript:;" data-vc-target="[data-model-id=\'{{ model_id }}\']" data-vc-accordion data-vc-container=".vc_tta-container"><span class="vc_tta-title-text">{{ section_title }}</span><i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i></a></h4>
            //     </div>
            //     <div class="vc_tta-panel-body">
            //         {{ editor_controls }}
            //         <div class="{{ container-class }}">
            //         {{ content }}
            //         </div>
            //     </div>',
            'default_content' => '',
        )
    );
}
add_action( 'vc_before_init', 'luxe_accordion_section_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Luxe_Accordion_Section extends WPBakeryShortCode {
    }
}

/**
 * Luxe accordion
 */
function luxe_accordion_section($atts, $content = null)
{
    extract(shortcode_atts( array(
        'open'=>'',
        'heading'=>'',
        'el_class'=>'',
    ),$atts));           

    global $luxe_accordion_id, $luxe_accordion_gap;

    // Inline styles
    $inline_css = 'style="';
    if ($luxe_accordion_gap != '' && $luxe_accordion_gap != '2px') {
        $inline_css .= 'margin-bottom:' . $luxe_accordion_gap . ';';
    }
    $inline_css .= '"';

    // Active section
    $active = '';
    if ($open) {
        $active = 'in';
    }

    $id = uniqid();
    ob_start();
    ?>
        <div class="accordion-group panel <?php echo $el_class; ?>" <?php echo $inline_css; ?>>
            <div class="accordion-heading">
                <div class="accordion-toggle" data-toggle="collapse" data-parent="#accordion-<?php echo $luxe_accordion_id; ?>" data-target="#accordion-body-<?php echo $id; ?>">
                    <h4><?php echo $heading; ?></h4>
                </div>
            </div>
            <div id="accordion-body-<?php echo $id; ?>" class="accordion-body collapse <?php echo $active; ?>"> 
                <div class="accordion-inner">
                    <?php echo do_shortcode($content); ?>
                </div>
            </div>
        </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_accordion_section', 'luxe_accordion_section' );
