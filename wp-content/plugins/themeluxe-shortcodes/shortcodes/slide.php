<?php

function luxe_slide_vc() {

    vc_map( array(
        'name' => esc_attr__( 'Luxe Slide', 'luxe-text-domain' ),
        'base' => 'luxe_slide',
        'icon' => 'icon-wpb-images-carousel',
        'category' => esc_attr__( 'Content', 'luxe-text-domain' ),
        'description' => esc_attr__( 'A single slide used in the luxe slider.', 'luxe-text-domain' ),
        'as_child' => array('only' => 'luxe_slider'),
        'params' => array(
            array(
                'type' => 'attach_image',
                'heading' => esc_attr__( 'Background Image', 'luxe-text-domain' ),
                'param_name' => 'bg',
                'description' => esc_attr__( 'Select an image for the background of your slide.', 'luxe-text-domain' ),
            ),
            array(
               'type' => 'textarea_html',
               'holder' => 'div',
               'class' => '',
               'heading' => esc_attr__( 'Content', 'luxe-text-domain' ),
               'param_name' => 'content',
               'value' => esc_attr__( '<h2>Slide header</h2><p>Slide text</p>', 'luxe-text-domain' ),
               'description' => esc_attr__( 'Enter your content.', 'luxe-text-domain' )
            )
        ),
    ));
}
add_action( 'vc_before_init', 'luxe_slide_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Luxe_Slide extends WPBakeryShortCode {
    }
}

function luxe_slide( $atts, $content = null ) {

    extract( shortcode_atts( array(
        'bg' => '',
    ), $atts ) );
    $inline_css = 'style="';
    if (!empty($bg)) {
        if (is_numeric($bg)) {
            $image = wp_get_attachment_image_src($bg, 'full');
            $image_url = $image[0];
        }
        else {
            $image_url = $bg;
        }
        $inline_css .= 'background-image:url('. $image_url . ');';
    }
    $inline_css .= '"';

    $div_id = uniqid();
    ob_start();
    ?>
            <div class="inner-slide" <?php echo $inline_css; ?>>
                <div class="slide-content">
            		<?php echo do_shortcode( $content ); ?>
                </div>
            </div>
    <?php
    $output = ob_get_clean(); 
    return $output;

}
add_shortcode( 'luxe_slide', 'luxe_slide' );