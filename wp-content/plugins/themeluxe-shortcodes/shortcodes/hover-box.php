<?php

use Luxe\Helper;

function luxe_hover_box_vc() {

	vc_map( 
		array(
			"name" => esc_attr__("Hover Box", "luxe-text-domain"),
			"base" => "luxe_hover_box",
			"icon" => "luxe_hover_box",
			"class" => "luxe_hover_box",
			"as_parent" => array('except' => 'luxe_hover_box'),
			"content_element" => true,
			"controls" => "full",
			"show_settings_on_create" => true,
			"category" => "Content",
			"description" => esc_attr__("Add a content box with a hover effect.",'luxe-text-domain'),
			"is_container"    => false,
			"params" => array(
				// add params same as with any other content element
				// array(
				// 	"type" => "dropdown",
				// 	"class" => "",
				// 	"heading" => esc_attr__("Effect","luxe-text-domain"),
				// 	"param_name" => "effect",
				// 	"value" => array(
				// 		array('zoom', 'Zoom'),
				// 	),
			 //  	),
			  	array(
			  	    'type' => 'attach_image',
			  	    'heading' => esc_attr__( 'Image', 'luxe-text-domain' ),
			  	    'param_name' => 'image',
			  	    'description' => esc_attr__( 'Select an image for the background of your hover box.', 'luxe-text-domain' ),
			  	),
				array(
					"type" => "textfield",
					"heading" => esc_attr__("Height", "luxe-text-domain"),
					"param_name" => "height",
					"description" => esc_attr__("Set the height of your hover box (e.g., 250px will set the height 250 pixels tall).  Leave blank to respond dynamically to inside content.", "luxe-text-domain")
				),
				array(
				    "type"        => "vc_link",
				    "class"       => "",
				    "heading"     => esc_attr__("Link", "luxe-text-domain"),
				    "param_name"  => "link",
				    "description" => esc_attr__("Link the entire box to a location.", "luxe-text-domain"),
				),
				array(
					"type" => "textfield",
					"heading" => esc_attr__("Extra class name", "luxe-text-domain"),
					"param_name" => "el_class",
					"description" => esc_attr__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "luxe-text-domain")
				),
			),
			"js_view" => 'VcColumnView'
		)
	);
}
add_action( 'vc_before_init', 'luxe_hover_box_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Hover_Box extends WPBakeryShortCodesContainer {
    }
}

/**
 * Luxe animation
 */
function luxe_hover_box($atts, $content = null)
{
    extract(shortcode_atts( array(
        'effect' => 'zoom',
        'image' => '',
        'height'=>'',
        'link'=>'',
        'el_class'=>'',
    ),$atts));           

    $classes = array();
    $classes[] = $el_class;
    $classes[] = 'hover-effect-' . $effect;

    $image = wp_get_attachment_image_src($image, 'full');
    $bg = $image[0];

    // Inline CSS
    $inline_css = 'style="';
    if ($height) {
    	$inline_css .= 'height:' . $height;
    }
    $inline_css .= '"';

    $link = Helper\build_link($link);

    ob_start();
    ?>
    <div class="hover-box <?php echo implode(' ', $classes); ?>" <?php echo $inline_css; ?>>
    	<div style="background-image:url('<?php echo $bg; ?>');" class="hover-box-bg"></div>
    	<?php if ($link) { ?>
	        <a class="overlay-link" href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" target="<?php echo $link['target']; ?>"></a>
    	<?php } ?>
    	<div class="hover-box-content">
	        <?php echo do_shortcode( $content ); ?>
        </div>	
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_hover_box', 'luxe_hover_box' );