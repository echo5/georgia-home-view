<?php

use Luxe\Helper;

/**
 * Luxe accordion VC map
 */

function luxe_accordion_vc() {
    vc_map(
    	array(
            'name' => esc_attr__( 'Accordion', 'luxe-text-domain' ),
            'base' => 'luxe_accordion',
            'icon' => 'icon-wpb-ui-accordion',
            'is_container' => true,
            'show_settings_on_create' => false,
            'as_parent' => array(
                'only' => 'luxe_accordion_section',
            ),
            'category' => esc_attr__( 'Content', 'luxe-text-domain' ),
            'description' => esc_attr__( 'Collapsible content panels', 'luxe-text-domain' ),
            'params' => array(
                array(
                    'type' => 'dropdown',
                    'param_name' => 'gap',
                    'value' => array(
                        esc_attr__( 'None', 'luxe-text-domain' ) => '',
                        '1px' => '1px',
                        '2px' => '2px',
                        '3px' => '3px',
                        '4px' => '4px',
                        '5px' => '5px',
                        '10px' => '10px',
                        '15px' => '15px',
                        '20px' => '20px',
                        '25px' => '25px',
                        '30px' => '30px',
                        '35px' => '35px',
                    ),
                    'std' => '2px',
                    'heading' => esc_attr__( 'Gap', 'luxe-text-domain' ),
                    'description' => esc_attr__( 'Select accordion gap.', 'luxe-text-domain' ),
                ),
                array(
                    'type' => 'textfield',
                    'heading' => esc_attr__( 'Extra class name', 'luxe-text-domain' ),
                    'param_name' => 'el_class',
                    'description' => esc_attr__( 'If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.', 'luxe-text-domain' ),
                ),
                array(
                    'type' => 'el_id',
                    'param_name' => 'accordion_id',
                    'settings' => array(
                        'auto_generate' => true,
                    ),
                    'heading' => esc_attr__( 'Accordion ID', 'luxe-text-domain' ),
                    'description' => esc_attr__( 'Enter section ID (Note: make sure it is unique and valid according to <a href="%s" target="_blank">w3c specification</a>).', 'luxe-text-domain' ),
                ),
                array(
                    'type' => 'css_editor',
                    'heading' => esc_attr__( 'CSS box', 'luxe-text-domain' ),
                    'param_name' => 'css',
                    'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
                ),
            ),
            "js_view" => 'VcColumnView'
            // 'js_view' => 'VcBackendTtaAccordionView',
            // 'custom_markup' => '
            //     <div class="vc_tta-container" data-vc-action="collapseAll">
            //         <div class="vc_general vc_tta vc_tta-accordion vc_tta-color-backend-accordion-white vc_tta-style-flat vc_tta-shape-rounded vc_tta-o-shape-group vc_tta-controls-align-left vc_tta-gap-2">
            //            <div class="vc_tta-panels vc_clearfix {{container-class}}">
            //               {{ content }}
            //               <div class="vc_tta-panel vc_tta-section-append">
            //                  <div class="vc_tta-panel-heading">
            //                     <h4 class="vc_tta-panel-title vc_tta-controls-icon-position-left">
            //                        <a href="javascript:;" aria-expanded="false" class="luxe_section-backend-add-control">
            //                            <span class="vc_tta-title-text">' . esc_attr__( 'Add Section', 'luxe-text-domain' ) . '</span>
            //                             <i class="vc_tta-controls-icon vc_tta-controls-icon-plus"></i>
            //                         </a>
            //                     </h4>
            //                  </div>
            //               </div>
            //            </div>
            //         </div>
            //     </div>',
            // 'default_content' => '[luxe_section title="' . sprintf( '%s %d', esc_attr__( 'Section', 'luxe-text-domain' ), 1 ) . '"][/luxe_section][luxe_section title="' . sprintf( '%s %d', esc_attr__( 'Section', 'luxe-text-domain' ), 2 ) . '"][/luxe_section]',
        )
    );
}
add_action( 'vc_before_init', 'luxe_accordion_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Accordion extends WPBakeryShortCodesContainer {
    }
}

/**
 * Luxe accordion
 */
function luxe_accordion($atts, $content = null)
{
    extract(shortcode_atts( array(
        'accordion_id' => '',
        'gap' => '',
        'el_class' => '',
        'css' => '',
    ),$atts));           

    global $luxe_accordion_id, $luxe_accordion_gap;
    $luxe_accordion_id = $accordion_id;
    $luxe_accordion_gap = $gap;
    
    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    ob_start();
    ?>
        <div class="accordion wpb_content_element <?php echo $el_class; ?> <?php echo $css_class; ?>" id="accordion-<?php echo $accordion_id; ?>">
            <?php echo do_shortcode($content); ?>
        </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_accordion', 'luxe_accordion' );
