<?php

function luxe_typewriter_vc() {
	vc_map( 
		array(
			"name" => esc_attr__("Typewriter", "luxe-text-domain"),
			"base" => "luxe_typewriter",
			"icon" => "luxe_typewriter",
			"class" => "luxe_typewriter",
			"category" => "Content",
			"description" => esc_attr__("Create a typewriter effect with any text.",'luxe-text-domain'),
			"params" => array(
				array(
				 "type" => "textfield",
				 "class" => "",
				 "heading" => esc_attr__("Text", "luxe-text-domain"),
				 "param_name" => "text",
				 "value" => "Line one | Line two | Line three",
				 "description" => esc_attr__("Enter the text for your typewriter separated by a | for each new string of text. (Copy and paste this character if you have trouble finding it on your keyboard).", "luxe-text-domain")
				),
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_attr__("Before Text","luxe-text-domain"),
					"param_name" => "before_text",
					"value" => '',
					"description" => esc_attr__("Static text displayed in front of your typewriter text.","luxe-text-domain"),
			  	),
		  		array(
		  			"type" => "textfield",
		  			"class" => "",
		  			"heading" => esc_attr__("After Text","luxe-text-domain"),
		  			"param_name" => "after_text",
		  			"value" => '',
		  			"description" => esc_attr__("Static text displayed after your typewriter text.","luxe-text-domain"),
		  	  	),
		  	  	array(
		  	  		"type" => "colorpicker",
		  	  		"class" => "",
		  	  		"heading" => esc_attr__("Text Color", "luxe-text-domain"),
		  	  		"param_name" => "text_color",
		  	  		"value" => "",
		  	  	),
		  	  	array(
		  	  		"type" => "colorpicker",
		  	  		"class" => "",
		  	  		"heading" => esc_attr__("Typewriter Text Color", "luxe-text-domain"),
		  	  		"param_name" => "typewriter_text_color",
		  	  		"value" => "",
		  	  	),
  	  			array(
  	  	            "type" => "dropdown",
  	  	            "class" => "",
  	  	            "heading" => esc_attr__("Font Style", "luxe-text-domain"),
  	  	            "param_name" => "font_style",
  	  	            "value" => array(
  	  	                esc_attr__('Default','luxe-text-domain') => '',
  	  	                esc_attr__('H1','luxe-text-domain') => 'h1',
  	  	                esc_attr__('H2','luxe-text-domain') => 'h2',
  	  	                esc_attr__('H3','luxe-text-domain') => 'h3',
  	  	                esc_attr__('H4','luxe-text-domain') => 'h4',
  	  	                esc_attr__('H5','luxe-text-domain') => 'h5',
  	  	            ),							
  	  	            "description" => esc_attr__("Size of modal box.", "luxe-text-domain")
  	  			),
		  	  	array(
		  	  		 "type" => "dropdown",
		  	  		 "class" => "",
		  	  		 "heading" => esc_attr__("Alignment", "luxe-text-domain"),
		  	  		 "param_name" => "alignment",
		  	  		 "value" => array(
		  	  		 		esc_attr__('Left','luxe-text-domain') => 'text-left',
		  	  		 		esc_attr__('Center','luxe-text-domain') => 'text-center',
		  	  				esc_attr__('Right','luxe-text-domain') => 'text-right',
		  	  		 		),							
		  	  		 "description" => esc_attr__("Enter Position of Icon", "luxe-text-domain")
		  	  	),
		  		array(
		  			"type" => "textfield",
		  			"class" => "",
		  			"heading" => esc_attr__("Type Speed","luxe-text-domain"),
		  			"param_name" => "speed",
		  			"value" => 100,
		  			"min" => 100,
		  			"max" => 10000,
		  			"description" => esc_attr__("Speed each character is typed in milliseconds (e.g., 100 would be 0.1 seconds).","luxe-text-domain"),
		  	  	),
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_attr__("Animation Loop","luxe-text-domain"),
					"param_name" => "loop_count",
					"value" => 1,
					"min" => 0,
					"max" => 100,
					"suffix" => "",
					"description" => esc_attr__("The number of times you'd like the animation to play.  Enter <strong>false</strong> to repeat indefinitely.","luxe-text-domain"),
			  	),
				array(
					'type' => 'checkbox',
					'heading' => esc_attr__( 'Cursor', 'luxe-text-domain' ),
					'param_name' => 'cursor',
					'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
					'description' => esc_attr__( 'Add a blinking cursor at the end of your text.', 'luxe-text-domain' ),
					'std' => 'true'
				),
				array(
					"type" => "textfield",
					"heading" => esc_attr__("Extra class name", "luxe-text-domain"),
					"param_name" => "el_class",
					"description" => esc_attr__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "luxe-text-domain")
				),
			),
		)
	);
}
add_action( 'vc_before_init', 'luxe_typewriter_vc', 100 );

/**
 * Luxe animation
 */
function luxe_typewriter($atts, $content = null)
{
    extract(shortcode_atts( array(
        'alignment' => '',
        'text' => 'Line one|Line two|Line three',
        'before_text' => '',
        'after_text' => '',
        'speed' => '100',
        'delay' => '0',
        'loop_count' => 'false',
        'cursor' => 'true',
        'text_color' => '',
        'typewriter_text_color' => '',
        'font_style' => '',
        'el_class'=>'',
    ),$atts));           

    $id = uniqid();

    $classes = array();
    $classes[] = $el_class;
    $classes[] = $alignment;
    $classes[] = $font_style;

    $inline_css = 'style="';
    if (!empty($text_color)) {
        $inline_css .= 'color:' . $text_color . ';';
    }
    $inline_css .= '"';

    $typewriter_inline_css = 'style="';
    if (!empty($typewriter_text_color)) {
        $typewriter_inline_css .= 'color:' . $typewriter_text_color . ';';
    }
	$typewriter_inline_css .= '"';

    $strings = explode('|', $text);
    $string_output = implode('", "', $strings);

    if(empty($cursor)) {
    	$cursor = 'false';
    } else {
    	$cursor = 'true';
    }

    ob_start();
    ?>
    <div class="typewriter-wrap wpb_content_element <?php echo implode(' ', $classes); ?>" <?php echo $inline_css; ?>>
    	<div class="typewriter-before"><?php echo $before_text; ?></div>
    	<div class="typewriter" id="typewriter-<?php echo $id; ?>" <?php echo $typewriter_inline_css; ?>></div>     
    	<div class="typewriter-after"><?php echo $after_text; ?></div>
	</div>      
    <script>
    jQuery( document ).ready(function($) {
        $("#typewriter-<?php echo $id; ?>").typed({
          strings: ["<?php echo $string_output; ?>"],
          typeSpeed: <?php echo $speed; ?>,
          showCursor: <?php echo $cursor; ?>,
          loop: true,
          loopCount: <?php echo $loop_count; ?>
        });
    });
    </script>
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_typewriter', 'luxe_typewriter' );