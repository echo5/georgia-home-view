<?php

/**
 * Luxe counter VC map
 */

function luxe_modal_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Modal","luxe-text-domain"),
    	   "base" => "luxe_modal",
    	   "class" => "luxe_modal",
    	   "icon" => "luxe_modal",
    	   // "as_parent" => array('only' => 'single_img'),
    	   "content_element" => true,
    	   "show_settings_on_create" => true,
    	   "is_container" => true,
    	   "category" => "Content",
    	   "description" => esc_attr__("Modal window (pop-up box) in which you can add any content.","luxe-text-domain"),
    	   "params" => array(
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => esc_attr__("Modal ID",  "luxe-text-domain"),
                    "param_name" => "id",
                    "value" => "",
                    "description" => esc_attr__("This field is required to access the modal.",  "luxe-text-domain"),
                ),
        		array(
    				"type" => "colorpicker",
    				"class" => "",
    				"heading" => esc_attr__("Background Color", "luxe-text-domain"),
    				"param_name" => "bg_color",
    				"value" => "",
        		),
        		array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => esc_attr__("Modal Size", "luxe-text-domain"),
                    "param_name" => "size",
                    "value" => array(
                        esc_attr__('Small','luxe-text-domain') => 'modal-sm',
                        esc_attr__('Medium','luxe-text-domain') => 'modal-m',
                    	esc_attr__('Large','luxe-text-domain') => 'modal-lg',
                    ),							
                    "description" => esc_attr__("Size of modal box.", "luxe-text-domain")
        		),
    			array(
    				"type" => "textfield",
    				"class" => "",
    				"heading" => esc_attr__("Extra Class",  "luxe-text-domain"),
    				"param_name" => "el_class",
    				"value" => "",
    				"description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.",  "luxe-text-domain"),
    			),
    		),
    	)
    );
}
add_action( 'vc_before_init', 'luxe_modal_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Modal extends WPBakeryShortCodesContainer {
    }
}

/**
 * Luxe animated digits counter
 */

function luxe_modal($atts, $content = null)
{
    extract(shortcode_atts( array(
        'id'=>'',
        'size'=>'modal-m',
        'bg_color'=>'',
        'el_class'=>'',
    ),$atts));           

    $modal_classes = array();
    $modal_classes[] = $el_class;

    $style = 'style="';
    if ($bg_color) {
        $style .= 'background-color:' . $bg_color . ';';
    }
    $style .= '"';

    ob_start();
    ?>
    <div class="modal fade <?php echo implode(' ', $modal_classes); ?>" id="<?php echo $id; ?>">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center <?php echo $size; ?>" role="document">
                <div class="modal-content" <?php echo $style; ?>>
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true"><i class="ion-android-close"></i></span>
                            <span class="sr-only">Close</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <?php echo do_shortcode( $content ); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_modal', 'luxe_modal' );
