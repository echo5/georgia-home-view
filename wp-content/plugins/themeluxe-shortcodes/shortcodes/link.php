<?php

use Luxe\Helper;

/**
 * Luxe link VC map
 */

function luxe_link_vc()
{
    vc_map(
        array(
            "name"        => esc_attr__("Link", "luxe-text-domain"),
            "base"        => "luxe_link",
            "class"       => "luxe_link",
            "icon"        => "link",
            "category"    => "Content",
            "description" => esc_attr__("A stylized link.", "luxe-text-domain"),
            "params"      => array(
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Link Text", "luxe-text-domain"),
                    "param_name"  => "link_text",
                    "admin_label" => true,
                    "description" => esc_attr__("Enter the content for the link.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Link Font Size", "luxe-text-domain"),
                    "param_name"  => "font_size",
                    "suffix"      => "px",
                    "description" => esc_attr__("Enter value in pixels or leave blank for default.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Link Color", "luxe-text-domain"),
                    "param_name" => "link_color",
                    "description" => esc_attr__("Leave blank for default.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "vc_link",
                    "class"       => "",
                    "heading"     => esc_attr__("Link", "luxe-text-domain"),
                    "param_name"  => "link",
                    "description" => esc_attr__("Link location.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Link Style", "luxe-text-domain"),
                    "param_name"  => "style",
                    "value"       => array(
                        esc_attr__("None", "luxe-text-domain")   => "none",
                        esc_attr__("Underline", "luxe-text-domain") => "underline",
                    ),
                    "description" => esc_attr__("Style for the link.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Underline Color", "luxe-text-domain"),
                    "param_name" => "underline_color",
                    "value"      => "",
                    "dependency" => array("element" => "style", "value" => array("underline")),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Icon to display:", "luxe-text-domain"),
                    "param_name"  => "icon_type",
                    "value"       => array(
                        esc_attr__("No Icon", "luxe-text-domain")   => "none",
                        esc_attr__("Font Icon", "luxe-text-domain") => "selector",
                    ),
                    "description" => esc_attr__("Add an icon to your link.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "iconpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Select Icon ", "luxe-text-domain"),
                    'settings'   => array(
                        'emptyIcon'    => false,
                        'type'         => 'ionicons',
                        'iconsPerPage' => 200,
                    ),
                    "param_name" => "icon",
                    "value"      => "",
                    "dependency" => array("element" => "icon_type", "value" => array("selector")),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Size of Icon", "luxe-text-domain"),
                    "param_name"  => "icon_size",
                    "min"         => 12,
                    "max"         => 72,
                    "suffix"      => "px",
                    "description" => esc_attr__("Enter value in pixels.", "luxe-text-domain"),
                    "dependency"  => array("element" => "icon_type", "value" => array("selector")),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Icon Color", "luxe-text-domain"),
                    "param_name" => "icon_color",
                    "value"      => "",
                    "dependency" => array("element" => "icon_type", "value" => array("selector")),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Icon Position", "luxe-text-domain"),
                    "param_name"  => "icon_position",
                    "value"       => array(
                        esc_attr__('Right', 'luxe-text-domain') => 'right',
                        esc_attr__('Left', 'luxe-text-domain')  => 'left',
                    ),
                    "description" => esc_attr__("Select position of icon", "luxe-text-domain"),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Extra Class", "luxe-text-domain"),
                    "param_name"  => "el_class",
                    "value"       => "",
                    "description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.", "luxe-text-domain"),
                ),
            ),
        )
    );
}
add_action('vc_before_init', 'luxe_link_vc', 100);

/**
 * Luxe link
 */
function luxe_link($atts, $content = null)
{
    extract(shortcode_atts(array(
        'icon_type'     => '',
        'icon'          => '',
        'icon_size'     => '',
        'icon_color'    => '',
        'icon_position' => 'right',
        'font_size'     => '',
        'link_text'    => '',
        'link_color'    => '',
        'link'          => '',
        'style'          => '',
        'underline_color'          => '#3d3d3d',
        'el_class'      => '',
    ), $atts));

    // Icon inline CSS
    $icon_inline_css = 'style="';
    if (!empty($icon_size)) {
        $icon_inline_css .= 'font-size:' . $icon_size . 'px;';
    }
    if (!empty($icon_color)) {
        $icon_inline_css .= 'color:' . $icon_color . ';';
    }
    $icon_inline_css .= '"';
    // Link inline CSS
    $link_inline_css = 'style="';
    if (!empty($font_size)) {
        $link_inline_css .= 'font-size:' . $font_size . 'px;';
    }
    if (!empty($link_color)) {
        $link_inline_css .= 'color:' . $link_color . ';';
    }
    $link_inline_css .= '"';    
    // Icon class
    $icon_class = '';
    if ((!empty($icon) && $icon_type != 'none')) {
        $icon_class = 'icon-position-' . $icon_position;
    }
    $link = Helper\build_link($link);
    $target = ($link['target'] ? 'target="' . $link['target'] . '"' : '');

    ob_start();
    ?>
        <a class="luxe-link <?php echo $icon_class; ?> <?php echo $style; ?>" href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" <?php echo $target; ?>>
            <?php if ((!empty($icon) && $icon_type != 'none')) {?>
                <i class="icon <?php echo $icon; ?>" <?php echo $icon_inline_css; ?>></i>
            <?php } ?>
            <span class="link-text" <?php echo $link_inline_css; ?>>
                <?php echo $link_text; ?>
            </span>
            <?php if ($style == 'underline') { ?>
                <span class="link-underline" style="background-color:<?php echo $underline_color; ?>;"></span>
            <?php } ?>
        </a>
    <?php
$output = ob_get_clean();
    return $output;
}
add_shortcode('luxe_link', 'luxe_link');
