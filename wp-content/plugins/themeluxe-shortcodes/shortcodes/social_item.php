<?php

/**
 * Luxe social item VC map
 */

function luxe_social_item_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Social Item","luxe-text-domain"),
    	   "base" => "luxe_social_item",
    	   "class" => "luxe_social_item",
    	   "icon" => "luxe_social_item",
    	   "category" => "Content",
           "as_child" => array('only' => 'luxe_social'),
    	   "description" => esc_attr__("Display your social media links.","luxe-text-domain"),
    	   "params" => array(
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Style", "luxe-text-domain"),
                    "param_name"  => "style",
                    "value"       => array(
                        esc_attr__("Icon", "luxe-text-domain")   => "icon",
                        esc_attr__("Text", "luxe-text-domain") => "text",
                    ),
                    "description" => esc_attr__("Use text or an icon for your social link.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "iconpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Select Icon ", "luxe-text-domain"),
                    'settings'   => array(
                        'emptyIcon'    => false,
                        'type'         => 'ionicons',
                        'iconsPerPage' => 200,
                    ),
                    "param_name" => "icon",
                    "value"      => "",
                    "dependency" => array("element" => "style", "value" => array("icon")),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Text", "luxe-text-domain"),
                    "param_name"  => "text",
                    "description" => esc_attr__("Enter link text.", "luxe-text-domain"),
                    "dependency"  => array("element" => "style", "value" => array("text")),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("URL", "luxe-text-domain"),
                    "param_name"  => "url",
                    "description" => esc_attr__("Enter URL for link.", "luxe-text-domain"),
                ),
    		),
    	)
    );
}
add_action( 'vc_before_init', 'luxe_social_item_vc', 100 );

if ( class_exists( 'WPBakeryShortCode' ) ) {
    class WPBakeryShortCode_Luxe_Social_Item extends WPBakeryShortCode {
    }
}

/**
 * Luxe social item
 */

function luxe_social_item($atts, $content = null)
{
    extract(shortcode_atts( array(
        'style'=>'',
        'icon'=>'',
        'text'=>'',
        'url'=>'',
    ),$atts));           

    ob_start();
    ?>
    <a href="<?php echo $url; ?>" class="social-item" target="_blank">
        <?php if ($style == 'text') { ?>
            <?php echo $text; ?>
        <?php } else { ?>
            <i class="icon <?php echo $icon; ?>"></i>
        <?php } ?>
    </a>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_social_item', 'luxe_social_item' );
