<?php

use Luxe\Helper;

/**
 * Luxe button VC map
 */

function luxe_button_vc()
{
    vc_map(
        array(
            "name"        => esc_attr__("Button", "luxe-text-domain"),
            "base"        => "luxe_button",
            "class"       => "luxe_button",
            "icon"        => "link",
            "category"    => "Content",
            "description" => esc_attr__("", "luxe-text-domain"),
            "params"      => array(
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Style", "luxe-text-domain"),
                    "param_name"  => "style",
                    "value"       => array(
                        esc_attr__("Default", "luxe-text-domain")   => "default",
                        esc_attr__("Primary", "luxe-text-domain") => "primary",
                    ),
                    "description" => esc_attr__("Choose whether the button will use your default or primary styles chosen in the customizer.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Link Text", "luxe-text-domain"),
                    "param_name"  => "link_text",
                    "admin_label" => true,
                    "description" => esc_attr__("Enter the content for the link.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "vc_link",
                    "class"       => "",
                    "heading"     => esc_attr__("Link", "luxe-text-domain"),
                    "param_name"  => "link",
                    "description" => esc_attr__("Link location.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Alignment", "luxe-text-domain"),
                    "param_name"  => "alignment",
                    "value"       => array(
                        esc_attr__('Left', 'luxe-text-domain')  => 'left',
                        esc_attr__('Center', 'luxe-text-domain')  => 'center',
                        esc_attr__('Right', 'luxe-text-domain') => 'right',
                    ),
                    "description" => esc_attr__("Select position of button", "luxe-text-domain"),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Icon to display:", "luxe-text-domain"),
                    "param_name"  => "icon_type",
                    "value"       => array(
                        esc_attr__("No Icon", "luxe-text-domain")   => "none",
                        esc_attr__("Font Icon", "luxe-text-domain") => "selector",
                    ),
                    "description" => esc_attr__("Add an icon to your link.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "iconpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Select Icon ", "luxe-text-domain"),
                    'settings'   => array(
                        'emptyIcon'    => false,
                        'type'         => 'ionicons',
                        'iconsPerPage' => 200,
                    ),
                    "param_name" => "icon",
                    "value"      => "",
                    "dependency" => array("element" => "icon_type", "value" => array("selector")),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Icon Position", "luxe-text-domain"),
                    "param_name"  => "icon_position",
                    "value"       => array(
                        esc_attr__('Right', 'luxe-text-domain') => 'right',
                        esc_attr__('Left', 'luxe-text-domain')  => 'left',
                    ),
                    "description" => esc_attr__("Select position of icon", "luxe-text-domain"),
                ),
                array(
                    'type' => 'checkbox',
                    'heading' => esc_attr__( 'Scroll To', 'luxe-text-domain' ),
                    'param_name' => 'scroll_to',
                    'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'yes' ),
                    'description' => esc_attr__( 'If checked, the page will attempt to scroll to the link if on the same page.', 'luxe-text-domain' ),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Extra Class", "luxe-text-domain"),
                    "param_name"  => "el_class",
                    "value"       => "",
                    "description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.", "luxe-text-domain"),
                ),
                array(
                    'type' => 'css_editor',
                    'heading' => esc_attr__( 'CSS box', 'luxe-text-domain' ),
                    'param_name' => 'css',
                    'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
                ),
            ),
        )
    );
}
add_action('vc_before_init', 'luxe_button_vc', 100);

/**
 * Luxe button
 */
function luxe_button($atts, $content = null)
{
    extract(shortcode_atts(array(
        'alignment'     => 'left',
        'icon_type'     => '',
        'icon'          => '',
        'icon_position' => 'right',
        'link_text'    => '',
        'link'          => '',
        'style'          => 'default',
        'scroll_to'      => '',
        'el_class'      => '',
        'css'      => '',
    ), $atts));

    // Icon class
    $icon_class = '';
    if ((!empty($icon) && $icon_type != 'none')) {
        $icon_class = 'icon-position-' . $icon_position;
    }
    $link = Helper\build_link($link);
    $target = ($link['target'] ? 'target="' . $link['target'] . '"' : '');

    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    // Setup link classes
    $link_classes = array();
    if ($scroll_to) {
        $link_classes[] = 'scroll-to';
    }

    ob_start();
    ?>
    <div class="luxe-btn-container wpb_content_element text-align-<?php echo $alignment; ?> <?php echo $css_class; ?>">
        <a class="luxe-btn btn <?php echo $icon_class; ?> btn-<?php echo $style; ?> <?php echo implode(' ', $link_classes); ?>" href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" <?php echo $target; ?>>
            <?php if ((!empty($icon) && $icon_type != 'none')) {?>
                <i class="icon <?php echo $icon; ?>"></i>
            <?php } ?>
            <span class="btn-text"><?php echo $link_text; ?></span>
        </a>
    </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('luxe_button', 'luxe_button');
