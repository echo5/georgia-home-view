<?php

/**
 * Luxe counter VC map
 */

function luxe_counter_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Counter","luxe-text-domain"),
    	   "base" => "luxe_counter",
    	   "class" => "luxe_counter",
    	   "icon" => "counter",
    	   "category" => "Content",
    	   "description" => esc_attr__("An animated rolling counter","luxe-text-domain"),
    	   "params" => array(
    			array(
    				"type" => "dropdown",
    				"class" => "",
    				"heading" => esc_attr__("Icon to display:", "luxe-text-domain"),
    				"param_name" => "icon_type",
    				"value" => array(
                        esc_attr__("Font Icon","luxe-text-domain") => "selector",
                        esc_attr__("Custom Image Icon","luxe-text-domain") => "custom",
    					esc_attr__("No Icon","luxe-text-domain") => "none",
    				),
    				"description" => esc_attr__("Use an existing font icon or upload a custom image.  Leave blank for none.", "luxe-text-domain")
    			),
    			array(
    				"type" => "iconpicker",
    				"class" => "",
    				"heading" => esc_attr__("Select Icon ","luxe-text-domain"),
                    'settings'    => array(
                        'emptyIcon'    => false,
                        'type'         => 'ionicons',
                        'iconsPerPage' => 200,
                    ),
    				"param_name" => "icon",
    				"value" => "",
    				"description" => esc_attr__("Click and select icon of your choice. If you can't find the one that suits for your purpose","luxe-text-domain").", ".esc_attr__("you can","luxe-text-domain")." <a href='admin.php?page=font-icon-Manager' target='_blank'>".esc_attr__("add new here","luxe-text-domain")."</a>.",
    				"dependency" => Array("element" => "icon_type","value" => array("selector")),
    			),
    			array(
    				"type" => "attach_image",
    				"class" => "",
    				"heading" => esc_attr__("Upload Image", "luxe-text-domain"),
    				"param_name" => "icon_img",
    				"value" => "",
    				"description" => esc_attr__("Upload the custom image icon.", "luxe-text-domain"),
    				"dependency" => Array("element" => "icon_type","value" => array("custom")),
    			),
    			array(
    				"type" => "textfield",
    				"class" => "",
    				"heading" => esc_attr__("Size of Icon", "luxe-text-domain"),
    				"param_name" => "icon_size",
    				"suffix" => "px",
    				"description" => esc_attr__("Enter value in pixels.", "luxe-text-domain"),
    				"dependency" => Array("element" => "icon_type","value" => array("selector")),
    			),
    			array(
    				"type" => "colorpicker",
    				"class" => "",
    				"heading" => esc_attr__("Icon Color", "luxe-text-domain"),
    				"param_name" => "icon_color",
    				"value" => "",
    				"dependency" => Array("element" => "icon_type","value" => array("selector")),
    			),
    		  array(
    			 "type" => "dropdown",
    			 "class" => "",
    			 "heading" => esc_attr__("Icon Position", "luxe-text-domain"),
    			 "param_name" => "icon_position",
    			 "value" => array(
                        esc_attr__('Top','luxe-text-domain') => 'top',
    			 		esc_attr__('Top Center','luxe-text-domain') => 'top-center',
    					esc_attr__('Right','luxe-text-domain') => 'right',
    					esc_attr__('Left','luxe-text-domain') => 'left',	
    			 		),							
    			 "description" => esc_attr__("Enter Position of Icon", "luxe-text-domain")
    			 ),
              array(
                 "type" => "textfield",
                 "class" => "",
                 "heading" => esc_attr__("Counter Text ", "luxe-text-domain"),
                 "param_name" => "counter_text",
                 "admin_label" => true,
                 "value" => "",
                 "description" => esc_attr__("Enter the text for your counter.", "luxe-text-domain")
              ),
              array(
                "type" => "colorpicker",
                "class" => "",
                "heading" => esc_attr__("Text Color", "luxe-text-domain"),
                "param_name" => "text_color",
                "value" => "",
              ),
                array(
                 "type" => "textfield",
                 "class" => "",
                 "heading" => esc_attr__("Counter Subtext ", "luxe-text-domain"),
                 "param_name" => "counter_subtext",
                 "value" => "",
                ),
                array(
                  "type" => "colorpicker",
                  "class" => "",
                  "heading" => esc_attr__("Subtext Color", "luxe-text-domain"),
                  "param_name" => "subtext_color",
                  "value" => "",
                ),
    		  array(
    			 "type" => "textfield",
    			 "class" => "",
    			 "heading" => esc_attr__("Counter Value", "luxe-text-domain"),
    			 "param_name" => "counter_value",
    			 "value" => "1250",
    			 "description" => esc_attr__("Enter number for counter without any special characters. You may enter a decimal number. Eg 12.76", "luxe-text-domain")
    		  ),
    		  array(
    			 "type" => "textfield",
    			 "class" => "",
    			 "heading" => esc_attr__("Thousands Separator", "luxe-text-domain"),
    			 "param_name" => "counter_sep",
    			 "value" => ",",
    			 "description" => esc_attr__("Enter character for thousanda separator. e.g. ',' will separate 125000 into 125,000", "luxe-text-domain")
    		  ),
    		  array(
    			 "type" => "textfield",
    			 "class" => "",
    			 "heading" => esc_attr__("Replace Decimal Point With", "luxe-text-domain"),
    			 "param_name" => "counter_decimal",
    			 "value" => ".",
    			 "description" => esc_attr__("Did you enter a decimal number (Eg - 12.76) The decimal point '.' will be replaced with value that you will enter above.", "luxe-text-domain"),
    		  ),
    		  array(
    			 "type" => "textfield",
    			 "class" => "",
    			 "heading" => esc_attr__("Counter Value Prefix", "luxe-text-domain"),
    			 "param_name" => "counter_prefix",
    			 "value" => "",
    			 "description" => esc_attr__("Enter prefix for counter value", "luxe-text-domain")
    		  ),
    		  array(
    			 "type" => "textfield",
    			 "class" => "",
    			 "heading" => esc_attr__("Counter Value Suffix", "luxe-text-domain"),
    			 "param_name" => "counter_suffix",
    			 "value" => "",
    			 "description" => esc_attr__("Enter suffix for counter value", "luxe-text-domain")
    		  ),
    		  array(
    				"type" => "textfield",
    				"class" => "",
    				"heading" => esc_attr__("Counter rolling time", "luxe-text-domain"),
    				"param_name" => "speed",
    				"value" => 3,
    				"min" => 1,
    				"max" => 10,
    				"suffix" => "seconds",
    				"description" => esc_attr__("How many seconds the counter should roll?", "luxe-text-domain")
    			),
    			array(
    				"type" => "colorpicker",
    				"class" => "",
    				"heading" => esc_attr__("Counter Digits Color", "luxe-text-domain"),
    				"param_name" => "counter_digits_color",
    				"value" => "",
    				"description" => esc_attr__("Select text color for the counter digits.", "luxe-text-domain"),	
    			),
    			array(
    				"type" => "textfield",
    				"class" => "",
    				"heading" => esc_attr__("Extra Class",  "luxe-text-domain"),
    				"param_name" => "el_class",
    				"value" => "",
    				"description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.",  "luxe-text-domain"),
    			),
                array(
                    "type" => "textfield",
                    "param_name" => "text_font_size",
                    "heading" => esc_attr__("Text size","luxe-text-domain"),
                    "value" => "",
                ),
              array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => esc_attr__("Counter Font Size", "luxe-text-domain"),
                    "param_name" => "counter_size",
                    "suffix" => "px",
                    "description" => esc_attr__("Enter value in pixels.", "luxe-text-domain")
                ),
                array(
                    "type" => "textfield",
                    "param_name" => "subtext_font_size",
                    "heading" => esc_attr__("Subtext size","luxe-text-domain"),
                    "value" => "",
                    "description" => esc_attr__("Enter value in pixels.", "luxe-text-domain")
                ),
    		),
    	)
    );
}
add_action( 'vc_before_init', 'luxe_counter_vc', 100 );


/**
 * Luxe animated digits counter
 */

function luxe_counter($atts, $content = null)
{
    extract(shortcode_atts( array(
        'icon_type' => '',
        'icon' => '',
        'icon_img' => '',
        'img_width' => '',
        'icon_size' => '',              
        'icon_color' => '',
        'icon_link' => '',
        'counter_digits_color' => '',
        'counter_text' => '',
        'counter_subtext' => '',
        'counter_value' => '789',
        'counter_sep' => ',',
        'counter_suffix' => '',
        'counter_prefix' => '',
        'counter_decimal' => '.',
        'icon_position'=>'top',
        'speed'=>'',
        'counter_size' => '',
        'text_font_size' => '',
        'subtext_font_size' => '',
        'text_color' => '',
        'subtext_color' => '',
        'el_class'=>'',
    ),$atts));           

    // Icon inline CSS
    $icon_inline_css = 'style="';
    if (!empty($icon_size)) {
        $icon_inline_css .= 'font-size:'.$icon_size.'px;';
    }
    if (!empty($icon_color)) {
        $icon_inline_css .= 'color:' . $icon_color . ';';
    }
    $icon_inline_css .= '"';
    // Counter inline CSS
    $counter_inline_css = 'style="';
    if (!empty($counter_size)) {
        $counter_inline_css .= 'font-size:'.$counter_size.'px;';
    }
    if (!empty($counter_digits_color)) {
        $counter_inline_css .= 'color:' . $counter_digits_color . ';';
    }
    $counter_inline_css .= '"';
    // Text inline CSS
    $text_inline_css = 'style="';
    if (!empty($text_font_size)) {
        $text_inline_css .= 'font-size:'.$text_font_size.'px;';
    }
    if (!empty($text_color)) {
        $text_inline_css .= 'color:'.$text_color.';';
    }
    $text_inline_css .= '"';
    // Subtext inline CSS
    $subtext_inline_css = 'style="';
    if (!empty($subtext_font_size)) {
        $subtext_inline_css .= 'font-size:'.$subtext_font_size.'px;';
    }
    if (!empty($subtext_color)) {
        $subtext_inline_css .= 'color:'.$subtext_color.';';
    }
    $subtext_inline_css .= '"';

    $icon_class = 'icon-position-'.$icon_position;
    ob_start();
    ?>
    <div class="counter counter-<?php echo $icon_position; ?> wpb_content_element <?php echo $el_class; ?>">
        <?php if ((!empty($icon) || !empty($icon_img)) && $icon_type != 'none') { ?>
            <div class="counter-icon <?php echo $icon_class; ?>">
                <?php if ($icon_type == 'custom' && $icon_img) { ?>
                    <img src="<?php $image = wp_get_attachment_image_src( $icon_img, 'full' ); echo $image[0]; ?>">
                <?php } else { ?>
                    <i class="icon <?php echo $icon; ?>" <?php echo $icon_inline_css; ?>></i>
                <?php } ?>
            </div>
        <?php } ?>
        <div class="counter-main">
            <div class="counter-number h1" <?php echo $counter_inline_css; ?> data-prefix="<?php echo $counter_prefix; ?>" data-suffix="<?php echo $counter_suffix; ?>" data-speed="<?php echo $speed; ?>" data-end-value="<?php echo $counter_value; ?>" data-separator="<?php echo $counter_sep; ?>" data-decimal="<?php echo $counter_decimal; ?>" data-decimal-places="<?php echo strlen(substr(strrchr($counter_value, "."), 1)); ?>">0</div>
            <div class="counter-text h4" <?php echo $text_inline_css; ?>><?php echo $counter_text; ?></div>
            <div class="counter-subtext h5" <?php echo $subtext_inline_css; ?>><?php echo $counter_subtext; ?></div>
        </div>
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_counter', 'luxe_counter' );
