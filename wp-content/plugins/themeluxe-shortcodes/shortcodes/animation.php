<?php

use Luxe\Helper;

function luxe_animation_vc() {
	$animations = Helper\get_animations();

	vc_map( 
		array(
			"name" => esc_attr__("Animation Block", "luxe-text-domain"),
			"base" => "luxe_animation",
			"icon" => "luxe_animation",
			"class" => "luxe_animation",
			"as_parent" => array('except' => 'luxe_animation'),
			"content_element" => true,
			"controls" => "full",
			"show_settings_on_create" => true,
			"category" => "Content",
			"description" => esc_attr__("Animate an element.",'luxe-text-domain'),
			"is_container"    => false,
			"params" => array(
				// add params same as with any other content element
				array(
					"type" => "dropdown",
					"class" => "",
					"heading" => esc_attr__("Animation","luxe-text-domain"),
					"param_name" => "animation",
					"value" => $animations,
			  	),
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_attr__("Animation Duration","luxe-text-domain"),
					"param_name" => "duration",
					"value" => 800,
					"min" => 100,
					"max" => 10000,
					"description" => esc_attr__("How long the animation effect should last in milliseconds (e.g., 1500 would be 1.5 seconds).","luxe-text-domain"),
			  	),
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_attr__("Animation Delay","luxe-text-domain"),
					"param_name" => "delay",
					"value" => 0,
					"min" => 0,
					"max" => 10000,
					"suffix" => "s",
					"description" => esc_attr__("How long to delay the animation after it has appeared on screen.","luxe-text-domain"),
			  	),
				array(
					"type" => "textfield",
					"class" => "",
					"heading" => esc_attr__("Animation Loop","luxe-text-domain"),
					"param_name" => "loop",
					"value" => 1,
					"min" => 0,
					"max" => 100,
					"suffix" => "",
					"description" => esc_attr__("The number of times you'd like the animation to play.  Enter <strong>true</strong> to repeat indefinitely.","luxe-text-domain"),
			  	),
				array(
					'type' => 'checkbox',
					'heading' => esc_attr__( 'Hide element before animating', 'luxe-text-domain' ),
					'param_name' => 'hide',
					'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
					'description' => esc_attr__( 'Element only becomes visible after being animated.', 'luxe-text-domain' ),
					'std' => 'true'
				),
				// array(
				// 	"type" => "number",
				// 	"class" => "",
				// 	"heading" => esc_attr__("Viewport Position", "luxe-text-domain"),
				// 	"param_name" => "opacity_start_effect",
				// 	"suffix" => "%",
				// 	//"admin_label" => true,
				// 	"value" => "90",
				// 	"description" => esc_attr__("The area of screen from top where animation effects will start working.", "luxe-text-domain"),
				// ),
				array(
					"type" => "textfield",
					"heading" => esc_attr__("Extra class name", "luxe-text-domain"),
					"param_name" => "el_class",
					"description" => esc_attr__("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "luxe-text-domain")
				),
			),
			"js_view" => 'VcColumnView'
		)
	);
}
add_action( 'vc_before_init', 'luxe_animation_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Animation extends WPBakeryShortCodesContainer {
    }
}

/**
 * Luxe animation
 */
function luxe_animation($atts, $content = null)
{
    extract(shortcode_atts( array(
        'animation' => 'transition.fadeIn',
        'duration' => '800',
        'delay' => '0',
        'loop' => 'false',
        'hide' => 'true',
        'el_class'=>'',
    ),$atts));           

    $classes = array();
    $classes[] = $el_class;
    $classes[] = ((!empty($hide)) ? '' : 'visible');

    ob_start();
    ?>
    <div class="has-animation <?php echo implode(' ', $classes); ?>" data-animation="<?php echo $animation; ?>" data-animation-duration="<?php echo $duration; ?>" data-animation-delay="<?php echo $delay; ?>" data-animation-loop="<?php echo $loop; ?>">
        <?php echo do_shortcode( $content ); ?>
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_animation', 'luxe_animation' );