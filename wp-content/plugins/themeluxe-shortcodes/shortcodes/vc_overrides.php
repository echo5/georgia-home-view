<?php

use Luxe\Helper;

if ( !Helper\is_vc_active())
    return false;

/**
 * Turn off VC scripts
 */
function remove_vc_scripts() {
    if ((get_theme_mod('visual_composer', true) == false) && !is_admin() && !(vc_is_page_editable())) {
        wp_dequeue_style( 'js_composer_front' );
        wp_deregister_style( 'js_composer_front' );
        wp_dequeue_script( 'wpb_composer_front_js' );
        wp_deregister_script( 'wpb_composer_front_js' );
    }
}
add_action('wp_enqueue_scripts', 'remove_vc_scripts', 9999);

/**
 * Remove disabled VC shortcodes
 */
function remove_vc_shortcodes() {
    if ((get_theme_mod('visual_composer', true) == false)) {
        vc_remove_element( 'vc_btn' );
        // vc_remove_element( 'vc_column_text' );
        vc_remove_element( 'vc_basic_grid' );
        vc_remove_element( 'vc_media_grid' );
        vc_remove_element( 'vc_masonry_grid' );
        vc_remove_element( 'vc_icon' );
        vc_remove_element( 'vc_cta' );
        vc_remove_element( 'vc_separator' );
        vc_remove_element( 'vc_text_separator' );
        vc_remove_element( 'vc_message' );
        vc_remove_element( 'vc_facebook' );
        vc_remove_element( 'vc_tweetmeme' );
        vc_remove_element( 'vc_googleplus' );
        vc_remove_element( 'vc_pinterest' );
        vc_remove_element( 'vc_toggle' );
        // vc_remove_element( 'vc_single_image' );
        vc_remove_element( 'vc_gallery' );
        vc_remove_element( 'vc_images_carousel' );
        vc_remove_element( 'vc_tabs' );
        vc_remove_element( 'vc_tour' );
        vc_remove_element( 'vc_accordion' );
        vc_remove_element( 'vc_posts_grid' );
        vc_remove_element( 'vc_carousel' );
        vc_remove_element( 'vc_posts_slider' );
        // vc_remove_element( 'vc_widget_sidebar' );
        vc_remove_element( 'vc_button' );
        vc_remove_element( 'vc_cta_button' );
        vc_remove_element( 'vc_video' );
        vc_remove_element( 'vc_gmaps' );
        // vc_remove_element( 'vc_raw_html' );
        // vc_remove_element( 'vc_raw_js' );
        vc_remove_element( 'vc_flickr' );
        vc_remove_element( 'vc_progress_bar' );
        vc_remove_element( 'vc_pie' );
        // vc_remove_element( 'contact-form-7' );
        // vc_remove_element( 'rev_slider_vc' );
        vc_remove_element( 'vc_wp_search' );
        vc_remove_element( 'vc_wp_meta' );
        vc_remove_element( 'vc_wp_recentcomments' );
        vc_remove_element( 'vc_wp_calendar' );
        vc_remove_element( 'vc_wp_pages' );
        vc_remove_element( 'vc_wp_tagcloud' );
        vc_remove_element( 'vc_wp_custommenu' );
        vc_remove_element( 'vc_wp_text' );
        vc_remove_element( 'vc_wp_posts' );
        vc_remove_element( 'vc_wp_links' );
        vc_remove_element( 'vc_wp_categories' );
        vc_remove_element( 'vc_wp_archives' );
        vc_remove_element( 'vc_wp_rss' );
    }
}
add_action( 'admin_init', 'remove_vc_shortcodes' );

/**
 * Remove unused features if VC styles disabled
 */
function remove_vc_unused_features() {
    if ((get_theme_mod('visual_composer', true) == false)) {
        vc_remove_param( 'vc_column_text', 'css_animation' );
        vc_remove_param( 'vc_single_image', 'style' );
        vc_remove_param( 'vc_single_image', 'onclick' );
        vc_remove_param( 'vc_single_image', 'css_animation' );
    }
}
add_action('init', 'remove_vc_unused_features', 100);


/**
 * Replace classes with bootstrap classes
 */
function custom_css_classes_for_vc_row_and_vc_column( $class_string, $tag ) {
    if (!is_admin()) {
        if ( $tag == 'vc_row' || $tag == 'vc_row_inner' ) {
            // $class_string = str_replace( 'vc_row', 'row', $class_string ); 
            $class_string = str_replace( 'vc_row-fluid', 'row', $class_string ); 
            $class_string = str_replace( 'vc_row-flex', 'row-flex', $class_string ); 
        }
        if ( $tag == 'vc_column' || $tag == 'vc_column_inner' ) {
            $class_string = str_replace( 'vc_column_container', 'column-container', $class_string ); 
            // $class_string = str_replace( 'vc_column-inner', 'column-inner', $class_string ); 
            $class_string = preg_replace( '/vc_col-xs-(\d{1,2})/', 'col-xs-$1', $class_string );
            $class_string = preg_replace( '/vc_col-sm-(\d{1,2})/', 'col-sm-$1', $class_string );
            $class_string = preg_replace( '/vc_col-md-(\d{1,2})/', 'col-md-$1', $class_string );
            $class_string = preg_replace( '/vc_col-lg-(\d{1,2})/', 'col-lg-$1', $class_string );
            $class_string = preg_replace( '/vc_col-xs-offset-(\d{1,2})/', 'col-xs-offset-$1', $class_string );
            $class_string = preg_replace( '/vc_col-sm-offset-(\d{1,2})/', 'col-sm-offset-$1', $class_string );
            $class_string = preg_replace( '/vc_col-md-offset-(\d{1,2})/', 'col-md-offset-$1', $class_string );
            $class_string = preg_replace( '/vc_col-lg-offset-(\d{1,2})/', 'col-lg-offset-$1', $class_string );
            $class_string = preg_replace( '/vc_hidden-xs/', 'hidden-xs', $class_string );
            $class_string = preg_replace( '/vc_hidden-sm/', 'hidden-sm', $class_string );
            $class_string = preg_replace( '/vc_hidden-md/', 'hidden-md', $class_string );
            $class_string = preg_replace( '/vc_hidden-lg/', 'hidden-lg', $class_string );
        }
        return $class_string; 
    }
}
add_filter( 'vc_shortcodes_css_class', 'custom_css_classes_for_vc_row_and_vc_column', 100, 2 );
add_filter( 'vc_frontend_editor_to_string', 'custom_css_classes_for_vc_row_and_vc_column', 100, 2 );

/**
 * Remove container if VC active
 */
function add_has_vc_row_class($classes) {
    global $post;
    if( is_a( $post, 'WP_Post' ) && has_shortcode( $post->post_content, 'vc_row') ) {
        $classes[] = 'has_vc_row';
    }
    return $classes;
}
add_filter('body_class', 'add_has_vc_row_class');


/**
 * Update VC shortcodes
 */
function update_vc_shortcodes() {

    $animations = Helper\get_animations();

    // Custom Headings
    $vc_shortcode = WPBMap::getShortCode('vc_custom_heading');
    foreach($vc_shortcode['params'] as $key => $param)
    {
        if($param['param_name'] === 'use_theme_fonts')
        {
            $vc_shortcode['params'][$key]['std'] = 'yes';
        }
    } 
    unset($vc_shortcode['base']);
    vc_map_update('vc_custom_heading', $vc_shortcode);

    // Buttons
    $vc_shortcode = WPBMap::getShortCode('vc_btn');
    foreach($vc_shortcode['params'] as $key => $param)
    {
        if($param['param_name'] === 'shape')
        {
            $vc_shortcode['params'][$key]['dependency'] = array(
                'element' => 'style',
                'value_not_equal_to' => array(
                    'theme-default'
                ),
            );
            $vc_shortcode['params'][$key]['value'] = array(
                esc_attr__( 'Theme Default', 'luxe-text-domain' ) => 'theme-default',
                esc_attr__( 'Rounded', 'luxe-text-domain' ) => 'rounded',
                esc_attr__( 'Square', 'luxe-text-domain' ) => 'square',
                esc_attr__( 'Round', 'luxe-text-domain' ) => 'round',
            );
            $vc_shortcode['params'][$key]['std'] = 'theme-default';
            // $vc_shortcode['params'][$key]['std'] = get_theme_mod('button_shape', 'theme-default');
        }
        elseif($param['param_name'] === 'style')
        {
            $vc_shortcode['params'][$key]['value'] = array(
                esc_attr__( 'Theme Default', 'luxe-text-domain' ) => 'theme-default',
                esc_attr__( 'Theme Primary', 'luxe-text-domain' ) => 'theme-primary',
                esc_attr__( 'Modern', 'luxe-text-domain' ) => 'modern',
                esc_attr__( 'Classic', 'luxe-text-domain' ) => 'classic',
                esc_attr__( 'Flat', 'luxe-text-domain' ) => 'flat',
                esc_attr__( 'Outline', 'luxe-text-domain' ) => 'outline',
                esc_attr__( '3d', 'luxe-text-domain' ) => '3d',
                esc_attr__( 'Custom', 'luxe-text-domain' ) => 'custom',
                esc_attr__( 'Outline custom', 'luxe-text-domain' ) => 'outline-custom',
            );
            $vc_shortcode['params'][$key]['std'] = 'theme-default';
        }
        elseif($param['param_name'] === 'color')
        {
            $vc_shortcode['params'][$key]['dependency'] = array(
                'element' => 'style',
                'value_not_equal_to' => array(
                    'custom',
                    'outline-custom',
                    'theme-default',
                    'theme-primary',
                ),
            );
        }
    } 
    unset($vc_shortcode['base']);
    vc_map_update('vc_btn', $vc_shortcode);

    // Posts Grid
    $vc_shortcode = WPBMap::getShortCode('vc_basic_grid');
    foreach($vc_shortcode['params'] as $key => $param)
    {
        if($param['param_name'] === 'btn_style')
        {
            $vc_shortcode['params'][$key]['std'] = 'theme-default';
        }
    } 
    unset($vc_shortcode['base']);
    vc_map_update('vc_basic_grid', $vc_shortcode);

    // VC Rows
    $vc_shortcode = WPBMap::getShortCode('vc_row');
    $vc_shortcode['params']['background_position'] = array(
        'type' => 'textfield',
        'heading' => esc_attr__( 'Background Position', 'luxe-text-domain' ),
        'param_name' => 'bg_position',
        'value' => '',
        'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
        'description' => esc_attr__( 'This determines how the background is placed. You can enter in percentage (x% y%) or pixels (i.e., 50px 100px). Page background is automatically centered if set to fixed and covered on Chrome due to a bug in Chrome.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['background_overlay'] = array(
        'type' => 'colorpicker',
        'heading' => esc_attr__( 'Background Overlay', 'luxe-text-domain' ),
        'param_name' => 'bg_overlay',
        'value' => '',
        'description' => esc_attr__( 'Places an overlay on top of your background which you can set the level of transparency.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['scroll_down'] = array(
        'type' => 'checkbox',
        'heading' => esc_attr__( 'Scroll Down Arrow', 'luxe-text-domain' ),
        'param_name' => 'scroll_down',
        'value' => '',
        'description' => esc_attr__( 'Adds a scroll down arrow to the bottom of your row if enabled.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['scroll_down_position'] = array(
        'type' => 'dropdown',
        'heading' => esc_attr__( 'Scroll Down Arrow Position', 'luxe-text-domain' ),
        'param_name' => 'scroll_down_position',
        'value' => array(
            esc_attr__( 'Left', 'luxe-text-domain' ) => 'left',
            esc_attr__( 'Center', 'luxe-text-domain' ) => 'center',
            esc_attr__( 'Right', 'luxe-text-domain' ) => 'right',
        ),
        'description' => esc_attr__( 'Select scroll down arrow position.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['scroll_down_color'] = array(
        'type' => 'colorpicker',
        'heading' => esc_attr__( 'Scroll Down Arrow Color', 'luxe-text-domain' ),
        'param_name' => 'scroll_down_color',
        'value' => '',
        'description' => esc_attr__( 'Color of your scroll down icon.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['video_iframe'] = array(
        'type' => 'textarea_raw_html',
        'heading' => esc_attr__( 'Video Iframe', 'luxe-text-domain' ),
        'param_name' => 'video_iframe',
        'value' => '',
        'description' => esc_attr__( 'Copy the embed code for your video here.', 'luxe-text-domain' ),
        'dependency' => array(
            'element' => 'video_bg',
            'value' => 'yes',
        ),
        'priority' => 10
    );
    $vc_shortcode['params']['animate'] = array(
        'type' => 'checkbox',
        'heading' => esc_attr__( 'Animate Row', 'luxe-text-domain' ),
        'param_name' => 'animate',
        'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'yes' ),
        'description' => esc_attr__( 'Check to select an animation for the row.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['animation'] = array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_attr__("Animation","luxe-text-domain"),
        "param_name" => "animation",
        "value" => $animations,
        'dependency' => array(
            'element' => 'animate',
            'value' => 'yes',
        ),
    );
    unset($vc_shortcode['base']);
    vc_map_update('vc_row', $vc_shortcode);
    vc_remove_param( 'vc_row', 'parallax_image' );
    vc_remove_param( 'vc_row', 'parallax_speed_bg' );
    vc_remove_param( 'vc_row', 'video_bg_url' );
    vc_remove_param( 'vc_row', 'gap' );

    // VC Columns
    $vc_shortcode = WPBMap::getShortCode('vc_column');
    $vc_shortcode['params']['animate'] = array(
        'type' => 'checkbox',
        'heading' => esc_attr__( 'Animate Column', 'luxe-text-domain' ),
        'param_name' => 'animate',
        'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'yes' ),
        'description' => esc_attr__( 'Check to select an animation for the column.', 'luxe-text-domain' ),
    );
    $vc_shortcode['params']['animation'] = array(
        "type" => "dropdown",
        "class" => "",
        "heading" => esc_attr__("Animation","luxe-text-domain"),
        "param_name" => "animation",
        "value" => $animations,
        'dependency' => array(
            'element' => 'animate',
            'value' => 'yes',
        ),
    );

    unset($vc_shortcode['base']);
    vc_map_update('vc_column', $vc_shortcode);

}
add_action('init', 'update_vc_shortcodes', 100);


