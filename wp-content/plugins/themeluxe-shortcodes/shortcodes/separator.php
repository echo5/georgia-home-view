<?php

/**
 * Luxe separator VC map
 */

function luxe_separator_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Separator","luxe-text-domain"),
    	   "base" => "luxe_separator",
    	   "class" => "luxe_separator",
    	   "icon" => "separator",
    	   "category" => "Content",
    	   "description" => esc_attr__("A line that generally separates headings from content.","luxe-text-domain"),
    	   "params" => array(
    			array(
    				"type" => "dropdown",
    				"class" => "",
    				"heading" => esc_attr__("Width", "luxe-text-domain"),
    				"param_name" => "width",
    				"value" => array(
                        esc_attr__("10%","luxe-text-domain") => "10%",
                        esc_attr__("20%","luxe-text-domain") => "20%",
                        esc_attr__("30%","luxe-text-domain") => "30%",
                        esc_attr__("40%","luxe-text-domain") => "40%",
                        esc_attr__("50%","luxe-text-domain") => "50%",
                        esc_attr__("60%","luxe-text-domain") => "60%",
                        esc_attr__("70%","luxe-text-domain") => "70%",
                        esc_attr__("80%","luxe-text-domain") => "80%",
                        esc_attr__("90%","luxe-text-domain") => "90%",
                        esc_attr__("100%","luxe-text-domain") => "100%",
    				),
    				"description" => esc_attr__("Width of the separator", "luxe-text-domain")
    			),
    			array(
    				"type" => "colorpicker",
    				"class" => "",
    				"heading" => esc_attr__("Color", "luxe-text-domain"),
    				"param_name" => "color",
    				"value" => "",
    			),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Alignment", "luxe-text-domain"),
                    "param_name"  => "alignment",
                    "value"       => array(
                        esc_attr__('Left', 'luxe-text-domain')  => 'left',
                        esc_attr__('Center', 'luxe-text-domain')  => 'center',
                        esc_attr__('Right', 'luxe-text-domain') => 'right',
                    ),
                    "description" => esc_attr__("Select position of button", "luxe-text-domain"),
                ),
                array(
                    "type" => "textfield",
                    "class" => "",
                    "heading" => esc_attr__("Extra Class",  "luxe-text-domain"),
                    "param_name" => "el_class",
                    "value" => "",
                    "description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.",  "luxe-text-domain"),
                ),
    		),
    	)
    );
}
add_action( 'vc_before_init', 'luxe_separator_vc', 100 );

/**
 * Luxe separator
 */
function luxe_separator($atts, $content = null)
{
    extract(shortcode_atts( array(
        'color' => '',
        'width' => '10%',
        'alignment' => 'left',
        'el_class'=>'',
    ),$atts));           

    // Icon inline CSS
    $inline_css = 'style="';
    if (!empty($width)) {
        $inline_css .= 'width:'.$width.';';
    }
    if (!empty($color)) {
        $inline_css .= 'background-color:' . $color . ';';
    }
    $inline_css .= '"';

    ob_start();
    ?>
        <div class="luxe-separator <?php echo $alignment; ?> <?php echo $el_class; ?>" <?php echo $inline_css; ?>></div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_separator', 'luxe_separator' );
