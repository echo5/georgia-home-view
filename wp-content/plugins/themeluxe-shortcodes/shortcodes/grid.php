<?php

use Luxe\Helper;

function luxe_grid_map() {

    // Get post types list
    $post_types_list = Helper\get_post_types_list();

    // Get grid array
    $grid_cols_list = Helper\get_grid_list();

    // Get taxonomies
    $taxonomies_for_filter = array();
    $taxonomies = get_taxonomies();
    if ( is_array( $taxonomies ) && ! empty( $taxonomies ) ) {
        foreach ( $taxonomies as $key => $value ) {
            $taxonomies_for_filter[ $key ] = $value;
        }
    }

    // Map shortcode
   vc_map( array(
      "name" => esc_attr__( "Luxe Grid", "luxe-text-domain" ),
      "base" => "luxe_grid",
      "class" => "",
      "category" => esc_attr__( "Content", "luxe-text-domain"),
      "params" => array(
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Data source', 'luxe-text-domain' ),
      		'param_name' => 'post_type',
      		'value' => $post_types_list,
      		'save_always' => true,
      		'description' => esc_attr__( 'Select content type for your grid.', 'luxe-text-domain' ),
      	),
      	array(
      		'type' => 'autocomplete',
      		'heading' => esc_attr__( 'Include only', 'luxe-text-domain' ),
      		'param_name' => 'include',
      		'description' => esc_attr__( 'Add posts, pages, etc. by title.', 'luxe-text-domain' ),
      		'settings' => array(
      			'multiple' => true,
      			'sortable' => true,
      			'groups' => true,
      		),
      		'dependency' => array(
      			'element' => 'post_type',
      			'value' => array( 'ids' ),
      		),
      	),
      	// Custom query tab
      	array(
      		'type' => 'textarea_safe',
      		'heading' => esc_attr__( 'Custom query', 'luxe-text-domain' ),
      		'param_name' => 'custom_query',
      		'description' => esc_attr__( 'Build custom query according to <a href="http://codex.wordpress.org/Function_Reference/query_posts">WordPress Codex</a>.', 'luxe-text-domain' ),
      		'dependency' => array(
      			'element' => 'post_type',
      			'value' => array( 'custom' ),
      		),
      	),
      	array(
      		'type' => 'textfield',
      		'heading' => esc_attr__( 'Total items', 'luxe-text-domain' ),
      		'param_name' => 'max_items',
      		'value' => 10, // default value
      		'param_holder_class' => 'vc_not-for-custom',
      		'description' => esc_attr__( 'Set max limit for items in grid.', 'luxe-text-domain' ),
      		'dependency' => array(
      			'element' => 'post_type',
      			'value_not_equal_to' => array( 'ids', 'custom' ),
      		),
      	),
      	array(
      		'type' => 'checkbox',
      		'heading' => esc_attr__( 'Show filter', 'luxe-text-domain' ),
      		'param_name' => 'show_filter',
      		'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'yes' ),
      		'description' => esc_attr__( 'Append filter to grid.', 'luxe-text-domain' ),
      	),
        array(
            'type' => 'textfield',
            'heading' => esc_attr__( 'Image size', 'luxe-text-domain' ),
            'param_name' => 'img_size',
            'value' => 'full',
            'description' => esc_attr__( 'Enter image size.  Leave blank for content default. (Example: "thumbnail", "medium", "large", "full" or other sizes defined by theme).', 'luxe-text-domain' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => esc_attr__( 'Portfolio Style', 'luxe-text-domain' ),
          'param_name' => 'portfolio_style',
          'value' => array(
            esc_attr__( 'Display info on hover', 'luxe-text-domain' ) => 'overlay-hover',
            esc_attr__( 'Display info on top of item', 'luxe-text-domain' ) => 'overlay',
            esc_attr__( 'Display info below item', 'luxe-text-domain' ) => 'below',
          ),
          'dependency' => array(
            'element' => 'post_type',
            'value' => array( 'portfolio' ),
          ),
          'description' => esc_attr__( 'Modify the image style for all images in the grid.', 'luxe-text-domain' ),
        ),
        array(
          'type' => 'dropdown',
          'heading' => esc_attr__( 'Image Style', 'luxe-text-domain' ),
          'param_name' => 'img_style',
          'value' => array(
            esc_attr__( 'Default', 'luxe-text-domain' ) => '',
            esc_attr__( 'Black and white', 'luxe-text-domain' ) => 'images-b-and-w',
          ),
          'description' => esc_attr__( 'Modify the image style for all images in the grid.', 'luxe-text-domain' ),
        ),
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Grid elements per row', 'luxe-text-domain' ),
      		'param_name' => 'element_width',
      		'value' => $grid_cols_list,
      		'std' => '3',
      		'edit_field_class' => 'vc_col-sm-6 vc_column',
      		'description' => esc_attr__( 'Select number of single grid elements per row.', 'luxe-text-domain' ),
      	),
        array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Padding Between Items', 'luxe-text-domain' ),
      		'param_name' => 'item_padding',
      		'value' => array(
      			esc_attr__('Default Padding', 'luxe-text-domain') => '',
      			esc_attr__('No Padding', 'luxe-text-domain') => 'no-padding',
      		),
      		'std' => '30',
      		'description' => esc_attr__( 'Select gap between grid elements.', 'luxe-text-domain' ),
      		'edit_field_class' => 'vc_col-sm-6 vc_column',
      	),
        array(
            'type' => 'colorpicker',
            'heading' => esc_attr__( 'Grid Item Background Color', 'luxe-text-domain' ),
            'param_name' => 'grid_item_bg_color',
            'value' => getVcShared( 'colors' ),
            'param_holder_class' => 'vc_colored-dropdown',
            'description' => esc_attr__( 'The color behind your grid items.  Use this if you want them to stand out against your background.', 'luxe-text-domain' ),
        ),
      	// Data settings
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Order by', 'luxe-text-domain' ),
      		'param_name' => 'orderby',
      		'value' => array(
      			esc_attr__( 'Date', 'luxe-text-domain' ) => 'post_date',
      			esc_attr__( 'Order by post ID', 'luxe-text-domain' ) => 'ID',
      			// esc_attr__( 'Author', 'luxe-text-domain' ) => 'author',
      			esc_attr__( 'Title', 'luxe-text-domain' ) => 'post_title',
      			esc_attr__( 'Last modified date', 'luxe-text-domain' ) => 'post_modified',
      			esc_attr__( 'Post/page parent ID', 'luxe-text-domain' ) => 'post_parent',
      			esc_attr__( 'Number of comments', 'luxe-text-domain' ) => 'comment_count',
      			esc_attr__( 'Menu order/Page Order', 'luxe-text-domain' ) => 'menu_order',
      			// esc_attr__( 'Meta value', 'luxe-text-domain' ) => 'meta_value',
      			// esc_attr__( 'Meta value number', 'luxe-text-domain' ) => 'meta_value_num',
      			esc_attr__( 'Random order', 'luxe-text-domain' ) => 'rand()',
      		),
      		'description' => esc_attr__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'luxe-text-domain' ),
      		'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
      		'param_holder_class' => 'vc_grid-data-type-not-ids',
      		'dependency' => array(
      			'element' => 'post_type',
      			'value_not_equal_to' => array( 'ids', 'custom' ),
      		),
      	),
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Sort order', 'luxe-text-domain' ),
      		'param_name' => 'order',
      		'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
      		'value' => array(
      			esc_attr__( 'Descending', 'luxe-text-domain' ) => 'DESC',
      			esc_attr__( 'Ascending', 'luxe-text-domain' ) => 'ASC',
      		),
      		'param_holder_class' => 'vc_grid-data-type-not-ids',
      		'description' => esc_attr__( 'Select sorting order.', 'luxe-text-domain' ),
      		'dependency' => array(
      			'element' => 'post_type',
      			'value_not_equal_to' => array( 'ids', 'custom' ),
      		),
      	),
        array(
      		'type' => 'textfield',
      		'heading' => esc_attr__( 'Meta key', 'luxe-text-domain' ),
      		'param_name' => 'meta_key',
      		'description' => esc_attr__( 'Input meta key for grid ordering.', 'luxe-text-domain' ),
      		'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
      		'param_holder_class' => 'vc_grid-data-type-not-ids',
      		'dependency' => array(
      			'element' => 'orderby',
      			'value' => array( 'meta_value', 'meta_value_num' ),
      		),
      	),
      	array(
      		'type' => 'textfield',
      		'heading' => esc_attr__( 'Offset', 'luxe-text-domain' ),
      		'param_name' => 'offset',
      		'description' => esc_attr__( 'Number of grid elements to displace or pass over.', 'luxe-text-domain' ),
      		'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
      		'param_holder_class' => 'vc_grid-data-type-not-ids',
      		'dependency' => array(
      			'element' => 'post_type',
      			'value_not_equal_to' => array( 'ids', 'custom' ),
      		),
      	),
      	//Filter tab
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Filter by', 'luxe-text-domain' ),
      		'param_name' => 'filter_source',
      		'value' => $taxonomies_for_filter,
      		'group' => esc_attr__( 'Filter', 'luxe-text-domain' ),
      		'dependency' => array(
      			'element' => 'show_filter',
      			'value' => array( 'yes' ),
      		),
      		'save_always' => true,
      		'description' => esc_attr__( 'Select filter source.', 'luxe-text-domain' ),
      	),
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Style', 'luxe-text-domain' ),
      		'param_name' => 'filter_style',
      		'value' => array(
                esc_attr__( 'Default', 'luxe-text-domain' ) => 'nav nav-pills',
                esc_attr__( 'Dropdown', 'luxe-text-domain' ) => 'dropdown-menu',
                esc_attr__( 'Sidebar list', 'luxe-text-domain' ) => 'list-group',
      		),
      		'dependency' => array(
      			'element' => 'show_filter',
      			'value' => array( 'yes' ),
      		),
      		'group' => esc_attr__( 'Filter', 'luxe-text-domain' ),
      		'description' => esc_attr__( 'Select filter display style.', 'luxe-text-domain' ),
      	),
        array(
            'type' => 'checkbox',
            'heading' => esc_attr__( 'Fix on scroll', 'luxe-text-domain' ),
            'param_name' => 'filter_fix_on_scroll',
            'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'yes' ),
            'group' => esc_attr__( 'Filter', 'luxe-text-domain' ),
            'dependency' => array(
                'element' => 'filter_style',
                'value' => array( 'list-group' ),
            ),
            'description' => esc_attr__( 'Fix filter list to left of screen while scrolling down portfolio.', 'luxe-text-domain' ),
        ),
      	array(
      		'type' => 'dropdown',
      		'heading' => esc_attr__( 'Alignment', 'luxe-text-domain' ),
      		'param_name' => 'filter_align',
      		'value' => array(
            esc_attr__( 'Left', 'luxe-text-domain' ) => 'left',
      			esc_attr__( 'Center', 'luxe-text-domain' ) => 'center',
      			esc_attr__( 'Right', 'luxe-text-domain' ) => 'right',
      		),
      		'dependency' => array(
      			'element' => 'show_filter',
      			'value' => array( 'yes' ),
      		),
      		'group' => esc_attr__( 'Filter', 'luxe-text-domain' ),
      		'description' => esc_attr__( 'Select filter alignment.', 'luxe-text-domain' ),
      	),
        array(
            'type' => 'colorpicker',
            'heading' => esc_attr__( 'Color', 'luxe-text-domain' ),
            'param_name' => 'filter_color',
            'value' => getVcShared( 'colors' ),
            'param_holder_class' => 'vc_colored-dropdown',
            'group' => esc_attr__( 'Filter', 'luxe-text-domain' ),
            'dependency' => array(
              'element' => 'show_filter',
              'value' => array( 'yes' ),
            ),
            'description' => esc_attr__( 'Pick the color of your filter text.', 'luxe-text-domain' ),
        ),
      	array(
      		'type' => 'textfield',
      		'heading' => esc_attr__( 'Extra class name', 'luxe-text-domain' ),
      		'param_name' => 'el_class',
      		'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'luxe-text-domain' ),
      	),
      	array(
      		'type' => 'css_editor',
      		'heading' => esc_attr__( 'CSS box', 'luxe-text-domain' ),
      		'param_name' => 'css',
      		'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
      	),
      )
   ) );
}
add_action( 'vc_before_init', 'luxe_grid_map' );


function luxe_grid( $atts ) {
    extract (shortcode_atts( array(
        'post_type' => 'post',
        'element_width' => '3',
        'max_items' => '-1',
        'el_class' => '',
        'show_filter' => false,
        'offset' => '0',
        'filter_source' => '',
        'filter_align' => 'left',
        'filter_fix_on_scroll' => '',
        'orderby' => 'post_date',
        'order' => 'DESC',
        'filter_style' => 'nav nav-pills',
        'filter_color' => '',
        'item_padding' => '',
        'grid_item_bg_color' => '',
        'img_size' => 'full',
        'img_style' => '',
        'portfolio_style' => '',
        'flexible_layout' => false,
        'css' => '',
    ), $atts ));

    // Get posts
    $post_args = array(
      'posts_per_page'   => $max_items,
      'offset'           => $offset,
      'orderby'          => $orderby,
      'order'            => $order,
      'post_type'        => $post_type,
      'post_status'      => 'publish',
      'suppress_filters' => true 
    );
    $posts = get_posts( $post_args ); 

    // Get filters
    $term_args = array();
    $terms = get_terms( $filter_source, $term_args );
    $filter_columns = ($filter_style == 'list-group'  && $show_filter) ? 3 : 12;

    // Grid classes
    $grid_classes = array();
    $unique_id = uniqid();
    $grid_columns = ($filter_columns < 12) ? 12 - $filter_columns : 12;
    $column_class = Helper\get_responsive_column_classes($element_width, 'lg');
    $size = $column_class;
    $grid_classes[] = 'grid';
    $grid_classes[] = empty($item_padding) ? 'row' : ''; 
    $grid_classes[] = 'grid-' . $post_type;
    $grid_classes[] = $img_style;
    $grid_classes[] = $el_class;
    $grid_classes[] = !empty($flexible_layout) ? 'grid-flexible' : ''; 
    $grid_classes[] = ($post_type == 'portfolio') ? 'portfolio-style-' . $portfolio_style : ''; 
    // Grid item classes
    $grid_item_classes = array();
    $grid_item_classes[] = 'grid-item';
    $grid_item_classes[] = !empty($item_padding) ? 'no-margin no-padding' : ''; 
    // Grid filter classes
    $grid_filter_classes[] = $filter_style;
    $grid_filter_inline_css = 'style="';
    if ($filter_color) {
      $grid_filter_inline_css .= 'color:' . $filter_color; ';';
    }
    $grid_filter_inline_css .= '"';


    // Images
    global $image_size;
    $image_size = $img_size;
    // Grid item inline styles
    $background_div = $background_div_close = '';
    if (!empty($grid_item_bg_color)) { 
        $background_div = '<div class="grid-item-bg" style="background-color:' . $grid_item_bg_color . ';">';
        $background_div_close = '</div>';
        $grid_item_classes[] = 'grid-item-padded';
    }

    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    ob_start();
    ?>
    <div class="row" class="<?php echo $css_class; ?>">
        <?php if ($show_filter) { ?>
            <div class="col-md-<?php echo $filter_columns; ?>">
                <div class="grid-filter-wrap h5 grid-filter-<?php echo $filter_style; ?> text-align-<?php echo $filter_align; ?>" id="grid-filter-wrap-<?php echo $unique_id; ?>" <?php echo $grid_filter_inline_css; ?>>
                    <div class="dropdown-wrap">
                        <button class="btn dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                            <?php esc_attr_e('Filter', 'luxe-text-domain'); ?>
                            <i class="icon ion-chevron-down"></i>
                        </button>
                        <ul id="grid-filter-<?php echo $unique_id; ?>" class="grid-filter dropdown-menu <?php echo implode(' ', $grid_filter_classes); ?>">
                            <li class="filter active <?php echo $filter_style;?>-item">
                                <a href="#" data-filter="*"><?php esc_attr_e('All', 'luxe-text-domain'); ?></a>
                            </li>
                            <?php foreach ($terms as $term) { ?>
                                <li class="filter <?php echo $filter_style;?>-item">
                                    <a href="#" data-filter=".<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        <?php } ?>
        <div class="col-md-<?php echo $grid_columns; ?>">
            <div id="grid-<?php echo $unique_id; ?>" class="<?php echo implode(' ', $grid_classes); ?> wpb_content_element">
                <?php global $post; ?>
                <?php foreach ($posts as $post) { 
                    setup_postdata( $post );
                    $post_terms = get_the_terms( get_the_ID(), $filter_source);
                    $terms = array();
                    if (is_array($post_terms)) {
                        foreach ( $post_terms as $term ) {
                            $terms[] = $term->slug;
                        }
                    }
                    if ($flexible_layout) {
                        $grid_item_size = get_post_meta( get_the_ID(), '_post_content_grid_item_size', true );
                        if ($grid_item_size) {
                            // $size = $grid_item_size;
                        } else {
                            // $size = 'grid-item-small';
                        }
                    }
                    ?>
                    <div class="<?php echo implode(' ', $grid_item_classes); ?> <?php echo implode(' ', $terms); ?> <?php echo $size; ?>">
                        <?php echo $background_div; ?>
                            <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
                        <?php echo $background_div_close; ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>

    <script> 
    (function($){ 
        $(window).load(function() {
            var grid = $('#grid-<?php echo $unique_id; ?>');
            grid.imagesLoaded(function() {
                grid.isotope({
                    layoutMode: 'masonry',
                    itemSelector: '.grid-item',
                    transitionDuration: '0.5s',
                    masonry: {
                      columnWidth: 0,
                    }
                });
                grid.isotope();
            });
            <?php if ($show_filter) { ?>
                var filters = $('#grid-filter-<?php echo $unique_id; ?> .filter a');
                filters.on( 'click', function(e) {
                    e.preventDefault();
                    var filterValue = $(this).attr('data-filter');
                    grid.isotope({ filter: filterValue });
                    filters.parent().removeClass('active');
                    $(this).parent().addClass('active');
                });
                <?php if ($filter_style == 'list-group' && $filter_fix_on_scroll) { ?>
                    $('#grid-filter-wrap-<?php echo $unique_id; ?>').scrollToFixed({
                        marginTop: $('header.banner').outerHeight() + 40,
                        limit: function() {
                            return grid.offset().top + grid.height() - $(this).height();
                        },
                        minWidth: 992,
                        removeOffsets: true
                    });
                <?php } ?>
            <?php } ?>
        }); 
    })(jQuery);
    </script>

    <?php
    $output = ob_get_clean();
    wp_reset_postdata();
    return $output;
}
add_shortcode( 'luxe_grid', 'luxe_grid' );

?>