<?php

use Luxe\Helper;

/**
 * Luxe link VC map
 */

function luxe_icon_vc()
{
    vc_map(
        array(
            "name"        => esc_attr__("Icon", "luxe-text-domain"),
            "base"        => "luxe_icon",
            "class"       => "luxe_icon",
            "icon"        => "link",
            "category"    => "Content",
            "description" => esc_attr__("An eye catching icon.", "luxe-text-domain"),
            "params"      => array(
                array(
                    "type"       => "iconpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Select Icon ", "luxe-text-domain"),
                    'settings'   => array(
                        'emptyIcon'    => false,
                        'type'         => 'ionicons',
                        'iconsPerPage' => 200,
                    ),
                    "param_name" => "icon",
                    "value"      => "",
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Icon Size", "luxe-text-domain"),
                    "param_name"  => "icon_size",
                    "value"       => array(
                        esc_attr__('Small', 'luxe-text-domain')  => 'small',
                        esc_attr__('Medium', 'luxe-text-domain')  => 'medium',
                        esc_attr__('Large', 'luxe-text-domain') => 'large',
                        esc_attr__('Extra Large', 'luxe-text-domain') => 'extra-large',
                    ),
                    "description" => esc_attr__("Select position of icon", "luxe-text-domain"),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Icon Color", "luxe-text-domain"),
                    "param_name" => "icon_color",
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Icon Position", "luxe-text-domain"),
                    "param_name"  => "icon_position",
                    "value"       => array(
                        esc_attr__('Left', 'luxe-text-domain')  => 'left',
                        esc_attr__('Center', 'luxe-text-domain')  => 'center',
                        esc_attr__('Right', 'luxe-text-domain') => 'right',
                    ),
                    "description" => esc_attr__("Select position of icon", "luxe-text-domain"),
                ),
                array(
                    "type"        => "vc_link",
                    "class"       => "",
                    "heading"     => esc_attr__("Link", "luxe-text-domain"),
                    "param_name"  => "link",
                    "description" => esc_attr__("Link location.", "luxe-text-domain"),
                ),
                array(
                    "type"        => "dropdown",
                    "class"       => "",
                    "heading"     => esc_attr__("Background Style", "luxe-text-domain"),
                    "param_name"  => "background_style",
                    "value"       => array(
                        esc_attr__("None", "luxe-text-domain")   => "none",
                        esc_attr__("Circle", "luxe-text-domain") => "circle",
                        esc_attr__("Square", "luxe-text-domain") => "square",
                        esc_attr__("Circle Outline", "luxe-text-domain") => "circle-outline",
                        esc_attr__("Square Outline", "luxe-text-domain") => "square-outline",
                    ),
                    "description" => esc_attr__("Style behind the icon.", "luxe-text-domain"),
                ),
                array(
                    "type"       => "colorpicker",
                    "class"      => "",
                    "heading"    => esc_attr__("Background Color", "luxe-text-domain"),
                    "param_name" => "background_color",
                    "value"      => "",
                    "dependency" => array(                
                        'element' => 'background_style',
                        'value_not_equal_to' => array(
                            'none'
                        ),),
                ),
                array(
                    "type"        => "textfield",
                    "class"       => "",
                    "heading"     => esc_attr__("Extra Class", "luxe-text-domain"),
                    "param_name"  => "el_class",
                    "value"       => "",
                    "description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.", "luxe-text-domain"),
                ),
            ),
        )
    );
}
add_action('vc_before_init', 'luxe_icon_vc', 100);

/**
 * Luxe link
 */
function luxe_icon($atts, $content = null)
{
    extract(shortcode_atts(array(
        'link'     => '',
        'icon_type'     => '',
        'icon'          => '',
        'icon_size'     => 'small',
        'icon_color'    => '',
        'icon_position' => 'left',
        'background_style'          => '',
        'background_color'          => '',
        'el_class'      => '',
    ), $atts));

    // Icon inline CSS
    $icon_inline_css = 'style="';
    if (!empty($icon_color)) {
        $icon_inline_css .= 'color:' . $icon_color . ';';
    }
    if (!empty($background_style) && $background_style != 'none') {
        $icon_inline_css .= 'background-color:' . $background_color . ';';
        $icon_inline_css .= 'border-color:' . $background_color . ';';
    }
    $icon_inline_css .= '"'; 
    $link = Helper\build_link($link);
    $target = ($link['target'] ? 'target="' . $link['target'] . '"' : '');
    ob_start();
    ?>
        <div class="luxe-icon-container wpb_content_element text-align-<?php echo $icon_position; ?>">
            <?php if (!empty($link)) { ?>
                <a href="<?php echo $link['url']; ?>" title="<?php echo $link['title']; ?>" <?php echo $target; ?>>
            <?php } ?>
                <?php if (!empty($icon)) { ?>
                    <i class="luxe-icon icon <?php echo $icon; ?> bg-<?php echo $background_style; ?> icon-size-<?php echo $icon_size; ?>" <?php echo $icon_inline_css; ?>></i>
                <?php } ?>
            <?php if (!empty($link)) { ?>
                </a>
            <?php } ?>
        </div>
    <?php
    $output = ob_get_clean();
    return $output;
}
add_shortcode('luxe_icon', 'luxe_icon');
