<?php

/**
 * Luxe unordered list VC map
 */

function luxe_unordered_list_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Unordered List Style","luxe-text-domain"),
    	   "base" => "luxe_unordered_list",
    	   "class" => "luxe_unordered_list",
    	   "icon" => "Unordered List Style",
    	   "category" => "Content",
    	   "description" => esc_attr__("Add a style to any child UL elements.","luxe-text-domain"),
           "content_element" => true,
           "show_settings_on_create" => false,
           "is_container" => true,
           'as_parent' => array('only' => 'vc_column_text'),
    	   "params" => array(
    		  // array(
    			 // "type" => "dropdown",
    			 // "class" => "",
    			 // "heading" => esc_attr__("Icon Position", "luxe-text-domain"),
    			 // "param_name" => "icon_position",
    			 // "value" => array(
    			 // 		esc_attr__('Top','luxe-text-domain') => 'top',
    			 // 		esc_attr__('Top Center','luxe-text-domain') => 'top-center',
    				// 	esc_attr__('Right','luxe-text-domain') => 'right',
    				// 	esc_attr__('Left','luxe-text-domain') => 'left',	
    			 // 		),							
    			 // "description" => esc_attr__("Enter Position of Icon", "luxe-text-domain")
    			 // ),
              array(
                "type" => "textfield",
                "class" => "",
                "heading" => esc_attr__("Extra Class",  "luxe-text-domain"),
                "param_name" => "el_class",
                "value" => "",
                "description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.",  "luxe-text-domain"),
              ),
    		),
            "js_view" => 'VcColumnView',
    	)
    );
}
add_action( 'vc_before_init', 'luxe_unordered_list_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Unordered_List extends WPBakeryShortCodesContainer {
    }
}

/**
 * Luxe unordered list
 */
function luxe_unordered_list($atts, $content = null)
{
    extract(shortcode_atts( array(
        'style' => 'arrow',
        'el_class'=>'',
    ),$atts));           

    ob_start();
    ?>
    <div class="list-style list-style-<?php echo $style; ?> <?php echo $el_class; ?>">
        <?php echo do_shortcode( $content ); ?>
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_unordered_list', 'luxe_unordered_list' );
