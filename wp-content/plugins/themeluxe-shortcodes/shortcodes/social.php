<?php

/**
 * Luxe social VC map
 */

function luxe_social_vc() {
    vc_map(
    	array(
    	   "name" => esc_attr__("Social","luxe-text-domain"),
    	   "base" => "luxe_social",
    	   "class" => "luxe_social",
    	   "icon" => "luxe_social",
    	   "as_parent" => array('only' => 'luxe_social_item'),
    	   "content_element" => true,
    	   "show_settings_on_create" => true,
    	   "is_container" => true,
    	   "category" => "Content",
    	   "description" => esc_attr__("Display your social media links.","luxe-text-domain"),
    	   "params" => array(
        		array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => esc_attr__("Font Style", "luxe-text-domain"),
                    "param_name" => "font_size",
                    "value" => array(
                        esc_attr__('Default','luxe-text-domain') => '',
                        esc_attr__('H1','luxe-text-domain') => 'h1',
                        esc_attr__('H2','luxe-text-domain') => 'h2',
                        esc_attr__('H3','luxe-text-domain') => 'h3',
                        esc_attr__('H4','luxe-text-domain') => 'h4',
                        esc_attr__('H5','luxe-text-domain') => 'h5',
                    ),							
                    "description" => esc_attr__("Size of modal box.", "luxe-text-domain")
        		),
                array(
                    "type" => "colorpicker",
                    "class" => "",
                    "heading" => esc_attr__("Color", "luxe-text-domain"),
                    "param_name" => "color",
                    "value" => "",
                ),
                array(
                    "type" => "dropdown",
                    "class" => "",
                    "heading" => esc_attr__("Style", "luxe-text-domain"),
                    "param_name" => "style",
                    "value" => array(
                        esc_attr__('None','luxe-text-domain') => '',
                        esc_attr__('Separator between items','luxe-text-domain') => 'social-separator',
                    ),                          
                    "description" => esc_attr__("Size of modal box.", "luxe-text-domain")
                ),
                array(
                    'type' => 'dropdown',
                    'heading' => esc_attr__( 'Alignment', 'luxe-text-domain' ),
                    'param_name' => 'alignment',
                    'value' => array(
                        esc_attr__( 'Left', 'luxe-text-domain' ) => 'left',
                        esc_attr__( 'Center', 'luxe-text-domain' ) => 'center',
                        esc_attr__( 'Right', 'luxe-text-domain' ) => 'right',
                    ),
                ),
    			array(
    				"type" => "textfield",
    				"class" => "",
    				"heading" => esc_attr__("Extra Class",  "luxe-text-domain"),
    				"param_name" => "el_class",
    				"value" => "",
    				"description" => esc_attr__("Add extra class name that will be applied to the icon process, and you can use this class for your customizations.",  "luxe-text-domain"),
    			),
    		),
            "js_view" => 'VcColumnView'
    	)
    );
}
add_action( 'vc_before_init', 'luxe_social_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Social extends WPBakeryShortCodesContainer {
    }
}

/**
 * Luxe social
 */

function luxe_social($atts, $content = null)
{
    extract(shortcode_atts( array(
        'style'=>'',
        'color'=>'',
        'font_size'=>'',
        'alignment'=>'left',
        'el_class'=>'',
    ),$atts));           

    $social_classes = array();
    $social_classes[] = $style;
    $social_classes[] = $font_size;
    $social_classes[] = 'text-align-' . $alignment;
    $social_classes[] = $el_class;

    $inline_css = 'style="';
    if (!empty($color)) {
        $inline_css .= 'color:' . $color . ';';
    }
    $inline_css .= '"';

    ob_start();
    ?>
    <div class="social <?php echo implode(' ', $social_classes); ?>" <?php echo $inline_css; ?>>
        <?php echo do_shortcode($content); ?>
    </div>           
    <?php
    $output = ob_get_clean();
    return $output;     
}
add_shortcode( 'luxe_social', 'luxe_social' );
