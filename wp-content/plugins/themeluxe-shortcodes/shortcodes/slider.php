<?php

use Luxe\Helper;

function luxe_slider_vc() {

    // Get post types list
    $post_types_list = Helper\get_post_types_list();
    $post_types_list[esc_attr__('Slides (manually add in Visual Composer)', 'luxe-text-domain')] = 'slides';

    vc_map( array(
        'name' => esc_attr__( 'Luxe Slider', 'luxe-text-domain' ),
        'base' => 'luxe_slider',
        'icon' => 'icon-wpb-images-carousel',
        'category' => esc_attr__( 'Content', 'luxe-text-domain' ),
        'description' => esc_attr__( 'Add content and images to a carousel/slider.', 'luxe-text-domain' ),
        'content_element' => true,
        'show_settings_on_create' => true,
        'is_container' => true,
        'as_parent' => array('only' => 'luxe_slide'),
        'params' => array(
            array(
                'type' => 'dropdown',
                'heading' => esc_attr__( 'Content Type', 'luxe-text-domain' ),
                'param_name' => 'post_type',
                'value' => $post_types_list,
                'save_always' => true,
                'description' => esc_attr__( 'Select content type for your slider or.', 'luxe-text-domain' ),
                'std' => 'images',
            ),
            // array(
            //     'type' => 'attach_images',
            //     'heading' => esc_attr__( 'Images', 'luxe-text-domain' ),
            //     'param_name' => 'images',
            //     'value' => '',
            //     'description' => esc_attr__( 'Select images from media library.', 'luxe-text-domain' ),
            //     'dependency' => array(
            //         'element' => 'post_type',
            //         'value' => array( 'images' ),
            //     ),
            // ),
            // Data settings
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Total items', 'luxe-text-domain' ),
                'param_name' => 'max_items',
                'value' => 10, // default value
                'param_holder_class' => 'vc_not-for-custom',
                'description' => esc_attr__( 'Set max limit for items in grid.', 'luxe-text-domain' ),
                'dependency' => array(
                    'element' => 'post_type',
                    'value_not_equal_to' => array( 'ids', 'custom' ),
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_attr__( 'Order by', 'luxe-text-domain' ),
                'param_name' => 'orderby',
                'value' => array(
                    esc_attr__( 'Date', 'luxe-text-domain' ) => 'post_date',
                    esc_attr__( 'Order by post ID', 'luxe-text-domain' ) => 'ID',
                    // esc_attr__( 'Author', 'luxe-text-domain' ) => 'author',
                    esc_attr__( 'Title', 'luxe-text-domain' ) => 'post_title',
                    esc_attr__( 'Last modified date', 'luxe-text-domain' ) => 'post_modified',
                    esc_attr__( 'Post/page parent ID', 'luxe-text-domain' ) => 'post_parent',
                    esc_attr__( 'Number of comments', 'luxe-text-domain' ) => 'comment_count',
                    esc_attr__( 'Menu order/Page Order', 'luxe-text-domain' ) => 'menu_order',
                    // esc_attr__( 'Meta value', 'luxe-text-domain' ) => 'meta_value',
                    // esc_attr__( 'Meta value number', 'luxe-text-domain' ) => 'meta_value_num',
                    esc_attr__( 'Random order', 'luxe-text-domain' ) => 'rand',
                ),
                'description' => esc_attr__( 'Select order type. If "Meta value" or "Meta value Number" is chosen then meta key is required.', 'luxe-text-domain' ),
                'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
                'param_holder_class' => 'vc_grid-data-type-not-ids',
                'dependency' => array(
                    'element' => 'post_type',
                    'value_not_equal_to' => array( 'ids', 'custom' ),
                ),
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_attr__( 'Sort order', 'luxe-text-domain' ),
                'param_name' => 'order',
                'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
                'value' => array(
                    esc_attr__( 'Descending', 'luxe-text-domain' ) => 'DESC',
                    esc_attr__( 'Ascending', 'luxe-text-domain' ) => 'ASC',
                ),
                'param_holder_class' => 'vc_grid-data-type-not-ids',
                'description' => esc_attr__( 'Select sorting order.', 'luxe-text-domain' ),
                'dependency' => array(
                    'element' => 'post_type',
                    'value_not_equal_to' => array( 'ids', 'custom' ),
                ),
            ),
          array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Meta key', 'luxe-text-domain' ),
                'param_name' => 'meta_key',
                'description' => esc_attr__( 'Input meta key for grid ordering.', 'luxe-text-domain' ),
                'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
                'param_holder_class' => 'vc_grid-data-type-not-ids',
                'dependency' => array(
                    'element' => 'orderby',
                    'value' => array( 'meta_value', 'meta_value_num' ),
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Offset', 'luxe-text-domain' ),
                'param_name' => 'offset',
                'description' => esc_attr__( 'Number of grid elements to displace or pass over.', 'luxe-text-domain' ),
                'group' => esc_attr__( 'Data Settings', 'luxe-text-domain' ),
                'param_holder_class' => 'vc_grid-data-type-not-ids',
                'dependency' => array(
                    'element' => 'post_type',
                    'value_not_equal_to' => array( 'ids', 'custom' ),
                ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Image size', 'luxe-text-domain' ),
                'param_name' => 'img_size',
                'value' => 'thumbnail',
                'description' => esc_attr__( 'Enter image size. Example: thumbnail, medium, large, full or other sizes defined by current theme. Alternatively enter image size in pixels: 200x100 (Width x Height). Leave empty to use "thumbnail" size.', 'luxe-text-domain' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Slides per view', 'luxe-text-domain' ),
                'param_name' => 'slides_per_view',
                'value' => '1',
                'description' => esc_attr__( 'Enter number of slides to display at the same time.', 'luxe-text-domain' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Margin', 'luxe-text-domain' ),
                'param_name' => 'margin',
                'value' => '30',
                'description' => esc_attr__( 'Space between items in px (e.g., enter 30 for 30 pixel spacing between each slide).', 'luxe-text-domain' ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Slider autoplay', 'luxe-text-domain' ),
                'param_name' => 'autoplay',
                'description' => esc_attr__( 'Enable autoplay mode.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Autoplay speed', 'luxe-text-domain' ),
                'param_name' => 'autoplay_speed',
                'value' => '400',
                'description' => esc_attr__( 'Animation time between slides.', 'luxe-text-domain' ),
                'dependency' => array('element' => 'autoplay', 'value' => 'true'),
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Hover pause', 'luxe-text-domain' ),
                'param_name' => 'hoverpause',
                'description' => esc_attr__( 'Pause slide when mouse is hovered over slider.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
                'std' => 'true'
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Loop', 'luxe-text-domain' ),
                'param_name' => 'loop',
                'description' => esc_attr__( 'Loop to beginning after last slide.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Bullets', 'luxe-text-domain' ),
                'param_name' => 'bullets',
                'description' => esc_attr__( 'If checked, pagination (bullet) controls will be shown.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
                'std' => 'true'
            ),
            array(
                'type' => 'dropdown',
                'heading' => esc_attr__( 'Pagination position', 'luxe-text-domain' ),
                'param_name' => 'bullets_position',
                'value' => array(
                    esc_attr__( 'Below content', 'luxe-text-domain' ) => 'below',
                    esc_attr__( 'Overlay on top of content', 'luxe-text-domain' ) => 'overlay',
                ),
                'description' => esc_attr__( 'Pick where your bullets should be located.  You may want them on top of your content in a traditional image slider, while they make more sense below with a content carousel.', 'luxe-text-domain' ),
                'dependency' => array(
                    'element' => 'bullets',
                    'value' => array( 'true' ),
                ),
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Arrows', 'luxe-text-domain' ),
                'param_name' => 'arrows',
                'description' => esc_attr__( 'If checked, prev/next buttons will be shown.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
                'std' => 'true'
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Animate Content', 'luxe-text-domain' ),
                'param_name' => 'animate_content',
                'description' => esc_attr__( 'Animate (fade in) each slide\'s content when in view.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Partial view', 'luxe-text-domain' ),
                'param_name' => 'partial_view',
                'description' => esc_attr__( 'Enter a number here in pixels to show a partial display of the next slide (e.g., 150 will show 150 pixels of the following slide).', 'luxe-text-domain' ),
                'value' => '0'
            ),
            array(
                'type' => 'checkbox',
                'heading' => esc_attr__( 'Full Browser Height', 'luxe-text-domain' ),
                'param_name' => 'full_browser_height',
                'description' => esc_attr__( 'Stretch this slider to be the height of the browser.', 'luxe-text-domain' ),
                'value' => array( esc_attr__( 'Yes', 'luxe-text-domain' ) => 'true' ),
            ),
            array(
                'type' => 'colorpicker',
                'heading' => esc_attr__( 'Slide Background Color', 'luxe-text-domain' ),
                'param_name' => 'slide_bg_color',
                'value' => getVcShared( 'colors' ),
                'param_holder_class' => 'vc_colored-dropdown',
                'description' => esc_attr__( 'The color behind your individual slides.  Images and other content may block this color from view, but it will be shown where there is only text.', 'luxe-text-domain' ),
            ),
            array(
                'type' => 'textfield',
                'heading' => esc_attr__( 'Extra class name', 'luxe-text-domain' ),
                'param_name' => 'el_class',
                'description' => esc_attr__( 'Style particular content element differently - add a class name and refer to it in custom CSS.', 'luxe-text-domain' ),
            ),
            array(
                'type' => 'css_editor',
                'heading' => esc_attr__( 'CSS box', 'luxe-text-domain' ),
                'param_name' => 'css',
                'group' => esc_attr__( 'Design Options', 'luxe-text-domain' ),
            ),
        ),
        "js_view" => 'VcColumnView'
    ));
}
add_action( 'vc_before_init', 'luxe_slider_vc', 100 );

/**
 * Extend content element
 */
if ( class_exists( 'WPBakeryShortCodesContainer' ) ) {
    class WPBakeryShortCode_Luxe_Slider extends WPBakeryShortCodesContainer {
    }
}

function luxe_slider( $atts, $content = null ) {

    extract( shortcode_atts( array(
        'post_type' => '',
        'images' => '',
        'img_size' => 'thumbnail',
        'slides_per_view' => '1',
        'arrows' => true,
        'bullets' => true,
        'bullets_position' => 'below',
        'autoplay' => false,
        'autoplay_speed' => '400',
        'hoverpause' => true,
        'loop' => true,
        'animate_content' => false,
        'partial_view' => '0',
        'margin' => '30',
        'el_class' => '',
        'full_browser_height' => '',
        'max_items' => '-1',
        'offset' => '0',
        'orderby' => 'post_date',
        'slide_bg_color' => '',
        'order' => 'DESC',
        'fill' => '',
        'css' => '',
    ), $atts ) );

    $slider_classes = array();
    $output = '';
    $div_id = uniqid();

    // Design option CSS class
    $css_class = Helper\get_vc_class($css, $atts);

    // Fill space
    if ($fill) {
        $slider_classes[] = 'fill-space';
    }
    // Background div
    $background_div = $background_div_close = '';
    if (!empty($slide_bg_color)) { 
        $background_div = '<div class="slide-bg" style="background-color:' . $slide_bg_color . ';">';
        $background_div_close = '</div>';
    }
    if (!empty($full_browser_height)) { 
        $slider_classes[] = 'full-browser-height';
    }
    if (!empty($animate_content)) {
        $slider_classes[] = 'animate-content';
    }
    // Bullets
    $slider_classes[] = 'bullets-' . $bullets_position;

    // Set image size in content types
    global $image_size;
    $image_size = $img_size;

    // Boolean vars
    $hoverpause = $hoverpause ? 'true' : 'false';
    $autoplay = $autoplay ? 'true' : 'false';
    $loop = $loop ? 'true' : 'false';
    $bullets = $bullets ? 'true' : 'false';
    $arrows = $arrows ? 'true' : 'false';

    // Get posts
    $post_args = array(
      'posts_per_page'   => $max_items,
      'offset'           => $offset,
      'orderby'          => $orderby,
      'order'            => $order,
      'post_type'        => $post_type,
      'post_status'      => 'publish',
      'suppress_filters' => true 
    );
    $posts = get_posts( $post_args ); 

    // Set content for slider
    if (!empty($images) && $post_type == 'images') {
        $images = explode(',', $images);
        foreach ($images as $image) {
            $content .= '[luxe_slide]';
            $content .= $background_div;
            $content .= wp_get_attachment_image( $image, $img_size );
            $content .= $background_div_close;
            $content .= '[/luxe_slide]';
        }
        $slider_classes[] = 'slider-type-images';
    }
    else {
        $n = 0;
        ob_start();
        global $post;
        foreach ($posts as $post) {
            setup_postdata( $post );
            ?>
                [luxe_slide] 
                <?php 
                    echo $background_div;
                    $post_format = get_post_format(); 
                    $post_format = ($post_format == 'gallery') ? 'standard' : $post_format;
                    get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : $post_format);
                    echo $background_div_close; 
                ?>
                [/luxe_slide] 
            <?php 
        }
        $content .= ob_get_clean();
        wp_reset_postdata();
        if ($n % $slides_per_view != 0) {
            $content .= '[/luxe_slide]';
        }
        $slider_classes[] = 'slider-type-content slider-content-' . $post_type;
    }


    if (!empty($content)) {
        ob_start();
        ?>
        <div id="slider-<?php echo $div_id; ?>" class="owl-carousel wpb_content_element <?php echo implode(' ', $slider_classes); ?> <?php echo $el_class; ?> <?php echo $css_class; ?>">
             <?php echo do_shortcode( $content ); ?>
        </div>
        <script>
        jQuery(document).ready(function($) {
            var slider = $('#slider-<?php echo $div_id; ?>');
            slider.imagesLoaded(function() {
                slider.owlCarousel({
                    nav: <?php echo $arrows; ?>,
                    dots: <?php echo $bullets; ?>,
                    navText: ['', ''],
                    <?php if ($slides_per_view > 1) { ?>
                    responsive:{
                        0:{
                            items:1,
                            stagePadding: 0
                        },
                        600:{
                            items:2,
                            stagePadding: <?php echo $partial_view/2; ?>
                        },
                        1000:{
                            items:<?php echo $slides_per_view; ?>,
                            stagePadding: <?php echo $partial_view; ?>

                        }
                    },
                    <?php } else { ?>
                        items: <?php echo $slides_per_view; ?>,
                        stagePadding: <?php echo $partial_view; ?>,
                    <?php } ?>
                    loop: <?php echo $loop; ?>, 
                    autoplay: <?php echo $autoplay; ?>, 
                    autoplaySpeed: <?php echo $autoplay_speed; ?>, 
                    autoplayHoverPause: <?php echo $hoverpause; ?>,
                    margin: <?php echo $margin; ?>,
                    // animateOut: 'fadeOut',
                    // animateIn: 'fadeIn',
                });
                slider.find('img').on('dragstart', function(event) { event.preventDefault(); });
            });
        });
        </script>
        <?php
        $output = ob_get_clean(); 
    } else {
        $output = esc_attr__('Please add some slides.', 'luxe-text-domain');
    }
    return $output;

}
add_shortcode( 'luxe_slider', 'luxe_slider' );